#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

static char *map = "      abcdefghijklmnopqrstuvwxyz      ABCDEFGHIJKLMNOPQRSTUVWXYZ       \n0123456789.,!?_#'\"/\\-:()";

static void out_char(uint8_t c) {
    static uint8_t abbr = 0;
    static uint8_t page = 0;

    if (abbr) {
        printf("[%d]", ((abbr-1)<<5) + c);
        abbr = 0;
    } else if (c == 4) {
        page = 32;
    } else if (c == 5) {
        page = 64;
    } else if (c == 0) {
        putchar(' ');
    } else if (c <= 3) {
        abbr = c;
    } else {
        putchar(map[page+c]);
        page = 0;
    }
}

uint8_t r[1024];

int main(void) {
    int i = 0;
    while (1) {
        int c = getchar();
        if (c == EOF)
            return 0;

        uint16_t s = (c << 8) | getchar();
        r[i] = (s >> 10) & 0x1F;
        out_char(r[i++]);
        r[i] = (s >>  5) & 0x1F;
        out_char(r[i++]);
        r[i] = (s >>  0) & 0x1F;
        out_char(r[i++]);
        if (s & 0x8000) {
            printf("\n");
            break;
        }
    }

    for (int j = 0; j < i; j++)
        printf("%d\n", r[j]);
}