#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include <termios.h>
#include <time.h>
#include <sys/ioctl.h>

#define STATS

FILE *zf;
uint8_t mem[0x8000];
uint32_t pc;
uint8_t n_args;
uint16_t operands[4];
uint16_t sp = 0x7F7E;
uint16_t bp = 0x7F7E;
uint16_t abbr_table_a;
uint8_t restart;

#define DYN_BASE  0x0100
#define DYN_SIZE  0x6E00

#define PAGE_SIZE 0x100
#define PAGE_MASK (PAGE_SIZE-1)
#define PAGE1     0x7000
#define PAGE2     (PAGE1+PAGE_SIZE)

#ifdef STATS
uint64_t used[256];
uint64_t page_changes = 0;
#endif

static inline uint16_t WORD(uint16_t a) {
    return (mem[a] << 8) | mem[(a)+1];
}
static inline void WWORD(uint16_t a, uint16_t v) {
    mem[a] = (v) >> 8;
    mem[(a)+1] = (v) & 0xFF;
}

uint8_t get_i(uint32_t a) {/*
    static uint32_t page[2];
    static uint8_t  old = 0;

    if (a < DYN_SIZE)
        return mem[DYN_BASE + a];

    if (page[0] == (a & ~PAGE_MASK)) {
        old = 1;
        return mem[PAGE1 + (a & PAGE_MASK)];
    }

    if (page[1] == (a & ~PAGE_MASK)) {
        old = 0;
        return mem[PAGE2 + (a & PAGE_MASK)];
    }

#ifdef STATS
    ++page_changes;
#endif

    page[old] = a & ~PAGE_MASK;
    fseek(zf, page[old], SEEK_SET);
    for (uint16_t i = 0; i < PAGE_SIZE; i++)
        mem[PAGE1+PAGE_SIZE*old + i] = fgetc(zf);

    uint8_t x = mem[PAGE1+PAGE_SIZE*old + (a & PAGE_MASK)];
    old = !old;
    return x;*/

    static uint32_t page;

    if (a < DYN_SIZE)
        return mem[DYN_BASE+a];
    if (page == (a & ~PAGE_MASK))
        return mem[PAGE1+(a & PAGE_MASK)];

#ifdef STATS
    ++page_changes;
#endif
    page = a & ~PAGE_MASK;
    fseek(zf, page, SEEK_SET);
    for (uint16_t i = 0; i < PAGE_SIZE; i++)
        mem[PAGE1 + i] = fgetc(zf);
    return mem[PAGE1+(a & PAGE_MASK)];

}

uint32_t print_zstr(uint32_t);

uint8_t zsabbr = 0;
uint8_t zspage = 0;
uint8_t zscode = 0;

void print_zchar(uint8_t c) {
    static const char *map = "      abcdefghijklmnopqrstuvwxyz      ABCDEFGHIJKLMNOPQRSTUVWXYZ      \x02\n0123456789.,!?_#'\"/\\-:()";

    if (zscode == 2) {
        zscode |= c << 5;
    } else if (zscode) {
        putchar(zscode | c);
        zscode = 0;
    } else if (zsabbr) {
        uint16_t a = WORD(DYN_BASE + abbr_table_a + ((zsabbr-1)<<6) + (c<<1));
        print_zstr(a << 1);
        zscode = 0;
        zsabbr = 0;
        zspage = 0;

    } else if (c == 4) {
        zspage = 32;
    } else if (c == 5) {
        zspage = 64;
    } else if (c == 0) {
        putchar(' ');
    } else if (c <= 3) {
        zsabbr = c;
    } else {
        c = map[zspage+c];
        if (c == 2)
            zscode = 2;
        else
            putchar(c);
        zspage = 0;
    }
}

uint32_t print_zstr(uint32_t a) {
    zscode = 0;
    zsabbr = 0;
    zspage = 0;
    while (1) {
        uint16_t s = (get_i(a) << 8) | get_i(a+1);
        print_zchar((s >> 10) & 0x1F);
        print_zchar((s >>  5) & 0x1F);
        print_zchar((s >>  0) & 0x1F);
        a += 2;
        if (s & 0x8000)
            return a;
    }
}

void push(uint16_t v) {
    sp -= 2;
    mem[sp] = v >> 8;
    mem[sp+1] = v & 0xFF;
}

uint16_t pop(void) {
    uint16_t v = (mem[sp] << 8) | mem[sp+1];
    sp += 2;
    return v;
}

uint16_t var(uint8_t ref) {
    if (ref < 0x10)
        return bp - ref*2;
    else
        return DYN_BASE + (ref-0x10)*2 + WORD(DYN_BASE+0xC);
}

void store(uint8_t ref, uint16_t val) {
    if (ref == 0)
        push(val);
    else
        WWORD(var(ref), val);
}

void call(void) {
    uint8_t ref = get_i(pc++);
    if (operands[0] == 0) {
        store(ref, 0);
        return;
    }

    push(pc);
    push(((pc >> 8) & 0x100) | ref);
    push(bp);
    bp = sp;
    pc = (uint32_t)operands[0] << 1;
    uint8_t n_local = get_i(pc++);
    for (int i = 0; i < n_local; i++) {
        if (i < n_args-1) {
            push(operands[i+1]);
            pc += 2;
        } else {
            uint8_t h = get_i(pc++);
            push((h << 8) | get_i(pc++));
        }
    }
}

void zreturn(uint16_t v) {
    sp = bp;
    bp = pop();
    uint16_t ref = pop();
    pc = pop();
    pc |= (ref & 0x100) << 8;
    store(ref & 0xFF, v);
}

uint16_t op_long(void) {
    uint8_t h = get_i(pc++);
    return (h << 8) | get_i(pc++);
}

uint16_t op_short(void) {
    return get_i(pc++);
}

uint16_t op_variable(void) {
    uint8_t ref = get_i(pc++);
    if (ref == 0)
        return pop();
    else
        return WORD(var(ref));
}

uint16_t op_parse(uint8_t type) {
    switch (type) {
        case 0b00:  return op_long();
        case 0b01:  return op_short();
        case 0b10:  return op_variable();
        default:    return 0;
    }
}

void storei(uint16_t val) {
    store(get_i(pc++), val);
}

void branch(uint8_t cond) {
    uint8_t b = get_i(pc++);
    int16_t offset = 0;
    if (~b & 0x40) {
        offset = (b << 8) | get_i(pc++);
        offset &= 0x3FFF;
        if (offset & 0x2000)
            offset |= 0xC000;
    } else {
        offset = b & 0x3F;
    }

    if (!cond == !(b >> 7)) {
        if (offset == 0)
            zreturn(0);
        else if (offset == 1)
            zreturn(1);
        else
            pc += offset - 2;
    }
}

uint16_t object(uint8_t n) {
    uint16_t base = WORD(DYN_BASE+0xA);
    uint16_t a = base + 31*2 + 9*(n-1);
    return a;
}

void remove_obj(uint8_t n) {
    uint16_t a = object(n);
    uint16_t p = mem[DYN_BASE+a+4];
    mem[DYN_BASE+a+4] = 0;
    if (p == 0)
        return;
    p = object(p);
    if (mem[DYN_BASE+p+6] == n)
        mem[DYN_BASE+p+6] = mem[DYN_BASE+a+5];
    else {
        uint16_t c = object(mem[DYN_BASE+p+6]);
        while (mem[DYN_BASE+c+5] != n)
            c = object(mem[DYN_BASE+c+5]);
        mem[DYN_BASE+c+5] = mem[DYN_BASE+a+5];
    }
}

void set_attr(uint8_t val) {
    uint16_t a = object(operands[0]);
    a += operands[1] >> 3;
    uint8_t mask = 1 << (7-(operands[1]&7));
    if (val)
        mem[DYN_BASE+a] |= mask;
    else
        mem[DYN_BASE+a] &= ~mask;
}

uint16_t prop_table(uint8_t n) {
    uint16_t a = WORD(DYN_BASE+object(n) + 7);
    a += 1 + mem[DYN_BASE+a]*2;
    return a;
}

uint16_t get_prop_addr(void) {
    uint16_t a = prop_table(operands[0]);
    while (1) {
        if ((mem[DYN_BASE+a] & 0x1F) < operands[1])
            return 0;
        if ((mem[DYN_BASE+a] & 0x1F) == operands[1])
            return a+1;

        a += (mem[DYN_BASE+a] >> 5) + 2;
    }
}

void input(void) {

    char *buf = (char *)mem+DYN_BASE+operands[0]+1;
    uint8_t len = mem[DYN_BASE+operands[0]];

    int i = 0;
    while (i < len) {
        int c = getchar();
        if (c == 127) {
            if (i > 0) {
                --i;
                printf("\b \b");
            }
        } else if (c == '\n' || c == '\r') {
            putchar('\n');
            break;
        } else if (isprint(c)) {
            putchar(c);
            buf[i++] = tolower(c);
        }
    }
    buf[i] = 0;

    uint8_t *parse_base = mem+DYN_BASE+operands[1];
    uint8_t *parse = parse_base + 2;
    uint8_t n_words = 0;
    char *wstart = buf;
    char *wend;

    while (1) {
        while (1) {
            if (!*wstart)
                goto end;
            if (*wstart != ' ')
                break;
            ++wstart;
        }

        wend = wstart;
        do {
            ++wend;
        } while (*wend && *wend != ' ' && *wend != '.' && *wend != ',' && *wend != '"');

        parse[0] = 0;
        parse[1] = 0;
        parse[2] = len = wend - wstart;
        parse[3] = wstart - buf + 1;

        uint8_t b[8];
        for (int i = 0; i < 6; i++) {
            if (i < len) {
                if (isalpha(wstart[i]))
                    b[i] = wstart[i]-'a'+6;
                else {
                    b[i] = 5;
                    char c = wstart[i];
                    if (isdigit(c)) {
                        b[++i] = c - '0' + 8;
                    } else {
                        switch (c) {
                            case '.':   b[++i] = 0x12; break;
                            case ',':   b[++i] = 0x13; break;
                            case '!':   b[++i] = 0x14; break;
                            case '?':   b[++i] = 0x15; break;
                            case '_':   b[++i] = 0x16; break;
                            case '#':   b[++i] = 0x17; break;
                            case '\'':  b[++i] = 0x18; break;
                            case '"':   b[++i] = 0x19; break;
                            case '/':   b[++i] = 0x1a; break;
                            case '\\':  b[++i] = 0x1b; break;
                            case '-':   b[++i] = 0x1c; break;
                            case ':':   b[++i] = 0x1d; break;
                            case '(':   b[++i] = 0x1e; break;
                            case ')':   b[++i] = 0x1f; break;

                            default:    b[++i] = 0x07; break;
                        }
                    }
                }
            }
            else
                b[i] = 5;
        }

        uint8_t word[4];

        word[0] = (b[0] << 2) | (b[1] >> 3);
        word[1] = (b[1] << 5) | b[2];
        word[2] = 0x80 | (b[3] << 2) | (b[4] >> 3);
        word[3] = (b[4] << 5) | b[5];

        uint16_t dict = WORD(DYN_BASE+8)+7;
        uint16_t start = 0;
        uint16_t middle;
        uint16_t end = WORD(DYN_BASE+dict-2);

        while (end >= start) {
            middle = (start + end) / 2;
            uint8_t *a = mem+DYN_BASE + dict + middle*7;
            int i = 0;
            while (1) {
                if (word[i] < a[i]) {
                    end = middle - 1;
                    break;
                }
                if (word[i] > a[i]) {
                    start = middle + 1;
                    break;
                }
                ++i;
                if (i == 4) {
                    uint16_t ba = a-mem-DYN_BASE;
                    parse[0] = ba >> 8;
                    parse[1] = ba & 0xFF;
                    goto found;
                }
            }
        }
    found:
        parse += 4;
        ++n_words;
        wstart = wend;
    }
end:
    parse_base[1] = n_words;
}

void run_0op(uint8_t op_num) {
    switch (op_num) {
        case 0x0:   /* rtrue */
            zreturn(1);
            break;
        case 0x1:   /* rfalse */
            zreturn(0);
            break;
        case 0x2:   /* print */
            pc = print_zstr(pc);
            break;
        case 0x3:   /* print_ret */
            pc = print_zstr(pc);
            putchar('\n');
            zreturn(1);
            break;
        //~ case 0x5:   /* save */
            //~ break;
        //~ case 0x6:   /* restore */
            //~ break;
        case 0x7:   /* restart */
            restart = 1;
            break;
        case 0x8:   /* ret_popped */
            zreturn(pop());
            break;
        case 0x9:   /* pop */
            pop();
            break;
        case 0xA:   /* quit */

#ifdef STATS
            uint64_t total = 0;
            for (int i = 0; i < 256; i++) {
                total += used[i];
                if (used[i])
                    printf(" %d %02x: %lu\n", i, i, used[i]);
            }
            printf("%lu pages, %lu instructions\n", page_changes, total);
#endif

            struct termios term;
            tcgetattr(fileno(stdin), &term);
            term.c_lflag |= ICANON;
            term.c_lflag |= ECHO;
            tcsetattr(fileno(stdin), 0, &term);
            exit(EXIT_SUCCESS);
            break;
        case 0xB:   /* new_line */
            putchar('\n');
            break;
        case 0xC:   /* show_status */
            break;
        case 0xD:   /* verify */
            branch(1);
            break;
        default:
            break;
    }
}

static void run_1op(uint8_t op_num) {
    uint16_t a;
    switch (op_num) {
        case 0x0:  /* jz */
            branch(operands[0] == 0);
            break;
        case 0x1:  /* get_sibling */
            a = mem[DYN_BASE+object(operands[0]) + 5];
            storei(a);
            branch(a != 0);
            break;
        case 0x2:  /* get_child */
            a = mem[DYN_BASE+object(operands[0]) + 6];
            storei(a);
            branch(a != 0);
            break;
        case 0x3:  /* get_parent */
            a = mem[DYN_BASE+object(operands[0]) + 4];
            storei(a);
            break;
        case 0x4:  /* get_prop_len */
            storei(operands[0] ? (mem[DYN_BASE+operands[0]-1] >> 5) + 1 : 0);
            break;
        case 0x5:  /* inc */
            a = var(operands[0]);
            WWORD(a, WORD(a)+1);
            break;
        case 0x6:  /* dec */
            a = var(operands[0]);
            WWORD(a, WORD(a)-1);
            break;
        case 0x7:  /* print_addr */
            print_zstr(operands[0]);
            break;
        case 0x9:  /* remove_obj */
            remove_obj(operands[0]);
            break;
        case 0xA:  /* print_obj */
            print_zstr(WORD(DYN_BASE+object(operands[0]) + 7) + 1);
            break;
        case 0xB:  /* ret */
            zreturn(operands[0]);
            break;
        case 0xC:  /* jump */
            pc += (int16_t)operands[0] - 2;
            break;
        case 0xD:  /* print_paddr */
            print_zstr((uint32_t)operands[0] << 1);
            break;
        case 0xE:  /* load */
            storei(WORD(var(operands[0])));
            break;
        default:
            break;
    }
}

static void run_2op(uint8_t op_num) {
    uint16_t a, b;
    switch (op_num) {
        case 0x01:  /* je */
            a = 0;
            for (int i = 1; !a && i < n_args; i++)
                a |= operands[0] == operands[i];
            branch(a);
            break;
        case 0x02:  /* jl */
            branch((int16_t)operands[0] < (int16_t)operands[1]);
            break;
        case 0x03:  /* jg */
            branch((int16_t)operands[0] > (int16_t)operands[1]);
            break;
        case 0x04:  /* dec_chk */
            a = var(operands[0]);
            b = WORD(a)-1;
            WWORD(a, b);
            branch((int16_t)b < (int16_t)operands[1]);
            break;
        case 0x05:  /* inc_chk */
            a = var(operands[0]);
            b = WORD(a)+1;
            WWORD(a, b);
            branch((int16_t)b > (int16_t)operands[1]);
            break;
        case 0x06:  /* jin */
            branch(mem[DYN_BASE+object(operands[0]) + 4] == operands[1]);
            break;
        case 0x07:  /* test */
            branch((operands[0] & operands[1]) == operands[1]);
            break;
        case 0x08:  /* or */
            storei(operands[0] | operands[1]);
            break;
        case 0x09:  /* and */
            storei(operands[0] & operands[1]);
            break;
        case 0x0A:  /* test_attr */
            a = object(operands[0]);
            a += operands[1] >> 3;
            branch(mem[DYN_BASE+a] & (1 << (7-(operands[1]&7))));
            break;
        case 0x0B:  /* set_attr */
            set_attr(1);
            break;
        case 0x0C:  /* clear_attr */
            set_attr(0);
            break;
        case 0x0D:  /* store */
            WWORD(var(operands[0]), operands[1]);
            break;
        case 0x0E:  /* insert_obj */
            remove_obj(operands[0]);
            uint8_t O = operands[0];
            uint8_t D = operands[1];
            a = object(D);
            b = object(O);

            mem[DYN_BASE+b+5] = mem[DYN_BASE+a+6];  /* O->sibling = D->child */
            mem[DYN_BASE+a+6] = O;                  /* D->child = O */
            mem[DYN_BASE+b+4] = D;                  /* O->parent = D */

            break;
        case 0x0F:  /* loadw */
            storei(WORD(DYN_BASE + operands[0] + 2*operands[1]));
            break;
        case 0x10:  /* loadb */
            storei(mem[DYN_BASE + operands[0] + operands[1]]);
            break;
        case 0x11:  /* get_prop */
            a = get_prop_addr();
            if (a) {
                if ((mem[DYN_BASE+a-1] >> 5))
                    b = WORD(DYN_BASE+a);
                else
                    b = mem[DYN_BASE+a];
                storei(b);
            } else {
                storei(WORD(DYN_BASE+WORD(DYN_BASE+0xA) + 2*(operands[1]-1)));
            }
            break;
        case 0x12:  /* get_prop_addr */
            storei(get_prop_addr());
            break;
        case 0x13:  /* get_next_prop */
            if (operands[1]) {
                a = get_prop_addr();
                a += 1 + (mem[DYN_BASE+a-1] >> 5);
                storei(mem[DYN_BASE+a] & 0x1F);
            } else {
                a = prop_table(operands[0]);
                storei(mem[DYN_BASE+a] & 0x1F);
            }
            break;
        case 0x14:  /* add */
            storei(operands[0] + operands[1]);
            break;
        case 0x15:  /* sub */
            storei(operands[0] - operands[1]);
            break;
        case 0x16:  /* mul */
            storei((int16_t)operands[0] * (int16_t)operands[1]);
            break;
        case 0x17:  /* div */
            storei((int16_t)operands[0] / (int16_t)operands[1]);
            break;
        case 0x18:  /* mod */
            storei((int16_t)operands[0] % (int16_t)operands[1]);
            break;

        default:
            break;
    }
}

static void run_var(uint8_t op_num) {
    uint16_t a;
    switch (op_num) {
        case 0x00:  /* call */
            call();
            break;
        case 0x01:  /* storew */
            WWORD(DYN_BASE + operands[0] + 2*operands[1], operands[2]);
            break;
        case 0x02:  /* storeb */
            mem[DYN_BASE + operands[0] + operands[1]] = operands[2];
            break;
        case 0x03:  /* put_prop */
            a = get_prop_addr();
            if ((mem[DYN_BASE+a-1] >> 5))
                WWORD(DYN_BASE+a, operands[2]);
            else
                mem[DYN_BASE+a] = operands[2] & 0xFF;
            break;
        case 0x04:  /* sread */
            input();
            break;
        case 0x05:  /* print_char */
            putchar(operands[0]);
            break;
        case 0x06:  /* print_num */
            printf("%hi", operands[0]);
            break;
        case 0x07:  /* random */
            a = rand() % operands[0] + 1;
            storei(a);
            break;
        case 0x08:  /* push */
            push(operands[0]);
            break;
        case 0x09:  /* pull */
            WWORD(var(operands[0]), pop());
            break;
        case 0x13:  /* output_stream */
            break;
        case 0x14:  /* input_stream */
            break;

        default:
            break;
    }
}

int main(int argc, char **argv) {
    srand(time(NULL));

    struct termios term;
    tcgetattr(fileno(stdin), &term);
    term.c_lflag &= ~ICANON;
    term.c_lflag &= ~ECHO;
    tcsetattr(fileno(stdin), 0, &term);
    setbuf(stdin, NULL);

    zf = fopen(argv[1], "r");

begin:
    restart = 0;
    rewind(zf);
    for (uint16_t i = 0; i < DYN_SIZE; i++)
        mem[i+DYN_BASE] = fgetc(zf);

    pc = WORD(6+DYN_BASE);
    abbr_table_a = WORD(0x18+DYN_BASE);

    while (1) {
        if (restart)
            goto begin;

        uint8_t i = get_i(pc++);

#ifdef STATS
        ++used[i];
#endif

main_loop:

        /* Long */
        if (~i & 0x80) {
            if (i & 0x40)
                operands[0] = op_variable();
            else
                operands[0] = op_short();
            if (i & 0x20)
                operands[1] = op_variable();
            else
                operands[1] = op_short();
            n_args = 2;
            run_2op(i & 0x1F);
        }
        /* Short */
        else if (~i & 0x40) {
            uint8_t op_type = (i >> 4) & 3;
            uint8_t op_num = i & 0xF;

            if (op_type == 3) {
                n_args = 0;
                run_0op(op_num);
            } else {
                n_args = 1;
                operands[0] = op_parse(op_type);
                run_1op(op_num);
            }
        }
        /* Variable */
        else {
            uint8_t op_type = get_i(pc++);
            uint8_t op_num = i & 0x1F;

            for (n_args = 0; n_args < 4; n_args++) {
                uint8_t t = (op_type >> (6-2*n_args)) & 3;
                if (t == 3)
                    break;
                operands[n_args] = op_parse(t);
            }

            if (i & 0x20)
                run_var(op_num);
            else
                run_2op(op_num);
        }
    }
}