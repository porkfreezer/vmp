#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>

#include "vmpasm.h"
#include "output.h"

typedef struct {
    const char *name;
    char *temp_name;
    FILE *file;
} atomic_writer_t;

static atomic_writer_t *atomic_writer_new(const char *name) {
    atomic_writer_t *a = calloc(1, sizeof(atomic_writer_t));
    a->name = name;
    a->temp_name = malloc(strlen(name)+6+1);
    strcpy(a->temp_name, name);
    strcat(a->temp_name, "XXXXXX");

    int fd;
    if ((fd = mkstemp(a->temp_name)) < 0)
        err("failed to create temporary file: %s", strerror(errno));
    if (!(a->file = fdopen(fd, "w")))
        err("failed to open temporary file: %s", strerror(errno));

    return a;
}

static void atomic_writer_close(atomic_writer_t *a, bool commit) {
    fclose(a->file);

    if (commit) {
        if (rename(a->temp_name, a->name))
            err("failed to rename temporary file to '%s': %s", a->name, strerror(errno));
    } else {
        if (remove(a->temp_name))
            err("failed to remove temporary file: %s", strerror(errno));
    }

    free(a->temp_name);
    free(a);
}

typedef struct {
    atomic_writer_t *file;
    int32_t segment;
    int32_t base;
    uint8_t buf[IHEX_BYTES_PER_RECORD];
    int n;
    int bytes;
} hex_file_t;

void fprintf_check(const char *name, FILE *file, const char *format, ...) {
    va_list args;
    va_start(args, format);
    if (vfprintf(file, format, args) < 0)
        err("failed to write to '%s': %s", name, strerror(errno));
    va_end(args);
}

static hex_file_t *hex = NULL;
static atomic_writer_t *list = NULL;
static atomic_writer_t *header = NULL;
static atomic_writer_t *pages = NULL;

static hex_file_t *ihex_new(const char *name) {
    hex_file_t *out = malloc(sizeof(hex_file_t));
    out->segment = 0;
    out->base = 0;
    out->n = 0;
    out->file = atomic_writer_new(name);
    out->bytes = 0;

    return out;
}

#define seg(a) (((a) & 0xF0000) >> 4)

static void ihex_output(hex_file_t *hf, int32_t address, uint8_t d) {

    ++hf->bytes;

    if (address >= 0 && hf->n < IHEX_BYTES_PER_RECORD && address == hf->base+hf->n && hf->segment == seg(address)) {
        hf->buf[hf->n++] = d;
        return;
    }

    atomic_writer_t *hex = hf->file;

    if (hf->n) {
        uint32_t a = hf->base;
        uint8_t checksum = hf->n + (a >> 8) + (a & 0xFF);

        fprintf_check(hex->name, hex->file, ":%02hhX%04hX00", hf->n, a & 0xFFFF);

        for (int i = 0; i < hf->n; i++) {
            fprintf_check(hex->name, hex->file, "%02hhX", hf->buf[i]);
            checksum += hf->buf[i];
        }

        fprintf_check(hex->name, hex->file, "%02hhX\n", ~checksum + 1);
    }

    if (address >= 0 && hf->segment != seg(address)) {
        hf->segment = seg(address);
        uint8_t checksum = 2 + 2 + (hf->segment >> 8) + (hf->segment & 0xFF);
        fprintf_check(hex->name, hex->file, ":02000002%04hX%02hhX\n", hf->segment, ~checksum + 1);
    }

    hf->buf[0] = d;
    hf->n = 1;
    hf->base = address;
}

static void ihex_close(hex_file_t *hf, bool commit) {
    if (commit) {
        printf("%d bytes output to %s\n", hf->bytes, hf->file->name);
        ihex_output(hf, -1, 0xff);
        fprintf_check(hf->file->name, hf->file->file, ":00000001FF\n");
    }
    atomic_writer_close(hf->file, commit);
    free(hf);
}

static void list_output(const char *line, int32_t address, const uint8_t *d, size_t n_items) {
    if (address >= 0)
        fprintf_check(list->name, list->file, "%05x ", address);
    else
        fprintf_check(list->name, list->file, "      ");

    for (size_t i = 0; i < 4 && i < n_items; i++)
        fprintf_check(list->name, list->file, "%02x ", d[i]);

    fprintf_check(list->name, list->file, "%*s   %s\n", (int)(n_items >= 4 ? 0 : (4-n_items)*3), "", line);

    for (size_t i = 4; i < n_items; i++)
        fprintf_check(list->name, list->file, "%s%02x%s", (i&3) == 0 ? "      " : " ", d[i], (i&3) == 3 ? "\n" : "");

    if (n_items > 4 && n_items % 4)
        fprintf_check(list->name, list->file, "\n");
}

void output_open(const char *hex_name, const char *list_name, const char *header_name, const char *pages_name) {
    if (hex_name)
        hex = ihex_new(hex_name);
    if (list_name)
        list = atomic_writer_new(list_name);
    if (header_name)
        header = atomic_writer_new(header_name);
    if (pages_name)
        pages = atomic_writer_new(pages_name);
}

void output(const char *line, int32_t address, const uint8_t *d, size_t n_items) {
    if (hex && address >= 0)
        for (size_t i = 0; i < n_items; i++)
            ihex_output(hex, address+i, d[i]);
    if (list)
        list_output(line, address, d, n_items);
}

void output_header(label_t *labels, int n_labels) {
    for (int i = 0; i < n_labels; i++) {
        label_t *l = labels+i;
        if (header && l->scope == 0)
            fprintf_check(header->name, header->file, ".set %s 0x%x\n", l->name, l->address);
        if (pages)
            fprintf_check(pages->name, pages->file, "%s:%d %s =%x\n", l->filename, l->line, l->name, (l->address >> 8) + ((l->address >> 7)&1));
    }
}

void output_cleanup(bool commit) {
    if (hex)
        ihex_close(hex, commit);
    if (list)
        atomic_writer_close(list, commit);
    if (header)
        atomic_writer_close(header, commit);
    if (pages)
        atomic_writer_close(pages, commit);

}