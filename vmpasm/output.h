#pragma once

#include <stdbool.h>
#include "vmpasm.h"

#define IHEX_BYTES_PER_RECORD 16

enum {
  OUTPUT_NONE = -2,
  OUTPUT_DATA = -1
  /* OUTPUT_BOTH >= 0 */
};

void output_open(const char *hex_name,
                 const char *list_name,
                 const char *header_name,
                 const char *pages_name);

void output(const char *line, int32_t address, const uint8_t *d, size_t n_items);

void output_header(label_t *labels, int n_labels);

void output_cleanup(bool commit);
