#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>
#include <errno.h>

#include "ansi-color-codes.h"
#include "vmpasm.h"
#include "output.h"

typedef enum {
    LINE_EMPTY,
    LINE_MNEMONIC,
    LINE_DIRECTIVE,
    LINE_LABEL,
    LINE_COMMENT
} line_type_t;

enum {
    SECTION_INSTR,
    SECTION_DATA
};

#define err_line(format, ...) do { \
    fprintf(stderr, BHWHT"%s:%d: "BRED"error"CRESET": "format"\n%7d | %s\n", filename, line_no, ##__VA_ARGS__, line_no, line_orig); \
    fail(); \
} while (0)

#define warn_line(format, ...) \
    fprintf(stderr, BHWHT"%s:%d: "BYEL"warning"CRESET": "format"\n%7d | %s\n", filename, line_no, ##__VA_ARGS__, line_no, line_orig); \

static const char *filename;
static const char *line_orig;
static int line_no;
static int scope_in;
static label_t *labels = NULL;
static int n_labels = 0;

static void add_label(const char *label, uint16_t address) {
    char *name = strdup(label);

    strtok(name, " \n\r\t:");
    if (isdigit(*name))
        err_line("label is a number");
    if (strchr(name, '.'))
        err_line("label contains '.'");
    if (strchr(name, '+'))
        err_line("label contains '+'");
    if (strchr(name, '-'))
        err_line("label contains '-'");

    n_labels++;
    labels = realloc(labels, n_labels*sizeof(label_t));

    label_t *l = labels+n_labels-1;

    l->name = name;
    l->address = address;
    l->filename = filename;
    l->line = line_no;
    l->scope = (isalpha(*name) || *name == '_') ? 0 : scope_in;

    for (int i = 0; i < n_labels-1; i++) {
        label_t *l2 = labels+i;
        if (!strcmp(l->name, l2->name) && (!l->scope || !l2->scope || (l->scope == l2->scope)))
            err_line("duplicated label '%s' from "BHWHT"%s:%d"CRESET, name, l2->filename, l2->line);
    }
}

static bool isregister(const char *op) {
    return op && ((strlen(op) == 1 && strchr("ABXYabxy", op[0])) || !strcasecmp(op, "PCL") || !strcasecmp(op, "PCH"));
}

static line_type_t line_type(const char *l) {
    if (!*l)
        return LINE_EMPTY;

    if (*l == '.')
        return LINE_DIRECTIVE;

    if (!isspace(*l) && *l != ';')
        return LINE_LABEL;

    const char *s = l;
    while (*s) {
        if (*s == ';')
            return LINE_COMMENT;
        if (!isspace(*s))
            return LINE_MNEMONIC;
        ++s;
    }

    return LINE_EMPTY;
}

static uint8_t parse_register(const char *op) {
    switch (toupper(*op)) {
        case 'A':   return 0b11;
        case 'B':   return 0b10;
        case 'X':   return 0b01;
        case 'Y':   return 0b00;
        default:
            err_line("invalid register '%s'", op);
    }

    return 0;
}
static uint8_t parse_register_b(const char *op) {
    if (op[0] && op[1]) {
        if (!strcasecmp(op, "PCL"))
            return 0b00000;
        if (!strcasecmp(op, "PCH"))
            return 0b00100;
        err_line("invalid register '%s'", op);
    }
    return (~parse_register(op) & 7) << 2;
}

static char parse_escaped(char c) {
    switch (c) {
        case 'n':   return '\n';
        case 'r':   return '\r';
        case 's':   return ' ';
        case 'c':   return ',';
        case 'e':   return '\x1b';
        default:    return c;
    }
}

static int32_t parse_immediate_inner(const char *op) {
    int32_t imm, read;
    /* number */
    if (isdigit(*op) || *op == '-') {
        if (sscanf(op, "%i%n", &imm, &read) != 1 || read != strlen(op))
            err_line("invalid operand for immediate: '%s'", op);
    }
    /* label/constant */
    else {
        char *name = strdup(op);
        char *p = strchr(name, '.');
        if (p)
            *p++ = '\0';
        for (int i = 0; ; i++) {
            if (i >= n_labels)
                err_line("undefined label '%s'", name);
            if (!strcmp(labels[i].name, name) && (!labels[i].scope || labels[i].scope == scope_in)) {
                imm = labels[i].address;
                break;
            }
        }
        if (p) {
            if (!strcasecmp(p, "H"))
                imm = (imm >> 8) + ((imm >> 7) & 1);
            else if (!strcasecmp(p, "HR"))
                imm >>= 8;
            else if (!strcasecmp(p, "L"))
                imm &= 0xFF;
            else
                err_line("undefined label part '%s'", p);
        }
        free(name);
    }
    return imm;
}

static uint16_t parse_immediate(const char *op, uint32_t mask) {
    int32_t imm = 0;
    int sign = 1;

    char *i = strdup(op);
    char *p;
    char *s = i;
    while (1) {
        if (!*s)
            break;
        if (*s == '+') {
            sign = 1;
            ++s;
        } else if (*s == '-') {
            sign = -1;
            ++s;
        } else if (s != i)
            err_line("weird stuff in immediate");

        if (*s == '\'') {
            if (strlen(s) < 3)
                err_line("unclosed character constant");
            if (s[1] == '\\') {
                imm += sign * parse_escaped(s[2]);
                s += 3;
            } else {
                imm += sign * s[1];
                s += 2;
            }
            if (*s++ != '\'')
                err_line("unclosed character constant");
        } else {
            p = s;
            while (isalpha(*p) || (*p && *p != '-' && *p != '+'))
                ++p;
            char save = *p;
            *p = 0;
            imm += sign * parse_immediate_inner(s);
            *p = save;
            s = p;
        }

    }

    free(i);

    if (!(imm == (imm & mask) || imm == ((imm & mask) | ~mask)))
        warn_line("'%s' overflows immediate byte; truncating 0x%04x to 0x%02x", op, imm & 0xFFFF, imm & 0xFF);

    return imm & mask;
}

static void parse_address(const char *op, opcode_t *out) {
    if (!*op || *op != '[' || op[strlen(op)-1] != ']')
        err_line("invalid address '%s'", op);
    ++op;

    uint8_t mode = 0;

    char base_h = 0;
    char *base_l = NULL;
    char *imm_s = NULL;

    const char *half = strchr(op, '|');
    base_h = toupper(*op);
    if (base_h == 'X')
        mode |= 0b100;
    else if (base_h == 'Y')
        mode |= 0b101;

    if (half != NULL && (half != op+1 || !mode))
        err_line("invalid register for high address base '%c'", base_h);

    if (!(strchr("]+-", op[1]) || half == op+1))
        mode = 0;

    if (half == NULL)
        half = op;
    else
        ++half;

    base_l = strdup(half);
    base_l[strlen(base_l)-1] = 0;

    char save;
    imm_s = strchr(base_l, '+');
    if (!imm_s)
        imm_s = strchr(base_l, '-');
    if (imm_s) {
        save = *imm_s;
        *imm_s = 0;
    }

    uint16_t imm = 0;
    if (isregister(base_l)) {
        if (strlen(base_l) == 1 && toupper(*base_l) == 'B') {
            if (base_h == 'Y')
                err_line("B must be paired with X");
            mode |= 0b010;
        }
        else if (strlen(base_l) == 1 && toupper(*base_l) == 'A') {
            if (base_h == 'X')
                err_line("A must be paired with Y");
            mode |= 0b011;
        }

        if (imm_s) {
            *imm_s = save;
            imm = parse_immediate(imm_s, DBYTE);
        }
    } else {
        if (imm_s)
            *imm_s = save;
        imm = parse_immediate(base_l, DBYTE);
        imm_s = base_l;
    }

    if ((imm & 0xFF00) && ((imm & 0xFF00) != 0xFF00))
        warn_line("'%s' overflows immediate byte; truncating 0x%04x to 0x%02x", imm_s, imm & 0xFFFF, imm & 0xFF);
    out->immediate = imm & 0xFF;

    if (base_l)
        free(base_l);

    out->command |= mode << 2;
}

static opcode_t encode_op(const char *mnem_in, const char *op1, const char *op2, const char *op3) {
    opcode_t op = { .op = 0 };

    char mnem[strlen(mnem_in+1)];

    if (!strcasecmp(mnem_in, "cmp"))
        strcpy(mnem, "subt");
    else if (!strcasecmp(mnem_in, "cmpc"))
        strcpy(mnem, "subct");
    else if (!strcasecmp(mnem_in, "test")) {
        strcpy(mnem, "subt");
        op2 = "0";
    }
    else if (!strcasecmp(mnem_in, "nop")) {
        strcpy(mnem, "jnv");
        op1 = "[0]";
    }
    else
        strcpy(mnem, mnem_in);

    if (!op1)
        err_line("no operands provided");

    /* mov */
    if (!strcasecmp(mnem, "mov")) {
        if (op3)
            err_line("too many operands");
        if (!op2)
            err_line("not enough operands");

        if (isregister(op1)) {
            /* mov R, [mem] => load */
            if (*op2 == '[') {
                op.command |= 0b010 << 5;
                op.command |= parse_register(op1);
                parse_address(op2, &op);
                return op;
            }

            /* else: mov R, R|imm => ALU */
        }

        /* mov [mem], R => store */
        else if (isregister(op2)) {
            op.command |= 0b011 << 5;
            op.command |= parse_register(op2);
            parse_address(op1, &op);

            return op;
        }

        else
            err_line("one register and one memory location required");
    }

    /* jxx mem */
    else if (mnem[0] == 'j' || mnem[0] == 'J') {
        static const char *jmps[] = {"jmp", "jz", "jn", "jc", "jnv", "jnz", "jp", "jnc"};
        uint8_t opcode;
        for (opcode = 0; opcode < 8 && strcasecmp(mnem, jmps[opcode]); opcode++)
            ;
        if (opcode == 8)
            err_line("unrecognized mnemonic: %s", mnem_in);

        op.command |= opcode & 0b11;
        op.command |= (opcode & 0b100) << (5-2);

        parse_address(op1, &op);

        return op;
    }

    /* ALU */
    op.command |= 1 << 7;

    bool compare = mnem[strlen(mnem)-1] == 't' || mnem[strlen(mnem)-1] == 't';
    if (compare) {
        mnem[strlen(mnem)-1] = '\0';
        op.command |= 1 << 6;
    }

    static const char *alu_ops[] = {"mov", "or", "nand", "and", "addc", "add", "subc", "sub"};
    uint8_t opcode;
    for (opcode = 0; opcode < 8 && strcasecmp(mnem, alu_ops[opcode]); opcode++)
        ;
    if (opcode == 8)
        err_line("unrecognized mnemonic: %s", mnem_in);
    op.command |= opcode << 3;

    /*                   d a b i
     * xxx  R, R, R   => 1 2 3 -
     * xxx  R, i      => 1 - - 2
     * xxx  R, R      => 1 1 2 - shorthand (add A, A, B => add A, B), or compare RR
     * test R         => x x 1 - test R
     * test i         => x - - 1 test i
     */

    if (op3) {
        op.command |= parse_register(op1);
        op.immediate |= parse_register(op2);
        op.immediate |= parse_register_b(op3);
    }

    else if (op2) {
        op.command |= parse_register(op1);
        if (isregister(op2)) {
            op.immediate |= parse_register(op1);
            op.immediate |= parse_register_b(op2);
        } else {
            op.immediate = parse_immediate(op2, BYTE);
            op.command |= 1 << 2;
        }
    }

    else {
        if (isregister(op1)) {
            op.immediate |= parse_register_b(op1);
        } else {
            op.immediate = parse_immediate(op1, BYTE);
            op.command |= 1 << 2;

        }
    }

    return op;
}

static opcode_t parse_line(const char *input) {
    char *mnem;
    char *op[] = {NULL, NULL, NULL};
    char *s = strdup(input);
    char *l = s;
    const char *delim = " \t,;";

    while (isspace(*l))
        ++l;
    mnem = l;

    int op_num = 0;
    while (*l) {
        if (*l == '\'' || *l == '"') {
            char save = *l++;
            while (*l != save)
                ++l;
        }

        while (!strchr(delim, *l))
            ++l;
        if (*l == 0 || *l == ';')
            break;
        *l++ = 0;

        while (strchr(delim, *l)) {
            if (*l == ';')
                goto done;
            ++l;
        }
        if (op_num >= 3)
            err_line("too many operands");
        op[op_num++] = l;
    }
    done:

    opcode_t out = encode_op(mnem, op[0], op[1], op[2]);
    free(s);
    return out;
}

static uint8_t *parse_data(const char *input, size_t *len_out, int pass) {
    uint8_t *out;

    while (isspace(*input))
        ++input;
    if (*input == '"') {
        out = malloc(strlen(input));
        ++input;
        size_t len = 0;
        while (*input && *input != '"') {
            if (*input == '\\') {
                if (!(out[len] = parse_escaped(*++input)))
                    break;
            } else
                out[len] = *input;

            ++input;
            ++len;
        }
        *len_out = len;
    } else if (*input == '\'') {
        out = malloc(1);
        out[0] = input[1];
        *len_out = 1;
    } else if (isdigit(*input)) {
        int64_t n;
        char *s = strdup(input);
        char size = s[strlen(s)-1];
        s[strlen(s)-1] = 0;
        sscanf(s, "%li", &n);
        free(s);
        int length;
        switch (size) {
            case 'b':
                length = 1;
                break;
            case 'd':
                length = 2;
                break;
            case 'q':
                length = 4;
                break;
            default:
                err_line("unrecognized length specifier '%c'", size);
                return NULL;
        }
        out = malloc(length);
        *len_out = length;
        for (int i = 0; i < length; i++)
            out[length-i-1] = (n >> (i*8)) & 0xFF;
    } else {
        *len_out = 2;
        uint16_t d = 0;
        if (pass == 2)
            d = parse_immediate(input, 0xFFFF);
        out = malloc(2);
        out[0] = d >> 8;
        out[1] = d & 0xFF;
    }

    return out;
}

void strip_line(char *line) {
    char *s = line;
    int open_slinegle = 0;
    int open_double = 0;
    while (1) {
        switch (*s) {
            case 0:
                goto end;
            case '\'':
                if (!open_double)
                    open_slinegle = !open_slinegle;
                break;
            case '"':
                if (!open_slinegle)
                    open_double = !open_double;
                break;
            case ';':
                if (!open_slinegle && !open_double) {
                    *s = 0;
                    goto end;
                }
                break;
            default:
                break;
        }
        ++s;
    }
    end:

    while (isspace(*(s-1)))
        --s;
    *s = 0;
}

void assemble(const char **filenames, int n_files) {
    FILE *files[n_files];
    for (int i = 0; i < n_files; i++) {
        if (!(files[i] = fopen(filenames[i], "r")))
            err("failed to open input file '%s': %s", filenames[i], strerror(errno));
    }

    size_t line_size = 0;
    uint16_t saved_address[2];

    for (int pass = 1; pass <= 2; pass++) {
        line_no = 1;
        scope_in = 1;
        filename = filenames[0];
        FILE *file = files[0];
        int file_on = 0;
        int section = SECTION_INSTR;

        uint16_t a = 0;
        uint8_t page = 0;
        while (1) {

            if (getline((char **)&line_orig, &line_size, file) < 0) {
                if (errno)
                    err("failed to read file '%s': %s", filename, strerror(errno));
                else {
                    rewind(file);
                    if (++file_on == n_files)
                        break;

                    file = files[file_on];
                    filename = filenames[file_on];
                    line_no = 1;
                    continue;
                }
            }

            char *s = (char *)line_orig+strlen(line_orig)-1;
            if (*s == '\n')
                *s = 0;

            char *line = strdup(line_orig);
            strip_line(line);

            line_type_t type = line_type(line_orig);
            switch (type) {
                case LINE_EMPTY:
                    if (pass == 2)
                        output(line_orig, OUTPUT_NONE, NULL, 0);
                    ++scope_in;
                    break;

                case LINE_MNEMONIC:
                    if (section == SECTION_INSTR) {
                        if (pass == 2) {
                            opcode_t op = parse_line(line);
                            uint8_t p[2];
                            p[0] = op.immediate;
                            p[1] = op.command;
                            output(line_orig, (page << 17) | (a << 1), p, 2);
                        }
                        ++a;
                    } else {
                        size_t len = 0;
                        uint8_t *p = parse_data(line, &len, pass);
                        if (pass == 2)
                            output(line_orig, ((page << 13) | a) + 0x80000 - 0xC000, p, len);
                        free(p);
                        a += len;
                    }
                    break;

                case LINE_DIRECTIVE: {
                    char *l = strdup(line);
                    char *name = strtok(l, " \t");
                    char *arg1 = strtok(NULL, " \t");
                    char *arg2 = strtok(NULL, " \t");
                    if (!strcasecmp(name, ".org")) {
                        if (arg2)
                            err_line("too many arguments");
                        a = parse_immediate(arg1, DBYTE);
                        if (pass == 2)
                            output(line_orig, OUTPUT_NONE, NULL, 0);
                    } else if (!strcasecmp(name, ".page")) {
                        if (arg2)
                            err_line("too many arguments");
                        page = parse_immediate(arg1, 0x0F);
                        if (pass == 2)
                            output(line_orig, OUTPUT_NONE, NULL, 0);
                    } else if (!strcasecmp(name, ".set")) {
                        uint16_t val = parse_immediate(arg2, DBYTE);
                        if (pass == 1) {
                            add_label(arg1, val);
                        } else if (pass == 2) {
                            uint8_t p[2];
                            p[0] = val >> 8;
                            p[1] = val & 0xFF;
                            output(line_orig, OUTPUT_DATA, p, 2);
                        }
                    } else if (!strcasecmp(name, ".section")) {
                        if (arg2)
                            err_line("too many arguments");

                        saved_address[section] = a;
                        if (!strcasecmp(arg1, "data"))
                            section = SECTION_DATA;
                        else if (!strcasecmp(arg1, "instruction"))
                            section = SECTION_INSTR;
                        else
                            err_line("unknown section '%s'", arg1);
                        a = saved_address[section];

                        if (pass == 2)
                            output(line_orig, OUTPUT_NONE, NULL, 0);
                    } else {
                        err_line("unknown directive '%s'", name);
                    }
                    free(l);
                    break;
                }

                case LINE_LABEL:
                    if (pass == 1)
                        add_label(line, a);
                    else if (pass == 2)
                        output(line_orig, OUTPUT_NONE, NULL, 0);
                    break;

                case LINE_COMMENT:
                    if (pass == 2)
                        output(line_orig, OUTPUT_NONE, NULL, 0);
                default:
                    break;
            }

            ++line_no;
            free(line);
        }

    }
    output_header(labels, n_labels);
}
