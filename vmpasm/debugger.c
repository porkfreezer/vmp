#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <readline/history.h>
#include <readline/readline.h>
#include "ansi-color-codes.h"
#include "vmpemu.h"

bool debug_enabled = true;
int n_breakpoints = 0;
breakpoint_t breakpoints[32] = {{0}};
int n_watchpoints = 0;
uint16_t watchpoints[32] = {0};

static char *disassemble_addr(char *out, uint16_t instr) {
    uint8_t mode = (instr >> 10) & 7;
    int8_t imm = instr & 0xFF;

    *out++ = '[';
    const char *reg = (const char *[]){"","","B","A","X","Y","X|B","Y|A"}[mode];
    out = stpcpy(out, reg);
    int read = 0;
    if (imm && mode > 1 && imm > 0)
        *out++ = '+';
    if (imm || mode <= 1)
        sprintf(out, "%hhd%n", imm, &read);
    out[read] = ']';
    out[read+1] = 0;
    return out+read+1;
}

static char *disassemble_register(char *out, uint8_t code) {
    return stpcpy(out, (const char *[]){"Y","X","B","A"}[code & 3]);
}
static char *disassemble_register_b(char *out, uint8_t code) {
    return stpcpy(out, (const char *[]){"PCL","PCH","PCL","PCH","A","B","X","Y"}[code & 7]);
}

/* Disassemble one instruction to text. Result must be freed */
static char *disassemble(uint16_t instr) {
    uint8_t op = instr >> 8;
    uint8_t imm = instr & 0xFF;

    char *line = malloc(64);

    /* ALU */
    if (op & 0x80) {
        uint8_t alu_op = (op >> 3) & 7;
        const char *mnem = (const char *[]){"mov","or","nand","and","addc","add","subc","sub"}[alu_op];
        char *s = stpcpy(line, mnem);
        if (op & 0x40)
            *s++ = 't';
        for (uint8_t i = s - line; i < 6; i++)
            *s++ = ' ';

        /* Immediate */
        if (op & 4) {
            s = disassemble_register(s, op & 3);
            *s++ = ',';
            sprintf(s, "%hhd ; 0x%02hhx", (int8_t)imm, imm);
        }
        /* Registers */
        else {
            uint8_t ra, rb, rd;
            rd = op & 3;
            ra = imm & 3;
            rb = (imm >> 2) & 7;
            s = disassemble_register(s, rd);
            *s++ = ',';
            if (ra != rd) {
                s = disassemble_register(s, ra);
                *s++ = ',';
            }
            disassemble_register_b(s, rb);
        }
    }
    /* Memory */
    else if (op & 0x40) {
        char *s = stpcpy(line, "mov   ");
        /* Store */
        if (op & 0x20) {
            s = disassemble_addr(s, instr);
            *s++ = ',';
            disassemble_register(s, op & 3);
        }
        /* Load */
        else {
            s = disassemble_register(s, op & 3);
            *s++ = ',';
            disassemble_addr(s, instr);
        }
    }
    /* Jump */
    else {
        uint8_t cond = (op & 3) | ((op >> 3) & 4);
        const char *mnem = (const char *[]){"jmp   ","jz    ","jn    ","jc    ","nop   ","jnz   ","jp    ","jnc   "}[cond];
        char *s = stpcpy(line, mnem);
        disassemble_addr(s, instr);
    }

    return line;
}

static uint32_t parse_address(char *arg) {
    char *s = arg[0] == '@' ? arg+1 : arg;
    uint32_t addr = strtol(s, NULL, 0);
    if (arg[0] == '@')
        addr /= 2;
    return addr;
}

/* https://stackoverflow.com/a/71688263 */
static void reset_input(int a) {
    write(2, "\n", 1);
    rl_replace_line("", 0);
    rl_on_new_line();
    rl_redisplay();
}

void debugger(void) {
    struct timespec debug_start, debug_end;
    clock_gettime(CLOCK_MONOTONIC, &debug_start);
    console_control(CONSOLE_NORMAL);
    void (*old_handler)(int) = signal(SIGINT, reset_input);

    uint32_t location = (app_page << 16) | PC;
    fprintf(stderr, "\nPC: "RED"0x%04x / 0x%04x\n"CRESET, location, location*2);
    fprintf(stderr, "A : %s0x%02x / %d\n"CRESET, reg_changed[A] ? BHRED : BLU, reg[A], reg[A]);
    fprintf(stderr, "B : %s0x%02x / %d\n"CRESET, reg_changed[B] ? BHRED : BLU, reg[B], reg[B]);
    fprintf(stderr, "X : %s0x%02x / %d\n"CRESET, reg_changed[X] ? BHRED : BLU, reg[X], reg[X]);
    fprintf(stderr, "Y : %s0x%02x / %d\n"CRESET, reg_changed[Y] ? BHRED : BLU, reg[Y], reg[Y]);
    fprintf(stderr, "%s%sZ"CRESET" %s%sN"CRESET" %s%sC"CRESET"\n",
            flags[Z] ? BLUB : "", flags_changed[Z] ? BHRED : "",
            flags[N] ? BLUB : "", flags_changed[N] ? BHRED : "",
            flags[C] ? BLUB : "", flags_changed[C] ? BHRED : "");

    memset(reg_changed, 0, sizeof(reg_changed));
    memset(flags_changed, 0, sizeof(flags_changed));

    uint16_t instr = mem.flash[(app_page << 16) | PC];
    char *disassembly = disassemble(instr);
    fprintf(stderr, RED"\n%05x"GRN" %s\n"CRESET, (app_page << 16) | PC, disassembly);
    free(disassembly);

    bool leave = false;
    do {
        char *line = readline(HGRN">> "CRESET);
        HIST_ENTRY *hist = history_get(history_length);
        if (line && *line) {
            if (!hist || strcmp(hist->line, line))
                add_history(line);
        } else {
            if (hist) {
                if (line)
                    free(line);
                line = strdup(hist->line);
            }
        }

        char *argv[16];
        int argc;

        if (line && *line) {
            argv[0] = strtok(line, " ");
            for (argc = 1; argc < 16; argc++) {
                if (!(argv[argc] = strtok(NULL, " ")))
                    break;
            }

            switch (argv[0][0]) {
                case 'c':
                    leave = true;
                    debug_enabled = false;
                    break;
                case 'n':
                    leave = true;
                    break;
                case 'b':
                    if (n_breakpoints == 32) {
                        fprintf(stderr, "Too many breakpoints\n");
                        break;
                    }
                    int n = n_breakpoints++;
                    breakpoints[n].location = parse_address(argv[1]);
                    for (int i = 2; i < argc; i += 2) {
                        breakpoints[n].condition_addrs[i/2-1] = parse_address(argv[i]);
                        breakpoints[n].conditions[i/2-1] = strtol(argv[i+1], NULL, 0);
                    }
                    breakpoints[n].n_conditions = (argc-2)/2;
                    for (int i = 0; i < breakpoints[n].n_conditions; i++) {
                        printf("%05x %02x\n", breakpoints[n].condition_addrs[i], breakpoints[n].conditions[i]);
                    }
                    break;
                case 'w':
                    if (n_watchpoints == 32) {
                        fprintf(stderr, "Too many watchpoints\n");
                        break;
                    }
                    watchpoints[n_watchpoints++] = parse_address(argv[1]);
                    break;
                case 'p':
                    uint32_t a = parse_address(argv[1]);
                    uint32_t count = 1;
                    if (argc > 2)
                        count = strtol(argv[2], NULL, 0);
                    for (uint32_t i = 0; i < count; ) {
                        fprintf(stderr, RED"%05x "GRN, a+i);
                        for (int j = 0; j < 16 && i < count; j++, i++)
                            fprintf(stderr, "%02hhx ", mem.ram[a+i]);
                        fprintf(stderr, "\n"CRESET);
                    }
                    break;
                case 'r':
                    load_program();
                    PC = 0;
                    app_page = 0;
                    leave = true;
                    break;
                case 'q':
                    vmp_exit();
                    break;
                default:
                    fprintf(stderr, "Unrecognized command\n");
                    break;
            }
        }

        free(line);
    } while (!leave);

    console_control(CONSOLE_VMP);
    clock_gettime(CLOCK_MONOTONIC, &debug_end);
    signal(SIGINT, old_handler);
    struct timespec delay = timespec_sub(debug_start, debug_end);
    clock_init = timespec_add(clock_init, delay);
    clock_start = timespec_add(clock_start, delay);
}