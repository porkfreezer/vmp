#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>
#include <errno.h>
#include <argp.h>

#include "ansi-color-codes.h"
#include "vmpasm.h"
#include "output.h"

extern void assemble(const char **, int);

static struct argp_option options[] = {
  {"output",        'o', "FILE",    0, "Output instruction memory to FILE" },
  {"list",          'l', "FILE",    0, "Output listing to FILE" },
  {"header",        'h', "FILE",    0, "Output a list of all global symbols to FILE" },
  {"page",          'p', "FILE",    0, "Output a list of all symbol pages to FILE" },
  { 0 }
};

struct arguments {
    char **in_files;
    size_t n_files;
    char *hex;
    char *list;
    char *header;
    char *pages;
};

static error_t parse_opt(int key, char *arg, struct argp_state *state) {
    struct arguments *arguments = state->input;

    switch (key) {
        case 'o':
            arguments->hex = arg;
            break;
        case 'l':
            arguments->list = arg;
            break;
        case 'h':
            arguments->header = arg;
            break;
        case 'p':
            arguments->pages = arg;
            break;
        case ARGP_KEY_ARG:
            arguments->in_files = realloc(arguments->in_files, (arguments->n_files + 1) * sizeof(char *));
            arguments->in_files[arguments->n_files++] = arg;
        case ARGP_KEY_END:
            if (arguments->n_files < 1)
                err("no input files");
            break;
        default:
            return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

void fail(void) {
    output_cleanup(false);
    exit(EXIT_FAILURE);
}


static const char doc[] = "VMPasm for a Very Mediocre Processor";

static const char args_doc[] = "IN1 IN2 ...";

static struct argp argp = { options, parse_opt, args_doc, doc };

int main(int argc, char **argv) {
    struct arguments arguments = {
        .in_files = NULL,
        .n_files = 0,
        .hex = NULL,
        .list = NULL,
        .header = NULL,
        .pages = NULL,
    };

    argp_parse(&argp, argc, argv, 0, 0, &arguments);

    output_open(arguments.hex, arguments.list, arguments.header, arguments.pages);

    assemble((const char **)arguments.in_files, arguments.n_files);

    output_cleanup(true);
}