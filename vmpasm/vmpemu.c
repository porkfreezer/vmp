#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <termios.h>
#include <time.h>
#include "vmpemu.h"

enum { IDE_ERR = 0, IDE_IDX, IDE_CORR, IDE_DRQ, IDE_DSC, IDE_DF, IDE_RDY, IDE_BSY };

struct mem mem;

#define RAM_PAGED(a) ((a & 0x3FFF) | ((a >> 1) & 0x4000) | ((ram_page & 0xF) << 15))

struct ide {
    uint8_t error;
    uint8_t features;
    uint8_t param;
    uint8_t sector_count;
    union {
        uint32_t a;
        uint8_t bytes[4];
    } addr;
    uint8_t status;

    FILE *file;
    enum {
        IDE_IDLE,
        IDE_READ,
        IDE_WRITE,
    } state;
    uint8_t sector_byte;
};

static struct ide ide;

static ulong num_cycle = 0;
static ulong acia_cycle = 0;
static struct timespec delay = {0, 50000};
struct timespec clock_start, clock_init;

uint32_t ram_page;
uint32_t app_page;
uint16_t PC = 0;
uint8_t reg[4];
uint8_t reg_changed[4];
uint8_t flags[4] = {1, 0, 0, 0};
uint8_t flags_changed[4];

bool timing = false;

void vmp_exit(void) {
    if (ide.file)
        fclose(ide.file);

  #ifdef TIMING_DEBUG
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);

    ulong diff = now.tv_nsec - clock_init.tv_nsec + (now.tv_sec - clock_init.tv_sec)*1000000000UL;
    printf("\n%lu -> %ld / %ld = %ld kHz\n", delay.tv_nsec, num_cycle, diff, (num_cycle*1000000) / diff);
  #endif

    struct termios term;
    tcgetattr(fileno(stdin), &term);
    term.c_lflag |= ICANON;
    term.c_lflag |= ECHO;
    tcsetattr(fileno(stdin), 0, &term);
    exit(0);
}

static void sigint_handler(int n) {
    debug_enabled = true;
}

static void parse_ihex(FILE *f) {
    uint32_t segment = 0;
    uint8_t bytes;
    uint32_t address;
    uint8_t type;
    while (1) {
        fscanf(f, ":%2hhX%4X%2hhX", &bytes, &address, &type);
        switch (type) {
            case 0x00:
                for (int i = 0; i < bytes; i++) {
                    uint32_t a = (segment<<4)+address+i;
                    uint8_t *out = a >= 0x80000 ? mem.rom+a-0x80000 : (uint8_t *)mem.flash + a;
                    fscanf(f, "%2hhX", out);
                }
                break;
            case 0x01:
                return;
            case 0x02:
                fscanf(f, "%4X", &segment);
                break;
            default:
                break;
        }
        fscanf(f, "%2hhX\n", &bytes);
    }
}

static void ide_write(uint8_t a, uint8_t d) {
    switch (a) {
        case 0:
            if (ide.state == IDE_WRITE) {
                fputc(d, ide.file);
                if (!++ide.sector_byte && !--ide.sector_count) {
                    ide.state = IDE_IDLE;
                    ide.status &= ~BIT(IDE_DRQ);
                }
            }
            break;
        case 1:
            if (ide.state == IDE_IDLE)
                ide.features = d;
            break;
        case 2:
            if (ide.state == IDE_IDLE)
                ide.param = d;
            break;
        case 3:
        case 4:
        case 5:
        case 6:
            if (ide.state == IDE_IDLE)
                ide.addr.bytes[a-3] = d;
            break;
        case 7:
            if (ide.state == IDE_IDLE)
              switch (d) {
                case 0x20:
                    ide.state = IDE_READ;
                    fseek(ide.file, (uint64_t)(ide.addr.a & 0x0FFFFFFF) << 8, SEEK_SET);
                    ide.status |= BIT(IDE_DRQ);
                    ide.sector_count = ide.param;
                    break;
                case 0x30:
                    ide.state = IDE_WRITE;
                    fseek(ide.file, (uint64_t)(ide.addr.a & 0x0FFFFFFF) << 8, SEEK_SET);
                    ide.status |= BIT(IDE_DRQ);
                    ide.sector_count = ide.param;
                    break;
                default:
                #ifdef DEBUG
                    printf("unimplemented ATA command 0x%02x\n", d);
                #endif
                    break;
            }
    }
}

static uint8_t ide_read(uint8_t a) {
    switch (a) {
        case 0:
            if (ide.state == IDE_READ) {
                if (!++ide.sector_byte && !--ide.sector_count) {
                    ide.state = IDE_IDLE;
                    ide.status &= ~BIT(IDE_DRQ);
                }
                return fgetc(ide.file);
            }
            break;
        case 1:
            return ide.error;
        case 2:
            return ide.sector_count;
        case 3:
        case 4:
        case 5:
        case 6:
            return ide.addr.bytes[a-3];
        case 7:
            return ide.status;
    }
    return 0;
}

static void acia_write(uint8_t a, uint8_t d) {
    if (a == 0) {
        putchar(d);
        fflush(stdout);
        acia_cycle = num_cycle;
    }
}

static uint8_t acia_read(uint8_t a) {
    int avail;
    ioctl(fileno(stdin), FIONREAD, &avail);

    if (a == 0)
        return avail ? getchar() : 0;
    if (a == 1) {
        bool tdre = (num_cycle - acia_cycle > ACIA_CYCLES_PER_BYTE) || !timing;
        bool rdrf = avail > 0;
        return (tdre << 4) | (rdrf << 3);
    }
    return 0;
}

static void mem_write(uint16_t a, uint8_t d) {
    for (int i = 0; i < n_watchpoints; i++) {
        if (a == watchpoints[i]) {
            printf("{ write 0x%04x 0x%02x }", a, d);
            debug_enabled = true;
        }
    }

    if ((a & 0xC000) == 0xC000) {
        if (a & 0x2000) {
            switch ((a >> 5) & 3) {
                case 0:
                    ram_page = d & 0xF;
                    break;
                case 1:
                    acia_write(a & 3, d);
                    break;
                case 2:
                    ide_write(a & 7, d);
                    break;
                case 3:
                    app_page = d & 3;
                    break;
            }
        }
    } else if ((a & 0xC000) == 0x0000)
        mem.ram[a] = d;
    else
        mem.ram[RAM_PAGED(a)] = d;
}

static uint8_t mem_read(uint16_t a) {
    if ((a & 0xC000) == 0xC000) {
        if (a & 0x2000) {
            switch ((a >> 5) & 3) {
                case 1:
                    return acia_read(a & 3);
                case 2:
                    return ide_read(a & 7);
                default:
                    return rand() & 0xFF;
            }
        } else
            return mem.rom[(a & 0x1FFF) | (app_page << 13)];
    } else if ((a & 0xC000) == 0x0000)
        return mem.ram[a];
    else
        return mem.ram[RAM_PAGED(a)];
}

void console_control(int control) {
    struct termios term;
    tcgetattr(fileno(stdin), &term);
    if (control == CONSOLE_VMP) {
        term.c_lflag &= ~ICANON;
        term.c_lflag &= ~ECHO;
    } else {
        term.c_lflag |= ICANON;
        term.c_lflag |= ECHO;
    }
    tcsetattr(fileno(stdin), 0, &term);
}

static const char *hex_file;
void load_program(void) {
    FILE *f;
    if ((f = fopen(hex_file, "r")) == NULL)
        err("Failed to open %s: %s", hex_file, strerror(errno));
    parse_ihex(f);
    fclose(f);
}

int main(int argc, char **argv) {
    if (argc < 2 || argc > 4) {
        err("USAGE: %s PROGRAM [DISKIMAGE] [TIMING]", argv[0]);
    }

    srand(time(NULL));

    hex_file = argv[1];
    load_program();

    if (argc >= 3) {
        if ((ide.file = fopen(argv[2], "r+")) == NULL)
            err("Failed to open %s: %s", argv[2], strerror(errno));
        ide.status |= BIT(IDE_RDY);
    }

    timing = argc >= 4 || (!ide.file && argc >= 3);

    setbuf(stdin, NULL);
    setbuf(stdout, NULL);

    console_control(CONSOLE_VMP);

    clock_gettime(CLOCK_MONOTONIC, &clock_init);
    clock_start = clock_init;

    signal(SIGINT, sigint_handler);

    while (1) {
        for (int i = 0; i < n_breakpoints; i++) {
            if (breakpoints[i].location == ((app_page << 16) | PC)) {
                debug_enabled = true;
                for (int j = 0; j < breakpoints[i].n_conditions; j++) {
                    if (mem.ram[breakpoints[i].condition_addrs[j]] != breakpoints[i].conditions[j]) {
                        debug_enabled = false;
                        break;
                    }
                }
                if (debug_enabled)
                    break;
            }
        }

        if (debug_enabled)
            debugger();

        uint16_t instr = mem.flash[(app_page << 16) | PC];

        /* ALU */
        if (instr & 0x8000) {
            uint8_t op = (instr >> 11) & 7;
            uint8_t a, b, c, d;
            if (instr & 0x0400) {
                a = reg[(instr >> 8) & 3];
                b = instr & 0xFF;
            } else {
                a = reg[instr & 3];
                if (instr & 0x10)
                    b = reg[~(instr >> 2) & 3];
                else if (instr & 0x4)
                    b = PC >> 8;
                else
                    b = PC & 0xFF;
            }

            if ((op & 6) == 6)
                b = ~b;
            c = (op == 7) || ((~op & 1) && flags[C]);
            switch (op) {
                case 0:     d = b;          break;
                case 1:     d = a | b;      break;
                case 2:     d = ~(a & b);   break;
                case 3:     d = a & b;      break;
                default:    d = a + b + c;  break;
            }

            if (op) {
                flags[Z] = d == 0;
                flags_changed[Z] = 1;
                flags[N] = d >> 7;
                flags_changed[N] = 1;
                if (instr & 0x2000) {
                    flags[C] = ((uint16_t)a + b + c) >> 8;
                    flags_changed[C] = 1;
                }
            }

            if (~instr & 0x4000) {
                uint8_t rd = (instr >> 8) & 3;
                reg[rd] = d;
                reg_changed[rd] = 1;
            }
        }

        else {
            uint8_t amode = (instr >> 10) & 7;
            uint16_t a = 0;

            if (amode & 4)
                a |= (uint16_t)reg[~amode & 1] << 8;
            if (amode & 2)
                a |= reg[2 | (amode & 1)];

            a += (int8_t)instr;

            /* mov */
            if (instr & 0x4000) {
                uint8_t r = (instr >> 8) & 3;
                if (instr & 0x2000) {
                    mem_write(a, reg[r]);
                } else {
                    reg[r] = mem_read(a);
                    reg_changed[r] = 1;
                }
            }

            /* jump */
            else {
                if (flags[(instr >> 8) & 3] != ((instr >> 13) & 1)) {
                    PC = a - 1;
                }
            }
        }

        ++PC;

        ++num_cycle;

        if (timing) {
            if ((num_cycle & 0x7) == 0)
                nanosleep(&delay, NULL);


            if ((num_cycle & 0xFF) == 0) {
                struct timespec now;
                clock_gettime(CLOCK_MONOTONIC, &now);
                long time = now.tv_nsec - clock_start.tv_nsec + (now.tv_sec - clock_start.tv_sec)*1000000000UL;
                int delta = -(time - 256*NSEC_PER_CLOCK) / DELAY_RATE;
                if (delta < 0 && delay.tv_nsec > 0) {
                    if (delay.tv_nsec > -delta)
                        delay.tv_nsec += delta;
                    else
                        delay.tv_nsec = 0;
                } else if (delta > 0) {
                    delay.tv_nsec += delta;
                    if (delay.tv_nsec >= 1000000000UL)
                        delay.tv_nsec = 1000000000UL-1;
                }
                clock_start = now;
            }
        }
    }
}
