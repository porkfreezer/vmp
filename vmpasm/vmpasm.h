#pragma once

#include "ansi-color-codes.h"

#define BYTE  0xff
#define DBYTE 0xffff

#define err(format, ...) do { \
    fprintf(stderr, BRED"error"CRESET": "format"\n", ##__VA_ARGS__); \
    fail(); \
} while (0)

#define warn(format, ...) \
    fprintf(stderr, BYEL"warning"CRESET": "format"\n", ##__VA_ARGS__);

void fail(void);

typedef union {
    uint16_t op;
    struct {
        uint8_t immediate;
        uint8_t command;
    };
} opcode_t;

typedef struct {
    const char *name;
    uint16_t address;
    int scope;
    const char *filename;
    int line;
} label_t;