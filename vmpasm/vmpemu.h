#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <time.h>

#define DELAY_RATE 5000
#define TIMING_DEBUG
//#define DEBUG

#define NSEC_PER_CLOCK 14000
#define ACIA_CYCLES_PER_BYTE (10*1e9/19200)/NSEC_PER_CLOCK

#define err(f, ...) do { \
    fprintf(stderr, f"\n", ##__VA_ARGS__); \
    exit(EXIT_FAILURE); \
} while (0)

#define BIT(x) (1<<x)

extern struct timespec clock_start, clock_init;

extern uint32_t ram_page;
extern uint32_t app_page;
extern uint16_t PC;
extern uint8_t reg[4];
extern uint8_t reg_changed[4];
enum { Y, X, B, A };
extern uint8_t flags[4];
extern uint8_t flags_changed[4];
enum { Z=1, N, C };

struct mem {
    uint16_t flash[1<<18];
    uint8_t rom[1<<16];
    uint8_t ram[1<<19];
};
extern struct mem mem;

extern bool debug_enabled;

typedef struct {
    uint32_t location;
    uint32_t n_conditions;
    uint32_t condition_addrs[32];
    uint8_t  conditions[32];
} breakpoint_t;

extern int n_breakpoints;
extern breakpoint_t breakpoints[32];
extern int n_watchpoints;
extern uint16_t watchpoints[32];

inline struct timespec timespec_sub(struct timespec start, struct timespec end) {
    long s = end.tv_sec - start.tv_sec;
    long ns = end.tv_nsec - start.tv_nsec;
    if (ns < 0) {
        ns += 1000000000UL;
        s -= 1;
    }
    return (struct timespec){s, ns};
}
inline struct timespec timespec_add(struct timespec start, struct timespec end) {
    long s = end.tv_sec + start.tv_sec;
    long ns = end.tv_nsec + start.tv_nsec;
    if (ns >= 1000000000UL) {
        ns -= 1000000000UL;
        s += 1;
    }
    return (struct timespec){s, ns};
}

void vmp_exit(void);

enum { CONSOLE_NORMAL, CONSOLE_VMP };
void console_control(int control);

void load_program(void);

void debugger(void);