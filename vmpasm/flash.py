#!/usr/bin/python

import serial
from time import sleep
from sys import argv

s = serial.Serial('/dev/ttyACM0', timeout=0.1)

def g():
    d = s.read(4096).decode('utf-8')
    print(d, end='')
    return d

def p(m):
    print(m)
    s.write(m)


g()
p(b'type\n' + (b'2\n' if argv[1] == 'd' else b'6\n') )
g()

def hexdump(a, d):
    out = ""
    for i in range(0, len(d), 0x10):
        out += "%05x: " % (a+i)
        for j in range(i, i+0x10):
            c = d[j] if j < len(d) else 0xFF
            out += "%02x " % c
        for j in range(i, i+0x10):
            c = d[j] if j < len(d) else 0xFF
            if c < 0x20 or c > 0x7E:
                out += '.'
            else:
                out += chr(c)
        out += '\r\n'
    return out

def flash(a, d, t):
    g()
    for i in range(0, len(d), 0x100):
        togo = d[i+t:i+t+0x100:2]
        if len(togo) < 0x80:
            togo += b'\xff' * (0x80 - len(togo))
        if togo == b'\xff'*0x80:
           # print(f"skip {i//2:04x}")
            continue
        g()
        p(("r %05x 80\n" % (i//2+a)).encode())
        old = ''
        while len(old) < 11+72*8+11:
            old += g()
        old = old[12:-11]
        new = hexdump(i//2+a, togo)
        if old != new:
            p(("wf %x\n" % (i//2+a)).encode())
            g()
            p(togo)
    g()

def burn_data(a, d):
    g()
    for i in range(0, len(d), 0x40):
        p(("wb %x 40\n" % (i+a)).encode())
        g()
        togo = d[i:i+0x40]
        if len(togo) < 0x40:
            togo += b'\xff' * (0x40 - len(togo))
        p(togo)
        while len(g()) == 0:
            pass
    g()

with open(argv[2], 'rb') as f:
    d = f.read()
    if argv[1] == 'd':
        input("\nInsert data EPROM")
        burn_data(0, d)
    else:
        input("\nInsert immediate Flash")
        flash(0, d, 0)

        input("\nInsert opcode Flash")
        flash(0, d, 1)

print("\nDone!")

try:
    while True:
        g()
except KeyboardInterrupt:
    print("\x1b[2K\r", end='')