.set RAM_PAGE        0xFF80
.set APP_PAGE        0xFFE0

.set ACIA_DATA       0xFFA0
.set ACIA_RESET      0xFFA1
.set ACIA_STATUS     0xFFA1
.set ACIA_COMMAND    0xFFA2
.set ACIA_CONTROL    0xFFA3

.set ACIA_RDRF       0x08
.set ACIA_TDRE       0x10

.set IDE_DATA        0xFFC0
.set IDE_ERROR       0xFFC1
.set IDE_FEATURES    0xFFC1
.set IDE_PARAM       0xFFC2
.set IDE_LBA         0xFFC3
.set IDE_STATUS      0xFFC7
.set IDE_COMMAND     0xFFC7

.set IDE_ERR         0x01
.set IDE_IDX         0x02
.set IDE_ECC         0x04
.set IDE_DRQ         0x08
.set IDE_SKC         0x10
.set IDE_WFT         0x20
.set IDE_RDY         0x40
.set IDE_BSY         0x80

.set IDE_AMNF        0x01
.set IDE_TK0NF       0x02
.set IDE_ABRT        0x04
.set IDE_MCR         0x08
.set IDE_IDNF        0x10
.set IDE_MC          0x20
.set IDE_UNC         0x40

.set IDE_RECAL       0x10
.set IDE_READ        0x20
.set IDE_WRITE       0x30
.set IDE_SEEK        0x70
.set IDE_IDENTIFY    0xEC
.set IDE_SPIN_DOWN   0xE0
.set IDE_SPIN_UP     0xE1
.set IDE_AUTO_OFF    0xE3
.set IDE_AUTO_OFF1   0xF3