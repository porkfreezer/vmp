
start:
   mov   X, STACK_START.hr
   mov   [SP], X
   mov   X, STACK_START.l
   mov   [SP+1], X


   mov   X, RES_END.hr
   mov   B, RES_END.l
   mov   [SDST], X
   mov   [SDST+1], B

   mov   A, @l.l
   mov   B, @l2.l
   mov   Y, @l.hr
   jmp   [push_rp]
@l2:
   mov   Y, msg.hr
   mov   A, msg.l
   jmp   [strcpy]
@l:

   mov   X, @l.hr
   mov   B, @l.l
   mov   [RP], X
   mov   [RP+1], B
   mov   Y, hello.hr
   mov   A, hello.l
   jmp   [print]
@l:

   mov   Y, loop.hr
   mov   A, loop.l
   mov   [RP], Y
   mov   [RP+1], A

loop:

   mov   A, PCL
   mov   Y, PCH
   jmp   [get_key]

   mov   A, [SDST]
   mov   A, [SDST+1]
   sub   A, 2
   mov   [Y|A], B

   mov   Y, RES_END.hr
   mov   A, RES_END.l
   jmp   [print]

.section data

.org 0x8000

hello:
   "Hello world!\n"
msg:
   "You pressed:  \n"