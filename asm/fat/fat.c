#include <stdio.h>
#include <stdint.h>
#include <string.h>

#define FAT_SECTOR      0x0010
#define ROOT_SECTOR     0x0410
#define DATA_SECTOR     0x0410
#define CLUSTER_SECTORS 16
#define SECTOR_SIZE     256
#define CLUSTER_SIZE    CLUSTER_SECTORS*SECTOR_SIZE
#define ROOT_ENTRIES    512

typedef struct {
    uint16_t second2 : 5;
    uint16_t minute  : 6;
    uint16_t hour    : 5;
} __attribute__((packed)) fat_time_t;

typedef struct {
    uint16_t day : 5;
    uint16_t month : 4;
    uint16_t year : 7;
} __attribute__((packed)) fat_date_t;

typedef struct {
    char name[11];
    uint8_t attr;
    uint8_t _reserved;
    uint8_t creation_10ms;
    fat_time_t creation_time;
    fat_date_t creation_date;
    fat_date_t access_date;
    uint16_t _ea;
    fat_time_t modified_time;
    fat_date_t modified_date;
    uint16_t cluster;
    uint32_t length;
} fat_dirent_t;

typedef struct {
    uint16_t start_cluster;
    uint16_t cluster;
    uint16_t cluster_pos;
    uint32_t length;
    uint8_t buf[CLUSTER_SIZE];
} fat_fd_t;

fat_fd_t fd_file;
fat_fd_t fd_dir;
fat_fd_t *fd;
char path_buf[256];
char input[256];
char *path = path_buf;
char *path_end;

FILE *fs;

/*void write_sector(uint32_t lba, const void *data) {
    fseek(fs, lba << 8, SEEK_SET);
    fwrite(data, SECTOR_SIZE, 1, fs);
}*/

void read_sectors(uint32_t lba, void *data, int n) {
#ifdef DEBUG
    printf("\tRead %d sectors from 0x%x00\n", n, lba);
#endif
    fseek(fs, lba << 8, SEEK_SET);
    fread(data, n, SECTOR_SIZE, fs);
}

#define read_cluster() read_sectors(((uint32_t)fd->cluster << 4) + DATA_SECTOR, fd->buf, CLUSTER_SECTORS)

uint16_t next_cluster(void) {
    static uint16_t cache[SECTOR_SIZE/2];
    static uint16_t cache_page = 0xFFFF;
    uint16_t page = fd->cluster & 0xFF80;
    if (page != cache_page) {
        read_sectors(FAT_SECTOR + (page >> 7), cache, 1);
        cache_page = page;
    }
    uint16_t next = cache[fd->cluster & 0x7F];
    fd->cluster = next > 0xFFEF ? 0 : next;
    fd->cluster_pos = 0;
    read_cluster();
}

/* Return 1 to indicate found */
typedef int (*diriter)(fat_dirent_t *);

int iterate_dir(diriter func) {
    fat_dirent_t *f = (fat_dirent_t *)fd->buf;
    for (int i = 0; i < CLUSTER_SIZE/32; i++, f++) {
        if (!f->name[0])
            return 0;
        if ((uint8_t)f->name[0] != 0xE5) {
            if (func(f))
                return 1;
        }
    }
    return 0;
}

#define OPEN_FILE 0x00
#define OPEN_DIR  0x10

char open_name[12];
fat_fd_t *open_fd;
int open_type;

int fat_open_iter(fat_dirent_t *f) {
    for (int i = 10; i >= 0; --i) {
        if (open_name[i] != f->name[i])
            return 0;
    }
    if ((f->attr & 0x10) != open_type)
        return 0;
    open_fd->cluster = f->cluster;
    open_fd->length = f->length;
    return 1;
}

#define FFD (fd == &fd_dir ? "fd_dir" : "fd_file")

int fat_open(uint8_t type) {
#ifdef DEBUG
    printf("\tOpen '%s' to %s\n", path_end, FFD);
#endif

    open_fd = fd;
    fd = &fd_dir;

    char *filename = path_end;
    char *p = path_end;
    while (*p) {
        if (*p++ == '/')
            filename = p;
    }

    p = path_end;

    open_type = OPEN_DIR;

    while (*p) {
        if (p == filename)
            open_type = type;

        int i;
        for (i = 0; i < 11; i++) {
            char c = *p;
            if (c == 0)
                break;
            ++p;
            if (c == '/')
                break;
            open_name[i] = c;
        }
        for ( ; i < 11; i++)
            open_name[i] = ' ';

        #ifdef DEBUG
            printf("\tOpen '%11s'\n", open_name);
        #endif

        if (!iterate_dir(fat_open_iter)) {
#ifdef DEBUG
            printf("\tunable to find %11s in %s\n", open_name, FFD);
#endif
            return 0;
        }

        fd = open_fd;
        read_cluster();
    }

    fd->start_cluster = fd->cluster;
    fd->cluster_pos = 0;
    return 1;
}

void fat_read(void *ptr, uint32_t n) {
    uint8_t *p = ptr;
    uint8_t *end = p + n;
    while (p < end) {
        while (fd->cluster_pos < CLUSTER_SIZE) {
            if (p == end)
                return;
            *p++ = fd->buf[fd->cluster_pos++];
        }

        next_cluster();
    }
}

int list_pos = 0;

int print_dirent(fat_dirent_t *f) {
    printf("%.11s%c ", f->name, (f->attr & 0x10) ? '/' : ' ');
    if (++list_pos >= 6) {
        printf("\n");
        list_pos = 0;
    }
    return 0;
}

int main(int argc, char **argv) {

    setbuf(stdout, NULL);

    fs = fopen(argv[1], "r");

    path[0] = '/';
    path[1] = 0;
    path_end = path+1;
    read_sectors(ROOT_SECTOR, fd_dir.buf, CLUSTER_SECTORS);

    while (1) {
        *path_end = 0;
        printf("%s > ", path);

        char *p = input;
        int arg2 = 0;
        while (1) {
            char c = getchar();
            if (c == '\n') {
                break;
            } else if (c == ' ' && !arg2) {
                *p = 0;
                p = path_end;
                arg2 = 1;
                continue;
            }
            if (c >= 'a' && c <= 'z')
                c &= ~0x20;
            *p++ = c;
        }
        *p = 0;

        if (!strcmp(input, "CD")) {
            fd = &fd_dir;
            if (!fat_open(OPEN_DIR)) {
                printf("failed to open directory\n");
                continue;
            }

            char *s = path;
            char *d = path;
            while (*s) {
                char c = *s;
                if (c == '.') {
                    if (s[1] == '.') {
                        do {
                            --d;
                        } while (d[-1] != '/');
                        if (s[2] == 0)
                            break;
                        s += 3;
                    } else {
                        s += 2;
                    }
                } else {
                    *d++ = c;
                    ++s;
                }
            }

            if (d[-1] != '/')
                *d++ = '/';
            *d = 0;
            path_end = d;
        } else if (!strcmp(input, "LS")) {
            list_pos = 0;
            fd = &fd_file;
            if (!fat_open(OPEN_DIR)) {
                printf("failed to open directory\n");
                continue;
            }
            iterate_dir(print_dirent);
            printf("\n");
        } else if (!strcmp(input, "CAT")) {
            fd = &fd_file;
            if (!fat_open(OPEN_FILE)) {
                printf("failed to open file\n");
                continue;
            }
            char buf[100];
            for (int32_t i = fd->length; i > 0; i -= 100) {
                int l = i > 100 ? 100 : i;
                fat_read(buf, l);
                for (int j = 0; j < l; j++)
                    putchar(buf[j]);
            }
        }
    }
}
