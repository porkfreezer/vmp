.page 0

.set FS_FAT_SECTOR      0x0010
.set FS_ROOT_SECTOR     0x0410
.set FS_DATA_SECTOR     0x0410
.set FS_CLUSTER_SECTORS 16
.set FS_SECTOR_SIZE     256
.set FS_CLUSTER_SIZE    4096

.set RP                 0x00 ; 2
.set FAT_PAGE           0x02 ; 2
.set FD                 0x04 ; 1
.set PATH_END           0x05 ; 1
.set ITER               0x08 ; 4
.set LIST_POS           0x0C ; 1
.set ZSAVE              0x7E ; 2

.set OPEN_NAME          0x10 ; 11
.set OPEN_FD            0x1B ; 1
.set OPEN_TYPE          0x1C ; 1

.set TMP                0x60

.set FD_NAME            0x00 ; 11
.set FD_START_CLUSTER   0x0B ;  2
.set FD_CLUSTER         0x0D ;  2
.set FD_CLUSTER_POS     0x0F ;  2
.set FD_LENGTH          0x11 ;  4

.set DT_NAME            0x00 ; 11
.set DT_ATTR            0x0B ;  1
.set DT_CLUSTER         0x1A ;  2
.set DT_LENGTH          0x1C ;  4

.set FAT_CACHE          0x0100
.set FILEPATH           0x0200
.set INPUT              0x0300

.set FILE_CLUSTER_BUF   0x1000
.set DIR_CLUSTER_BUF    0x3000

.set FD_FILE            FILE_CLUSTER_BUF.hr
.set FD_DIR             DIR_CLUSTER_BUF.hr

.set OPEN_FILE          0x00
.set OPEN_DIR           0x10

.org 0x0000
   mov   Y, start.h
   jmp   [Y|start.l]


; Read FD_CLUSTER to X
fs_read_cluster:
   mov   [RP+1], A
@wait1:
   mov   A, [IDE_STATUS]
   andt  A, IDE_BSY
   jnz   [@wait1]
   andt  A, IDE_RDY
   jz    [@wait1]
   mov   [TMP+16], X
   mov   A, [FD]
   mov   X, [A+FD_CLUSTER]
   mov   B, [A+FD_CLUSTER+1]
   mov   A, 0
   add   B, B
   addc  X, X
   addc  A, A
   add   B, B
   addc  X, X
   addc  A, A
   add   B, B
   addc  X, X
   addc  A, A
   add   B, B
   addc  X, X
   addc  A, A
   add   B, FS_DATA_SECTOR.l
   addc  X, FS_DATA_SECTOR.hr
   addc  A, A
   mov   [IDE_LBA], B
   mov   [IDE_LBA+1], X
   mov   [IDE_LBA+2], A
   mov   A, 0xE0
   mov   [IDE_LBA+3], A
   mov   A, 0x10
   mov   [IDE_PARAM], A
   mov   A, IDE_READ
   mov   [IDE_COMMAND], A
   mov   X, [TMP+16]
   mov   B, 0
@loop:
   mov   A, [IDE_STATUS]
   andt  A, IDE_DRQ
   jz    [@loop]
   mov   A, [IDE_DATA]
   mov   [X|B], A
   add   B, 1
   jnz   [@loop]
   add   X, 1
   mov   A, [TMP+16]
   add   A, 0x10
   cmp   X, A
   jnz   [@loop]
   mov   A, [RP+1]
   jmp   [Y|A]

; find next cluster through FAT
fs_next_cluster:
   mov   [RP], Y
   mov   [RP+1], A
   mov   X, 0
   mov   A, [FD]
   mov   B, [A+FD_CLUSTER]
   add   B, B
   addc  X, 0
   mov   Y, [A+FD_CLUSTER+1]
   addt  Y, Y
   addc  B, 0
   mov   Y, [FAT_PAGE]
   cmp   X, Y
   jnz   [@switch]
   mov   Y, [FAT_PAGE+1]
   cmp   B, Y
   jz    [@noswitch]
@switch:
   mov   [FAT_PAGE], X
   mov   [FAT_PAGE+1], B
   add   B, FS_FAT_SECTOR.l
   addc  X, FS_FAT_SECTOR.hr
   mov   [IDE_LBA], B
   mov   [IDE_LBA+1], X
   mov   B, 0
   mov   [IDE_LBA+2], B
   mov   B, 1
   mov   [IDE_PARAM], B
   mov   B, 0xE0
   mov   [IDE_LBA+3], B
   mov   B, IDE_READ
   mov   [IDE_COMMAND], B
   mov   X, FAT_CACHE.hr
   mov   B, 0
@wait:
   mov   Y, [IDE_STATUS]
   and   Y, IDE_DRQ
   jz    [@wait]
@loop:
   mov   Y, [IDE_DATA]
   mov   [X|B], Y
   add   B, 1
   jnc   [@loop]
@noswitch:
   mov   B, [A+FD_CLUSTER+1]
   add   B, B
   mov   X, FAT_CACHE.hr
   mov   Y, [X|B+1]
   mov   B, [X|B]
   cmp   Y, 0xFF
   jnz   [@ok]
   cmp   B, 0xF0
   jnc   [@ok]
   mov   B, 0
   mov   Y, 0
@ok:
   mov   [A+FD_CLUSTER], Y
   mov   [A+FD_CLUSTER+1], B
   mov   B, 0
   mov   [A+FD_CLUSTER_POS], B
   mov   [A+FD_CLUSTER_POS+1], B
   mov   Y, [RP]
   mov   A, [RP+1]
   jmp   [Y|A]


fs_iterate_dir:
   mov   [RP], Y
   mov   [RP+1], A
   mov   X, [FD]
   mov   B, 0
@loop:
   mov   [ITER+2], X
   mov   [ITER+3], B
   mov   A, [X|B]
   test  A
   mov   Y, @end.h
   jz    [Y|@end.l]
   cmp   A, 0xE5
   jz    [fs_diriter_exit]
;
   mov   Y, [ITER]
   mov   A, [ITER+1]
   jmp   [Y|A]
fs_diriter_exit:
   mov   Y, @end.h
   jnz   [Y|@end.l]
;
   mov   X, [ITER+2]
   mov   B, [ITER+3]
   add   B, 32
   jnc   [@loop]
   add   X, 1
   mov   A, X
   and   A, 0x1F
   cmp   A, 0x1F
   jnz   [@loop]
   sub   A, A
@end:
   mov   Y, [RP]
   mov   A, [RP+1]
   jmp   [Y|A]

fs_open_iter:
   mov   A, 10
   mov   Y, @loop.h
@loop:
   mov   X, [ITER+2]
   mov   B, [ITER+3]
   add   B, A
   mov   B, [X|B]
   mov   X, [A+OPEN_NAME]
   cmp   B, X
   jnz   [Y|@fail.l]
   sub   A, 1
   jc    [Y|@loop.l]
;
   mov   X, [ITER+2]
   mov   B, [ITER+3]
   mov   A, [X|B+DT_ATTR]
   and   A, 0x10
   mov   Y, [OPEN_TYPE]
   cmp   A, Y
   mov   Y, @fail.h
   jnz   [Y|@fail.l]
   mov   A, [OPEN_FD]
   mov   Y, [X|B+DT_CLUSTER]
   mov   [A+FD_CLUSTER+1], Y
   mov   Y, [X|B+DT_CLUSTER+1]
   mov   [A+FD_CLUSTER], Y
   mov   Y, [X|B+DT_LENGTH]
   mov   [A+FD_LENGTH+3], Y
   mov   Y, [X|B+DT_LENGTH+1]
   mov   [A+FD_LENGTH+2], Y
   mov   Y, [X|B+DT_LENGTH+2]
   mov   [A+FD_LENGTH+1], Y
   mov   Y, [X|B+DT_LENGTH+3]
   mov   [A+FD_LENGTH], Y
   mov   A, 1
   sub   A, 0
   mov   Y, fs_diriter_exit.h
   jmp   [Y|fs_diriter_exit.l]
@fail:
   sub   A, A
   mov   Y, fs_diriter_exit.h
   jmp   [Y|fs_diriter_exit.l]


; B=type
; TMP RP
; TMP+3 type
fs_open:
   mov   [TMP], Y
   mov   [TMP+1], A
   mov   [TMP+3], B
   mov   A, [FD]
   mov   [OPEN_FD], A
   mov   A, FD_DIR
   mov   [FD], A

   mov   Y, @loop.h
   mov   X, FILEPATH.hr
   mov   B, [PATH_END]
   mov   [TMP+2], B
@loop:
   mov   A, [X|B]
   test  A
   jz    [Y|@end.l]
   add   B, 1
   cmp   A, '/'
   jnz   [Y|@loop.l]
   mov   [TMP+2], B
   jmp   [Y|@loop.l]
@end:

   mov   B, [PATH_END]
   mov   A, OPEN_DIR
   mov   [OPEN_TYPE], A
   mov   [TMP+5], X
   mov   [TMP+6], B

fs_open_loop:
   mov   X, [TMP+5]
   mov   B, [TMP+6]
   mov   A, [X|B]
   test  A
   jz    [Y|fs_open_end.l]
   mov   A, [TMP+2]
   cmp   A, B
   jnz   [Y|@dir.l]
   mov   A, [TMP+3]
   mov   [OPEN_TYPE], A
@dir:
   mov   A, 0
   mov   [TMP+4], A
@loop:
   mov   A, [X|B]
   cmp   A, 0
   jz    [Y|@end.l]
   add   B, 1
   cmp   A, '/'
   jz    [Y|@end.l]
   mov   Y, A
   mov   A, [TMP+4]
   mov   [A+OPEN_NAME], Y
   add   A, 1
   mov   [TMP+4], A
   cmp   A, 11
   mov   Y, @loop.h
   jnz   [Y|@loop.l]
@end:
   mov   [TMP+5], X
   mov   [TMP+6], B

   mov   A, [TMP+4]
   mov   B, ' '
@loop:
   cmp   A, 11
   jz    [Y|@end.l]
   mov   [A+OPEN_NAME], B
   add   A, 1
   jmp   [Y|@loop.l]
@end:

   mov   A, fs_open_iter.hr
   mov   [ITER], A
   mov   A, fs_open_iter.l
   mov   [ITER+1], A
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   X, fs_iterate_dir.h
   jmp   [X|fs_iterate_dir.l]
@l1:
   mov   Y, @l2.h
   jnz   [Y|@l2.l]
   mov   Y, [TMP]
   mov   A, [TMP+1]
   jmp   [Y|A]
@l2:

   mov   A, [OPEN_FD]
   mov   [FD], A
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   X, [FD]
   jmp   [fs_read_cluster]
@l1:
   mov   Y, fs_open_loop.h
   jmp   [Y|fs_open_loop.l]

fs_open_end:
   mov   A, [FD]
   mov   B, [A+FD_CLUSTER]
   mov   [A+FD_START_CLUSTER], B
   mov   B, [A+FD_CLUSTER+1]
   mov   [A+FD_START_CLUSTER+1], B
   mov   B, 0
   mov   [A+FD_CLUSTER_POS], B
   mov   [A+FD_CLUSTER_POS], B
   add   B, 1
   mov   Y, [TMP]
   mov   A, [TMP+1]
   jmp   [Y|A]

start:
   mov   A, 0x0B
   mov   [ACIA_COMMAND], A
   mov   A, 0x1F
   mov   [ACIA_CONTROL], A
@wait:
   mov   A, [IDE_STATUS]
   andt  A, IDE_BSY
   jnz   [Y|@wait.l]
   andt  A, IDE_RDY
   jz    [Y|@wait.l]

   mov   A, 12
   mov   [IDE_PARAM], A
   mov   A, IDE_AUTO_OFF
   mov   [IDE_COMMAND], A
   mov   A, 0xE0
   mov   [IDE_LBA+3], A

   mov   A, 0xFF
   mov   [FAT_PAGE], A
   mov   [FAT_PAGE+1], A
   mov   A, '/'
   mov   X, FILEPATH.hr
   mov   [X], A
   mov   A, 0
   mov   [X+1], A
   mov   [FD_DIR+FD_CLUSTER], A
   mov   [FD_DIR+FD_CLUSTER+1], A
   mov   A, 1
   mov   [PATH_END], A
   mov   A, FD_DIR
   mov   [FD], A
   mov   [TMP+16], A
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   X, [FD]
   jmp   [fs_read_cluster]
@l1:

loop:
   mov   X, FILEPATH.hr
   mov   B, [PATH_END]
   mov   A, 0
   mov   [X|B], A

   mov   B, 0
   mov   Y, @print.h
@print:
   mov   A, [ACIA_STATUS]
   and   A, ACIA_TDRE
   jz    [Y|@print.l]
   mov   A, [X|B]
   test  A
   jz    [Y|@end.l]
   mov   [ACIA_DATA], A
   add   B, 1
   jmp   [Y|@print.l]
@end:
   mov   A, '>'
   mov   [ACIA_DATA], A

   mov   X, INPUT.hr
   mov   B, 0
@input:
   mov   A, [ACIA_STATUS]
   and   A, ACIA_RDRF
   jz    [Y|@input.l]
   mov   A, [ACIA_DATA]
   cmp   A, '\n'
   jz    [Y|@end.l]
   cmp   A, 0x7F
   jnz   [Y|@notback.l]
   cmp   B, 0
   jz    [Y|@input.l]
   sub   B, 1
@l1:
   mov   A, [ACIA_STATUS]
   and   A, ACIA_TDRE
   jz    [Y|@l1.l]
   mov   A, 0x08
   mov   [ACIA_DATA], A
@l2:
   mov   A, [ACIA_STATUS]
   and   A, ACIA_TDRE
   jz    [Y|@l2.l]
   mov   A, ' '
   mov   [ACIA_DATA], A
@l3:
   mov   A, [ACIA_STATUS]
   and   A, ACIA_TDRE
   jz    [Y|@l3.l]
   mov   A, 0x08
   mov   [ACIA_DATA], A
   jmp   [Y|@input.l]
@notback:
   jc    [Y|@input.l]
   cmp   A, 0x20
   jnc   [Y|@input.l]
   cmp   A, 'a'
   jnc   [Y|@upper.l]
   cmp   A, 'z'+1
   jc    [Y|@upper.l]
   and   A, 0xDF
@upper:
   mov   [ACIA_DATA], A
   mov   [X|B], A
   add   B, 1
   jmp   [Y|@input.l]
@end:
   mov   [ACIA_DATA], A
   mov   A, 0
   mov   [X|B], A

   mov   B, 0
   mov   Y, @loop.h
@loop:
   mov   A, [X|B]
   add   B, 1
   cmp   A, ' '
   jz    [Y|@end.l]
   cmp   A, 0
   jnz   [Y|@loop.l]
   mov   X, FILEPATH.hr
   mov   B, [PATH_END]
   mov   [X|B], A
   mov   Y, input_end.h
   jmp   [Y|input_end.l]
@end:

   mov   [TMP], B
   mov   B, [PATH_END]
   mov   [TMP+1], B
@loop:
   mov   X, INPUT.hr
   mov   B, [TMP]
   mov   A, [X|B]
   add   B, 1
   mov   [TMP], B
   mov   X, FILEPATH.hr
   mov   B, [TMP+1]
   mov   [X|B], A
   add   B, 1
   mov   [TMP+1], B
   cmp   A, 0
   jnz   [Y|@loop.l]

input_end:

   mov   X, INPUT.hr
   mov   A, [X]
   cmp   A, 'C'
   mov   Y, @notc.h
   jnz   [Y|@notc.l]
   mov   A, [X+1]
   mov   Y, cd.h
   cmp   A, 'D'
   jz    [Y|cd.l]
   mov   Y, loop.h
   jmp   [Y|loop.l]
@notc:
   cmp   A, 'L'
   mov   Y, ls.h
   jz    [Y|ls.l]
   cmp   A, 'R'
   mov   Y, read.h
   jz    [Y|read.l]
   cmp   A, 'S'
   mov   Y, serial.h
   jz    [Y|serial.l]
   cmp   A, 'Z'
   mov   Y, loadz.h
   jz    [Y|loadz.l]
   mov   Y, loop.h
   jmp   [Y|loop.l]

cd:
   mov   B, FD_DIR
   mov   [FD], B
   mov   B, OPEN_DIR
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   X, fs_open.h
   jmp   [X|fs_open.l]
@l1:
   mov   Y, loop.h
   jz    [Y|loop.l]
   mov   X, FILEPATH.hr
   mov   B, 0
   mov   [TMP], B
   mov   [TMP+1], B
@loop:
   mov   B, [TMP]
   mov   A, [X|B]
   cmp   A, 0
   mov   Y, @end.h
   jz    [Y|@end.l]
   cmp   A, '.'
   jnz   [Y|@normal.l]
   mov   A, [X|B+1]
   cmp   A, '.'
   jnz   [Y|@dot.l]
@dotdot:
   mov   B, [TMP+1]
@loop2:
   sub   B, 1
   mov   A, [X|B-1]
   cmp   A, '/'
   jnz   [Y|@loop2.l]
   mov   [TMP+1], B
   mov   B, [TMP]
   mov   A, [X|B+2]
   cmp   A, 0
   jz    [Y|@end.l]
   add   B, 3
   mov   [TMP], B
   mov   Y, @loop.h
   jmp   [Y|@loop.l]
@dot:
   add   B, 2
   mov   [TMP], B
   mov   Y, @loop.h
   jmp   [Y|@loop.l]
@normal:
   add   B, 1
   mov   [TMP], B
   mov   B, [TMP+1]
   mov   [X|B], A
   add   B, 1
   mov   [TMP+1], B
   mov   Y, @loop.h
   jmp   [Y|@loop.l]
@end:
   mov   B, [TMP+1]
   mov   A, [X|B-1]
   cmp   A, '/'
   jz    [Y|@slash.l]
   mov   A, '/'
   mov   [X|B], A
   add   B, 1
@slash:
   mov   A, 0
   mov   [X|B], A
   mov   [PATH_END], B
   mov   Y, loop.h
   jmp   [Y|loop.l]

ls:
   mov   A, FD_FILE
   mov   [FD], A
   mov   B, OPEN_DIR
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   X, fs_open.h
   jmp   [X|fs_open.l]
@l1:
   mov   Y, loop.h
   jz    [Y|loop.l]
   mov   A, 0
   mov   [LIST_POS], A

   mov   A, ls_iter.hr
   mov   [ITER], A
   mov   A, ls_iter.l
   mov   [ITER+1], A
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   X, fs_iterate_dir.h
   jmp   [X|fs_iterate_dir.l]
@l1:
   mov   A, '\n'
   mov   [ACIA_DATA], A

   mov   Y, loop.h
   jmp   [Y|loop.l]

ls_iter:
   mov   Y, @loop.h
   mov   [TMP+8], B
@loop:
   mov   A, [ACIA_STATUS]
   and   A, ACIA_TDRE
   jz    [Y|@loop.l]
   mov   B, [TMP+8]
   mov   A, [X|B]
   add   B, 1
   mov   [TMP+8], B
   and   B, 0xF
   cmp   B, 0xC
   jz    [Y|@end.l]
   mov   [ACIA_DATA], A
   jmp   [Y|@loop.l]
@end:

   mov   B, ' '
   and   A, 0x10
   jz    [Y|@file.l]
   mov   B, '/'
@file:
   mov   [ACIA_DATA], B
@wait:
   mov   A, [ACIA_STATUS]
   and   A, ACIA_TDRE
   jz    [Y|@wait.l]
   mov   A, [LIST_POS]
   add   A, 1
   mov   [LIST_POS], A
   cmp   A, 6
   mov   A, ' '
   jnc   [Y|@end.l]
   mov   A, 0
   mov   [LIST_POS], A
   mov   A, '\n'
@end:
   mov   [ACIA_DATA], A
   sub   A, A
   mov   Y, fs_diriter_exit.h
   jmp   [Y|fs_diriter_exit.l]

loadz:
   mov   A, FD_FILE
   mov   [FD], A
   mov   B, OPEN_FILE
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   X, fs_open.h
   jmp   [X|fs_open.l]
@l1:
   mov   Y, loop.h
   jz    [Y|loop.l]
   mov   A, FD_FILE
   mov   B, [A+FD_CLUSTER]
   mov   [ZSAVE], B
   mov   B, [A+FD_CLUSTER+1]
   mov   [ZSAVE+1], B

loadz_opened_entry:
   mov   A, 0
   mov   [TMP], A
@loop:
   mov   A, [TMP]
   add   A, 1
   mov   [TMP], A
   cmp   A, 5     ; 32*4=128
   jz    [jmp_z]
   mov   [RAM_PAGE], A
   mov   X, 0x30
   mov   [TMP+1], X
@loop2:
   mov   X, [TMP+1]
   add   X, 0x10
   mov   [TMP+1], X
   cmp   X, 0xC0
   mov   Y, @loop.h
   jz    [Y|@loop.l]
   mov   A, '#'
   mov   [ACIA_DATA], A
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [fs_read_cluster]
@l1:
   mov   Y, @loop2.hr
   mov   A, @loop2.l
   jmp   [fs_next_cluster]

loadz_opened:
   mov   A, 0xFF
   mov   [FAT_PAGE], A
   mov   [FAT_PAGE+1], A
   mov   A, FD_FILE
   mov   [FD], A
   mov   B, [ZSAVE]
   mov   [A+FD_CLUSTER], B
   mov   B, [ZSAVE+1]
   mov   [A+FD_CLUSTER+1], B
   mov   Y, loadz_opened_entry.h
   jmp   [Y|loadz_opened_entry.l]

read:
   mov   A, FD_FILE
   mov   [FD], A
   mov   B, OPEN_FILE
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   X, fs_open.h
   jmp   [X|fs_open.l]
@l1:
   mov   Y, loop.h
   jz    [Y|loop.l]

   mov   A, [FD_FILE+FD_LENGTH]
   mov   [TMP], A
   mov   A, [FD_FILE+FD_LENGTH+1]
   mov   [TMP+1], A
   mov   A, [FD_FILE+FD_LENGTH+2]
   mov   [TMP+2], A
   mov   A, [FD_FILE+FD_LENGTH+3]
   mov   [TMP+3], A
   mov   A, 22
   mov   [TMP+5], A
@loop:
   mov   X, FD_FILE-1
   mov   B, 0xFF
@loop2:
   mov   Y, @ok.h
   add   B, 1
   addc  X, 0
   cmp   X, FD_FILE+0x10
   mov   Y, @l2_end.h
   jz    [Y|@l2_end.l]
   mov   A, [TMP+3]
   sub   A, 1
   mov   [TMP+3], A
   mov   Y, @ok.h
   jc    [Y|@ok.l]
   mov   A, [TMP+2]
   sub   A, 1
   mov   [TMP+2], A
   jc    [Y|@ok.l]
   mov   A, [TMP+1]
   sub   A, 1
   mov   [TMP+1], A
   jc    [Y|@ok.l]
   mov   A, [TMP]
   sub   A, 1
   mov   [TMP], A
   jnc   [Y|@end.l]
@ok:
   mov   A, [X|B]
   mov   [TMP+4], A
@wait1:
   mov   A, [ACIA_STATUS]
   and   A, ACIA_TDRE
   jz    [Y|@wait1.l]
   mov   A, [TMP+4]
   mov   [ACIA_DATA], A
   cmp   A, '\n'
   jnz   [Y|@loop2.l]
   mov   A, [TMP+5]
   sub   A, 1
   mov   [TMP+5], A
   jnz   [Y|@loop2.l]
@wait2:
   mov   A, [ACIA_STATUS]
   and   A, ACIA_RDRF
   mov   Y, @wait2.h
   jz    [Y|@wait2.l]
   mov   A, [ACIA_DATA]
   cmp   A, 'q'
   mov   Y, loop.h
   jz   [Y|loop.l]
   cmp   A, 'n'
   mov   Y, @skip.h
   jz    [Y|@skip.l]
   mov   A, 'n'-21
@skip:
   sub   A, 'n'-22
   mov   [TMP+5], A
   mov   Y, @loop2.h
   jmp   [Y|@loop2.l]
@l2_end:
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [fs_next_cluster]
@l1:
   mov   Y, @loop.hr
   mov   A, @loop.l
   mov   X, FD_FILE
   jmp   [fs_read_cluster]
@end:

   mov   Y, loop.h
   jmp   [Y|loop.l]

serial:
   mov   Y, @outer.h
   mov   A, 1
   mov   [TMP], A
   mov   [RAM_PAGE], A
@outer:
   mov   X, 0x40
   mov   B, 0
   mov   A, '#'
   mov   [ACIA_DATA], A
@inner:
   mov   A, [ACIA_STATUS]
   and   A, ACIA_RDRF
   jz    [Y|@inner.l]
   mov   A, [ACIA_DATA]
   mov   [X|B], A
   add   B, 1
   jnc   [Y|@inner.l]
   add   X, 1
   cmp   X, 0xC0
   jnz   [Y|@inner.l]
;
   mov   A, [TMP]
   add   A, 1
   mov   [TMP], A
   mov   [RAM_PAGE], A
   cmp   A, 5
   jnz   [Y|@outer.l]
   jmp    [jmp_z]

.org 0xFFFC
   mov   Y, loadz_opened.h
   jmp   [Y|loadz_opened.l]

.org 0xFFFE
jmp_z:
   mov   A, 1
   mov   [APP_PAGE], A
