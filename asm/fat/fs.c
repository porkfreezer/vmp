#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>

#define err(f, ...) do { \
    fprintf(stderr, f"\n", ##__VA_ARGS__); \
    exit(EXIT_FAILURE); \
} while (0)

#define TYPE_FILE 0xAA
#define TYPE_DIR  0xBB
#define TYPE_ROOT 0xCC
#define TYPE_BLANK 0xFF

#define BLOCK_SIZE 1024

#define INODE_BITMAP    0x000001
#define INODES          0x000021
#define DATA_BITMAP     0x010000
#define DATA            0x010800

#define READ 0
#define WRITE 1

/*

  65503 inodes
4177408 blocks * 1024B = 3.98 GiB data

000000 000001 Root
000001 000020 Inode Bitmap
000021 00FFDF Inodes
010000 000800 Data Bitmap
010800 FEF800 Data blocks

*/

typedef uint8_t block_p[3];
#define BLOCK(b) ((b[2] << 16) | (b[1] << 8) | b[0])

typedef uint16_t inode_p;

/*

DIRENT
00 01 type
01 02 inode
03 14 name

*/

typedef struct dirent_t {
    uint8_t magic;
    inode_p inode;
    char name[20];
} dirent_t;

/*

FILE
00 01 type
01 02 next_inode
03 04 length
07 03 blocks * 83

DIRECTORY
00 01 type
01 02 next_inode
03 17 dirent * 11

*/

typedef struct inode_t {
    uint8_t type;
    inode_p next_inode;
    union {
        struct {
            uint32_t length;
            block_p blocks[83];
        } file;
        struct {
            dirent_t entries[11];
        } dir;
    };
} inode_t;

typedef struct fd_t {
    char path[256];
    inode_t inode;
    uint32_t location;
} fd_t;

static FILE *fs;

fd_t fd[2];

inode_t tmpi;
uint8_t *buf = &tmpi;

void write_sector(uint32_t lba, const void *data) {
    fseek(fs, lba << 8, SEEK_SET);
    fwrite(data, 256, 1, fs);
}

void read_sector(uint32_t lba, void *data) {
    fseek(fs, lba << 8, SEEK_SET);
    fread(data, 256, 1, fs);
}

void create_fs(void) {
    memset(&tmpi, 0xFF, 256);
    tmpi.type = TYPE_ROOT;
    tmpi.next_inode = 0;
    write_sector(0, &tmpi);

    memset(buf, 0, 256);
    buf[0] = 0xFF;
    buf[1] = 0xFF;
    buf[2] = 0xFF;
    buf[3] = 0xFF;
    buf[4] = 0x01;
    write_sector(INODE_BITMAP, buf);

    memset(buf, 0, 256);
    for (int i = INODE_BITMAP+1; i < INODES; i++)
        write_sector(i, buf);

    int used_bytes = DATA / (BLOCK_SIZE / 256) / 8;
    memset(buf, 0xFF, 256);
    for (int i = 0; i < used_bytes / 8; i++)
        write_sector(i + DATA_BITMAP, buf);
    memset(buf+256 - (used_bytes & 0xFF), 0, used_bytes & 0xFF);

    memset(buf, 0, 256);
    for (int i = DATA_BITMAP + used_bytes/8 + 1; i < DATA; i++)
        write_sector(i, buf);

}

int check_fs(void) {
    read_sector(0, &tmpi);
    return tmpi.type == TYPE_ROOT;
}

void ls(const char *path) {
    printf("List %s:\n", path);
    read_sector(0, &tmpi);
    while (1) {
        dirent_t *ent = tmpi.dir.entries;
        for (int i = 0; i < 8; i++) {
            if (ent[0].magic == TYPE_FILE)
                printf(" %s\n", ent[0].name);
            else if (ent[0].magic == TYPE_DIR)
                printf(" %s/\n", ent[0].name);
            ++ent;
        }
        uint32_t next = tmpi.next_inode;
        if (next)
            read_sector(next, &tmpi);
        else
            break;
    }
}

void open(const char *filepath, int n, int mode) {
    strcpy(fd[n].path, filepath);

    char *base = fd[n].path+strlen(fd[n].path)-1;
    while (base > fd[n].path && base[-1] != '/')
        --base;

    char *p = strtok(fd[n].path, "/");
    read_sector(0, &fd[n].inode);

    do {
        int expected = p == base ? TYPE_FILE : TYPE_DIR;

        if (fd[n].inode.type == TYPE_FILE)
            err("%s: wrong type", filepath);

        while (1) {
            for (int i = 0; i < 9; i++) {
                dirent_t *e = &fd[n].inode.dir.entries[i];
                if (e->magic == expected && !strcmp(e->name, p)) {
                    read_sector(e->inode, &fd[n].inode);
                    goto found;
                }
            }
            uint32_t next = fd[n].inode.next_inode;
            if (next)
                read_sector(next, &fd[n].inode);
            else if (mode == READ || p != base)
                err("%s: file not found\n", filepath);
        }

    found:
    } while (p != base && (p = strtok(NULL, "/")));

    fd[n].location = 0;
}

void cat(int n) {
    uint32_t l = fd[n].inode.file.length;
    uint8_t buf[256];
    for (uint32_t i = 0; i <= (l >> 8); i++) {
        read_sector(BLOCK(fd[n].inode.file.blocks[i]), buf);
        uint32_t diff = l - (i << 8);
        printf("%.*s", diff > 256 ? 256 : diff, buf);
    }
}

int main(int argc, char **argv) {
    if (argc != 2)
        err("USAGE: %s <IMAGE>", argv[0]);

    if ((fs = fopen(argv[1], "r+")) == NULL)
        err("Failed to open %s: %s", argv[1], strerror(errno));

    if (!check_fs()) {
        create_fs();
        open("/aoeu", WRITE, 0);
        open("/bad/bad", WRITE, 0);
    }

    ls("/");

    open("/aoeu", READ, 0);
    cat(0);

    open("/part2/aoeu", READ, 0);
    cat(0);
}