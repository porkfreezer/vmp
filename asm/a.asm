.org 0x0000

.set LBA 0

start:
   mov   A, 0x0B
   mov   [ACIA_COMMAND], A
   mov   A, 0x1F
   mov   [ACIA_CONTROL], A
   mov   A, '>'
   mov   [ACIA_DATA], A
@wait:
   mov   A, [ACIA_STATUS]
   andt  A, ACIA_RDRF
   jz    [@wait]
   mov   Y, [ACIA_DATA]

@wait:
   mov   A, [IDE_STATUS]
   andt  A, IDE_RDY
   jz    [@wait]

   mov   A, 12
   mov   [IDE_PARAM], A
   mov   A, IDE_AUTO_OFF
   mov   [IDE_COMMAND], A

   sub   Y, '0'
   jz    [test1]
   jmp   [Y]

test1:
   mov   B, [ACIA_STATUS]
   andt  B, ACIA_TDRE
   jz    [test1]
   mov   B, [IDE_DATA]
   mov   [ACIA_DATA], B
   add   A, 1
   jmp   [test1]
