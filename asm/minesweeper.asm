
.set WIN 1
.set LOSE 0x10

   mov   Y, @zero.h
   mov   B, USR
   mov   A, 0
@zero:
   mov   [B], A
   mov   [B+1], A
   mov   [B+2], A
   mov   [B+3], A
   add   B, 4
   jnz   [Y|@zero.l]

   mov   A, 10
   mov   [USR+4], A
@place:
   mov   B, [RAND]
   and   B, 0x77
   or    B, 0x80
   mov   X, [B+1]
   andt  X, 0x10
   jnz   [Y|@place.l]
   mov   X, 0x10
   mov   [B+1], X
;
   andt  B, 0x70 ; z = row == 0
   jz    [Y|@mid]
   mov   X, [B+-16]
   add   X, 1
   mov   [B+-16], X
   mov   X, [B+-15]
   add   X, 1
   mov   [B+-15], X
   mov   X, [B+-14]
   add   X, 1
   mov   [B+-14], X
@mid:
   mov   X, [B]
   add   X, 1
   mov   [B], X
   mov   X, [B+2]
   add   X, 1
   mov   [B+2], X
;
   mov   X, B
   and   X, 0x70
   cmp   X, 0x70 ; z = row == 7
   jz    [Y|@end]
;
   mov   X, [B+16]
   add   X, 1
   mov   [B+16], X
   mov   X, [B+17]
   add   X, 1
   mov   [B+17], X
   mov   X, [B+18]
   add   X, 1
   mov   [B+18], X
@end:
;
   sub   A, 1
   jnz   [Y|@place.l]

loop:

   mov   Y, @l.hr
   mov   [RP], Y
   mov   A, @l.l
   mov   [RP+1], A
   mov   Y, clear.hr
   mov   A, clear.l
   jmp   [print]
@l:

   mov   B, [USR+4]
   mov   X, '0'
   cmp   B, 10
   mov   Y, @lt.h
   jn    [Y|@lt]
   mov   X, '1'
   sub   B, 10
@lt:
   add   B, 0x30
   mov   [OUT], X
   mov   [OUT], B
   mov   B, '\s'
   mov   [OUT], B
   mov   [OUT], B

   mov   A, '0'
   mov   B, '0'
   mov   X, [USR+6]
@h:
   cmp   X, 100
   jn    [Y|@t]
   sub   X, 100
   add   A, 1
   jmp   [Y|@h]
@t:
   cmp   X, 10
   jn    [Y|@o]
   sub   X, 10
   add   B, 1
   jmp   [Y|@t]
@o:
   add   X, '0'
   mov   [OUT], A
   mov   [OUT], B
   mov   [OUT], X

   mov   B, '\n'
   mov   [OUT], B
   mov   [OUT], B

   mov   Y, @l.hr
   mov   [RP], Y
   mov   A, @l.l
   mov   [RP+1], A
   mov   Y, board_start.hr
   mov   A, board_start.l
   jmp   [print]
@l:

   mov   B, WIN
   mov   [USR+3], B
   mov   B, '1'
   mov   [USR+5], B
   mov   B, 0x80
   mov   X, '\s'
   mov   Y, @print.h
@print_line:
   mov   A, [USR+5]
   mov   [OUT], A
   add   A, 1
   mov   [USR+5], A
   mov   A, '|'
   mov   [OUT], A
   mov   [OUT], X
@print:
   mov   A, [B+1]
   andt  A, 0x80
   jz    [Y|@noflag.l]
   mov   A, '+'
   jmp   [Y|@done.l]
@noflag:
   cmp   A, 0x30
   jnz   [Y|@full.l]
   mov   A, '\s'
   jmp   [Y|@done.l]
@full:
   cmp   A, 0x20
   jp    [Y|@done.l]
   andt  A, 0x10
   jnz   [Y|@mine.l]
   mov   A, 0
   mov   [USR+3], A
@mine:
   mov   A, '.'
@done:
   mov   [OUT], A
   mov   [OUT], X
   add   B, 1
   andt  B, 7
   jnz   [Y|@print.l]
   mov   A, '|'
   mov   [OUT], A
   mov   A, '\n'
   mov   [OUT], A
   add   B, 8
   jnz   [Y|@print_line.l]

   mov   Y, @l.hr
   mov   [RP], Y
   mov   A, @l.l
   mov   [RP+1], A
   mov   Y, board_end.hr
   mov   A, board_end.l
   jmp   [print]
@l:

   mov   Y, end.h
   mov   A, [USR+2]
   test  A
   jnz   [Y|end.l]
   mov   A, [USR+3]
   mov   [USR+2], A
   test  A
   jnz   [Y|end.l]

   mov   Y, @l.hr
   mov   A, @l.l
   jmp   [get_key]
@l:
   mov   A, @l3.l
   mov   [USR+1], B
   cmp   B, 0x3a
   jn    [Y|A]
   mov   [OUT], B
   jmp   [get_key]
@l3:
   mov   [OUT], B
   sub   B, '1'
   mov   [USR], B
   mov   Y, @l2.hr
   mov   A, @l2.l
   jmp   [get_key]
@l2:
   mov   Y, loop.h
   mov   X, '\c'
   mov   [OUT], X
   mov   [OUT], B
   sub   B, '1'
   jn    [Y|loop.l]
   cmp   B, 8
   jp    [Y|loop.l]
   mov   X, [USR]
   test  X
   jn    [Y|loop.l]
   cmp   X, 8
   jp    [Y|loop.l]
   add   X, X
   add   X, X
   add   X, X
   add   X, X
   add   B, X
   add   B, 0x81
   mov   X, '\n'
   mov   [OUT], X

   mov   A, [B]
   mov   Y, @open.h
   mov   X, [USR+1]
   cmp   X, 0x3a
   jn    [Y|@open.l]
   mov   X, [USR+4]
   andt  A, 0x80
   jnz   [Y|@flagged.l]
   sub   X, 2
@flagged:
   add   X, 1
   mov   [USR+4], X
   add   A, 0x50
   jmp   [Y|@nomine.l]
@open:
   mov   X, loop.h
   andt  A, 0xa0
   jnz   [X|loop.l]
   andt  A, 0x10
   jz    [Y|@nomine.l]
   mov   A, LOSE
   mov   [USR+2], A
@nomine
   mov   X, [USR+6]
   add   X, 1
   mov   [USR+6], X
   add   A, '0'
   mov   [B], A
   cmp   A, '0'
   mov   Y, loop.h
   jnz   [Y|loop.l]

   mov   A, @l1.l
   jmp   [push]
@l1:
   mov   Y, @finish.hr
   mov   A, @finish.l
   mov   B, blank_start.l
   jmp   [push_rp]
@finish:
   mov   Y, loop.h
   jnz   [Y|loop.l]

open_blank:
   mov   A, @l1.l
   jmp   [peek]
@l1:
   mov   X, B
   and   X, 0x0F
   jz    [pop_rp]
   cmp   X, 9
   jp    [pop_rp]
   andt  B, 0x80
   jz    [pop_rp]
   mov   X, [B]
   andt  X, 0x30
   jnz   [pop_rp]
   add   X, '0'
   mov   [B], X
   cmp   X, '0'
   jnz   [pop_rp]
blank_start:


   mov   A, @l1.l
   jmp   [peek]
@l1:

; NW
   sub   B, 17
   mov   A, @l1.l
   jmp   [push]
@l1:
   mov   B, open_blank.l
   mov   A, @l2.l
   jmp   [push_rp]
@l2:
   mov   A, @l3.l
   jmp   [pop]
@l3:

; N
   add   B, 1
   mov   A, @l1.l
   jmp   [push]
@l1:
   mov   B, open_blank.l
   mov   A, @l2.l
   jmp   [push_rp]
@l2:
   mov   A, @l3.l
   jmp   [pop]
@l3:

; NE
   add   B, 1
   mov   A, @l1.l
   jmp   [push]
@l1:
   mov   B, open_blank.l
   mov   A, @l2.l
   jmp   [push_rp]
@l2:
   mov   A, @l3.l
   jmp   [pop]
@l3:

; W
   add   B, 14
   mov   A, @l1.l
   jmp   [push]
@l1:
   mov   B, open_blank.l
   mov   A, @l2.l
   jmp   [push_rp]
@l2:
   mov   A, @l3.l
   jmp   [pop]
@l3:

; E
   add   B, 2
   mov   A, @l1.l
   jmp   [push]
@l1:
   mov   B, open_blank.l
   mov   A, @l2.l
   jmp   [push_rp]
@l2:
   mov   A, @l3.l
   jmp   [pop]
@l3:

; SW
   add   B, 14
   mov   A, @l1.l
   jmp   [push]
@l1:
   mov   B, open_blank.l
   mov   A, @l2.l
   jmp   [push_rp]
@l2:
   mov   A, @l3.l
   jmp   [pop]
@l3:

; S
   add   B, 1
   mov   A, @l1.l
   jmp   [push]
@l1:
   mov   B, open_blank.l
   mov   A, @l2.l
   jmp   [push_rp]
@l2:
   mov   A, @l3.l
   jmp   [pop]
@l3:

; SE
   add   B, 1
   mov   A, @l1.l
   jmp   [push]
@l1:
   mov   B, open_blank.l
   mov   A, @l2.l
   jmp   [push_rp]
@l2:
   mov   A, @l3.l
   jmp   [pop]
@l3:

   jmp   [pop_rp]

end:
   mov   Y, @l1.hr
   mov   [RP], Y
   mov   A, @l1.l
   mov   [RP+1], A
   mov   Y, gameover.hr
   mov   A, gameover.l
   jmp   [print]
@l1:
   mov   A, lost.l
   mov   X, [USR+2]
   cmp   X, LOSE
   mov   X, @l2.h
   jz    [X|@l2.l]
   mov   A, won.l
@l2:
   mov   X, @l3.l
   mov   [RP+1], X
   jmp   [print]
@l3:
   mov   Y, reset.hr
   mov   A, reset.l
   jmp   [get_key]

.org 0xFF80
push:
   mov   [LTMP], A
   mov   A, B
   mov   X, [SP]
   mov   B, [SP+1]
   sub   B, 1
   subc  X, 0
   mov   [SP], X
   mov   [SP+1], B
   mov   [X|B], A
   mov   A, [LTMP]
   jmp   [Y|A]

pop:
   mov   X, [SP]
   mov   B, [SP+1]
   add   B, 1
   addc  X, 0
   mov   [SP], X
   mov   [SP+1], B
   mov   B, [X|B+-1]
   jmp   [Y|A]

peek:
   mov   X, [SP]
   mov   B, [SP+1]
   mov   B, [X|B+2]
   jmp   [Y|A]

.section data
.org 0x8000
gameover:
   "Game over.\n"
   0b
lost:
   "You lost.\n\n"
   0b
won:
   "You won.\n\n"
   0b

board_start:
   "   1 2 3 4 5 6 7 8\n"
board_end:
   " +-----------------+\n"
   0b

clear:
   27b
   "[2J"
   27b
   "[H"
   0b