.set ZSCII_PAGE 	TMP18
.set ZABBR 			TMP18+1
.set ZCODE			TMP18+2

_zscii_word:
   mov   [TMP20+4], B
   mov   Y, @lines_ok.h
   mov   A, [LINES]
   cmp   A, 20
   jnc   [Y|@lines_ok.l]
   mov   Y, @input_wait.h
   mov   A, ':'
   mov   [ACIA_DATA], A
@input_wait:
   mov   A, [ACIA_STATUS]
   andt  A, ACIA_RDRF
   jz    [Y|@input_wait.l]
   mov   A, [ACIA_DATA]
   mov   A, '\r'
   mov   [ACIA_DATA], A
   mov   A, 0
   mov   [LINES], A
@lines_ok:
   mov   A, [WORDBUF]
   mov   X, [CURPOS]
   add   X, A
   sub   X, 0x7F
   test  B
   mov   Y, @skip.h
   jnz   [Y|@skip.l]
   sub   X, 1
@skip:
   cmp   X, 81
   jnc   [Y|@skipnl.l]
   mov   B, '\n'
   mov   [ACIA_DATA], B
   mov   X, [LINES]
   add   X, 1
   mov   [LINES], X
   mov   X, A
   sub   X, 0x7F
@skipnl:
   mov   [CURPOS], X
   mov   B, 0x80
   mov   [WORDBUF], B
@loop:
   mov   X, [ACIA_STATUS]
   and   X, ACIA_TDRE
   jz    [Y|@loop.l]
   cmp   B, A
   jz    [Y|@loop_end.l]
   mov   X, [B]
   mov   [ACIA_DATA], X
   add   B, 1
   jmp   [Y|@loop.l]
@loop_end:
   mov   B, [TMP20+4]
   cmp   B, '\n'
   jnz   [Y|@skip2.l]
   mov   X, 0
   mov   [CURPOS], X
   mov   X, [LINES]
   add   X, 1
   mov   [LINES], X
@skip2:
   mov   [ACIA_DATA], B
   mov   X, -6
   mov   [ZSCII_PAGE], X
   mov   Y, [TMP20+2]
   mov   A, [TMP20+3]
   jmp   [Y|A]

_zscii_char:
   mov   X, @norm.h
   mov   [TMP+7], B
   mov   B, [ZABBR]
   test  B
   jnz   [X|@abbr.l]
   mov   B, [ZCODE]
   test  B
   jnz   [X|@zcode.l]
   mov   B, [TMP+7]
   test  B
   jnz   [X|@nospace.l]
   mov   B, '\s'
@space:
   mov   [TMP20+2], Y
   mov   [TMP20+3], A
   mov   Y, _zscii_word.h
   jmp   [Y|_zscii_word.l]
@nospace:
   cmp   B, 6
   jp    [X|@norm.l]
   cmp   B, 4
   jn    [X|@set_abbr.l]
   jnz   [X|@l1.l]
   mov   X, 20
   mov   [ZSCII_PAGE], X
   jmp   [Y|A]
@l1:
   mov   X, 46
   mov   [ZSCII_PAGE], X
   jmp   [Y|A]
@norm:
   mov   X, [ZSCII_PAGE]
   add   B, X
   mov   X, ztable.hr
   mov   B, [X|B]
   mov   X, @output.h
   cmp   B, 10
   jz    [X|@space.l]
   cmp   B, 2
   jnz   [X|@output.l]
   mov   X, 0x80
   mov   [ZCODE], X
   mov   X, -6
   mov   [ZSCII_PAGE], X
   jmp   [Y|A]
@output:
   mov   X, B
   mov   B, [WORDBUF]
   mov   [B], X
   add   B, 1
   mov   [WORDBUF], B
   mov   X, -6
   mov   [ZSCII_PAGE], X
   jmp   [Y|A]
@set_abbr:
   mov   [ZABBR], B
   jmp   [Y|A]
@abbr:
   mov   [TMP10], Y
   mov   [TMP10+1], A
   mov   X, [TMP]
   mov   [TMP8], X
   mov   X, [TMP+1]
   mov   [TMP8+1], X
   mov   X, [TMP+2]
   mov   [TMP8+2], X
   mov   X, [TMP+3]
   mov   [TMP8+3], X
   mov   X, [TMP+4]
   mov   [TMP8+4], X
   mov   X, [STR]
   mov   [TMP8+5], X
   mov   X, [STR+1]
   mov   [TMP8+6], X
   mov   X, [STR+2]
   mov   [TMP8+7], X
;
   sub   B, 1
   add   B, B
   add   B, B
   add   B, B
   add   B, B
   add   B, B
   mov   X, [TMP+7]
   add   B, X
   add   B, B
   mov   Y, [ABBR]
   mov   A, [ABBR+1]
   add   A, B
   addc  Y, 0
   mov   B, 1
   mov   [RAM_PAGE], B
   mov   X, [Y|A]
   mov   B, [Y|A+1]
   mov   Y, 0
   add   B, B
   addc  X, X
   addc  Y, 0
   sub   B, 1
   subc  X, 0
   subc  Y, 0
   mov   [STR], Y
   mov   [STR+1], X
   mov   [STR+2], B
   mov   Y, @l2.hr
   mov   A, @l2.l
   mov   X, _print_zscii.h
   jmp   [X|_print_zscii.l]
@l2:
;
   mov   X, -6
   mov   [ZSCII_PAGE], X
   mov   X, 0
   mov   [ZABBR], X
   mov   [ZCODE], X
   mov   X, [TMP8]
   mov   [TMP], X
   mov   X, [TMP8+1]
   mov   [TMP+1], X
   mov   X, [TMP8+2]
   mov   [TMP+2], X
   mov   X, [TMP8+3]
   mov   [TMP+3], X
   mov   X, [TMP8+4]
   mov   [TMP+4], X
   mov   X, [TMP8+5]
   mov   [STR], X
   mov   X, [TMP8+6]
   mov   [STR+1], X
   mov   X, [TMP8+7]
   mov   [STR+2], X
   mov   Y, [TMP10]
   mov   A, [TMP10+1]
   jmp   [Y|A]
@zcode:
   mov   X, @first.h
   jn    [X|@first.l]
   mov   X, [TMP+7]
   add   B, B
   add   B, B
   add   B, B
   add   B, B
   add   B, B
   or    X, B
   mov   B, [WORDBUF]
   mov   [B], X
   add   B, 1
   mov   [WORDBUF], B
   mov   B, 0
   mov   [ZCODE], B
   jmp   [Y|A]
@first:
   mov   B, [TMP+7]
   or    B, 0x40
   mov   [ZCODE], B
   jmp   [Y|A]

; Print STR, return to Y|A
_print_zscii:
   mov   [TMP], Y
   mov   [TMP+1], A
   mov   A, -6
   mov   [ZSCII_PAGE], A
   mov   A, 0
   mov   [ZABBR], A
   mov   [ZCODE], A

_print_zscii_loop:
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [get_s]
@l1:
   mov   [TMP+2], B
   mov   A, @l2.l
   jmp   [get_s]
@l2:
   mov   [TMP+3], B

   mov   B, [TMP+2]
   and   B, 0x7C
   mov   X, shr2.hr
   mov   B, [X|B]
   mov   A, @l2.l
   mov   X, _zscii_char.h
   jmp   [X|_zscii_char.l]
@l2:

   mov   B, [TMP+2]
   and   B, 3
   add   B, B
   add   B, B
   add   A, B, B

   mov   B, [TMP+3]
   mov   X, shr5.hr
   mov   B, [X|B]
   add   B, A
   mov   A, @l2.l
   mov   X, _zscii_char.h
   jmp   [X|_zscii_char.l]
@l2:

   mov   B, [TMP+3]
   and   B, 0x1F
   mov   A, @l2.l
   mov   X, _zscii_char.h
   jmp   [X|_zscii_char.l]
@l2:
   mov   A, [TMP+2]
   andt  A, 0x80
   mov   X, _print_zscii_loop.h
   jz    [X|_print_zscii_loop.l]

   mov   A, 1
   mov   [RAM_PAGE], A
   mov   Y, [TMP]
   mov   A, [TMP+1]
   mov   [TMP20+2], Y
   mov   [TMP20+3], A
   mov   B, 0
   mov   Y, _zscii_word.h
   jmp   [Y|_zscii_word.l]

