.section instruction

jz:
   mov   Y, branch.h
   mov   A, [OP0]
   test  A
   jnz   [Y|dont_branch.l]
   mov   A, [OP0+1]
   test  A
   jnz   [Y|dont_branch.l]
   jmp   [Y|branch.l]

get_sibling:
   mov   B, [OP0+1]
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [object]
@l1:
   mov   Y, 0
   mov   A, [X|B+5]
   mov   [OP0], Y
   mov   [OP0+1], A
   mov   Y, @l2.hr
   mov   A, @l2.l
   mov   X, storei.h
   jmp   [X|storei.l]
@l2:
   mov   B, [OP0+1]
   mov   Y, do_branch.h
   jmp   [Y|do_branch.l]

get_child:
   mov   B, [OP0+1]
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [object]
@l1:
   mov   Y, 0
   mov   A, [X|B+6]
   mov   [OP0], Y
   mov   [OP0+1], A
   mov   Y, @l2.hr
   mov   A, @l2.l
   mov   X, storei.h
   jmp   [X|storei.l]
@l2:
   mov   B, [OP0+1]
   mov   Y, do_branch.h
   jmp   [Y|do_branch.l]

get_parent:
   mov   B, [OP0+1]
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [object]
@l1:
   mov   Y, 0
   mov   A, [X|B+4]
   mov   [OP0], Y
   mov   [OP0+1], A
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   X, storei.h
   jmp   [X|storei.l]

get_prop_len:
   mov   X, [OP0]
   mov   B, [OP0+1]
   mov   Y, @l1.h
   test  B
   jnz   [Y|@l1.l]
   test  X
   jz    [Y|@end.l]
@l1:
   add   X, 0x40
   mov   B, [X|B-1]
   mov   X, shr5.hr
   mov   B, [X|B]
   add   B, 1
@end:
   mov   [OP0+1], B
   mov   B, 0
   mov   [OP0], B
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   X, storei.h
   jmp   [X|storei.l]

inc:
   mov   B, [OP0+1]
   test  B
   mov   X, @var.h
   jnz   [X|@var.l]
   mov   Y, [SP]
   mov   A, [SP+1]
   jmp   [X|@l1.l]
@var:
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [var]
@l1:
   mov   X, [Y|A]
   mov   B, [Y|A+1]
   add   B, 1
   addc  X, 0
   mov   [Y|A], X
   mov   [Y|A+1], B
   mov   Y, main_loop.h
   jmp   [Y|main_loop.l]

dec:
   mov   B, [OP0+1]
   test  B
   mov   X, @var.h
   jnz   [X|@var.l]
   mov   Y, [SP]
   mov   A, [SP+1]
   jmp   [X|@l1.l]
@var:
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [var]
@l1:
   mov   X, [Y|A]
   mov   B, [Y|A+1]
   sub   B, 1
   subc  X, 0
   mov   [Y|A], X
   mov   [Y|A+1], B
   mov   Y, main_loop.h
   jmp   [Y|main_loop.l]

print_addr:
   mov   A, 0
   mov   X, [OP0]
   mov   B, [OP0+1]
   sub   B, 1
   subc  X, 0
   subc  A, 0
   mov   [STR], A
   mov   [STR+1], X
   mov   [STR+2], B
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   jmp   [print_zscii]

remove_obj:
   mov   Y, main_loop.hr
   mov   A, main_loop.l
_remove_obj:
   mov   [TMP], Y
   mov   [TMP+1], A
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   B, [OP0+1]
   jmp   [object]
@l1:
   mov   Y, @end.h
   mov   A, [X|B+4]
   test  A
   jz    [Y|@end.l]
   mov   [TMP+2], A
   mov   A, 0
   mov   [X|B+4], A
   mov   A, [X|B+5]
   mov   [TMP+3], A
   mov   B, [TMP+2]
   mov   Y, @l2.hr
   mov   A, @l2.l
   jmp   [object]
@l2:
   mov   A, [X|B+6]
   mov   Y, [OP0+1]
   cmp   Y, A
   mov   Y, @chain.h
   jnz   [Y|@chain.l]
   mov   A, [TMP+3]
   mov   [X|B+6], A
   jmp   [Y|@end.l]
@chain:
   mov   B, [X|B+6]
   mov   Y, @l3.hr
   mov   A, @l3.l
   jmp   [object]
@l3:
   mov   A, [X|B+5]
   mov   Y, [OP0+1]
   cmp   A, Y
   mov   Y, @lend.h
   jz    [Y|@lend.l]
   mov   B, [X|B+5]
   mov   Y, @l3.hr
   mov   A, @l3.l
   jmp   [object]
@lend:
   mov   A, [TMP+3]
   mov   [X|B+5], A
@end:
   mov   Y, [TMP]
   mov   A, [TMP+1]
   jmp   [Y|A]

print_obj:
   mov   B, [OP0+1]
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [object]
@l1:
   mov   Y, [X|B+7]
   mov   A, [X|B+8]
   mov   B, 0
   mov   [STR], B
   mov   [STR+1], Y
   mov   [STR+2], A
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   jmp   [print_zscii]

jump:
   mov   A, 0
   mov   X, [OP0]
   test  X
   mov   Y, @p.h
   jp    [Y|@p.l]
   mov   A, 0xFF
@p:
   mov   B, [OP0+1]
   sub   B, 2
   subc  X, 0
   subc  A, 0
   mov   Y, [PC+2]
   add   B, Y
   mov   Y, [PC+1]
   addc  X, Y
   mov   Y, [PC]
   addc  A, Y
   mov   [PC], A
   mov   [PC+1], X
   mov   [PC+2], B
   mov   Y, main_loop.h
   jmp   [Y|main_loop.l]

print_paddr:
   mov   A, 0
   mov   X, [OP0]
   mov   B, [OP0+1]
   add   B, B
   addc  X, X
   addc  A, A
   sub   B, 1
   subc  X, 0
   subc  A, 0
   mov   [STR], A
   mov   [STR+1], X
   mov   [STR+2], B
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   jmp   [print_zscii]

load:
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   B, [OP0+1]
   test  B
   jnz   [var]
   mov   Y, [SP]
   mov   A, [SP+1]
@l1:
   mov   X, [Y|A]
   mov   B, [Y|A+1]
   mov   [OP0], X
   mov   [OP0+1], B
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   X, storei.h
   jmp   [X|storei.l]

not:
   mov   X, [OP0]
   mov   B, [OP0+1]
   nand  X, 0xFF
   nand  B, 0xFF
   mov   [OP0], X
   mov   [OP0+1], B
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   X, storei.h
   jmp   [X|storei.l]

.section data
.org 0xC600
run_1op:
   jz
   get_sibling
   get_child
   get_parent
   get_prop_len
   inc
   dec
   print_addr
   main_loop
   remove_obj
   print_obj
   return
   jump
   print_paddr
   load
   not