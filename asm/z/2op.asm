.section instruction

je:
   mov   Y, branch.h
   mov   X, [OP0]
   mov   B, [OP0+1]
@loop:
   mov   A, [N_ARGS]
   sub   A, 1
   mov   [N_ARGS], A
   mov   Y, dont_branch.h
   jz    [Y|dont_branch.l]
   add   A, A
   mov   Y, [A]
   mov   A, [A+1]
   cmp   X, Y
   mov   Y, @loop.h
   jnz   [Y|@loop.l]
   cmp   B, A
   jnz   [Y|@loop.l]
   mov   Y, branch.h
   jmp   [Y|branch.l]


; invert sign bits so that unsigned comparisons are accurate
jl:
   mov   X, [OP0]
   mov   B, [OP0]
   nand  B, 0x80
   or    X, 0x80
   and   X, B
   mov   B, [OP0+1]
; Sorry, OP1 is in A|Y, LSB is easy to access
   mov   A, [OP1]
   mov   Y, [OP1]
   nand  Y, 0x80
   or    A, 0x80
   and   A, Y
   mov   Y, [OP1+1]

   cmp   B, Y
   mov   Y, @notzero.h
   jnz   [Y|@notzero.l]
   cmpc  X, A
   mov   Y, dont_branch.h
   jz    [Y|dont_branch.l]
@notzero:
   mov   Y, [OP1+1]
   cmp   B, Y
   cmpc  X, A
   mov   Y, branch.h
   jnc   [Y|branch.l]
   jmp   [Y|dont_branch.l]

jg:
   mov   X, [OP0]
   mov   B, [OP0]
   nand  B, 0x80
   or    X, 0x80
   and   X, B
   mov   B, [OP0+1]
; Sorry, OP1 is in A|Y, LSB is easy to access
   mov   A, [OP1]
   mov   Y, [OP1]
   nand  Y, 0x80
   or    A, 0x80
   and   A, Y
   mov   Y, [OP1+1]

   cmp   B, Y
   mov   Y, @notzero.h
   jnz   [Y|@notzero.l]
   cmpc  X, A
   mov   Y, dont_branch.h
   jz    [Y|dont_branch.l]
@notzero:
   mov   Y, [OP1+1]
   cmp   B, Y
   cmpc  X, A
   mov   B, 0
   addc  B, 0
   mov   Y, do_branch.h
   jmp   [Y|do_branch.l]

dec_chk:
   mov   B, [OP0+1]
   test  B
   mov   X, @var.h
   jnz   [X|@var.l]
   mov   Y, [SP]
   mov   A, [SP+1]
   jmp   [X|@l1.l]
@var:
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [var]
@l1:
   mov   X, [Y|A]
   mov   B, [Y|A+1]
   sub   B, 1
   subc  X, 0
   mov   [Y|A], X
   mov   [Y|A+1], B
   mov   [OP0], X
   mov   [OP0+1], B
   mov   Y, jl.h
   jmp   [Y|jl.l]

inc_chk:
   mov   B, [OP0+1]
   test  B
   mov   X, @var.h
   jnz   [X|@var.l]
   mov   Y, [SP]
   mov   A, [SP+1]
   jmp   [X|@l1.l]
@var:
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [var]
@l1:
   mov   X, [Y|A]
   mov   B, [Y|A+1]
   add   B, 1
   addc  X, 0
   mov   [Y|A], X
   mov   [Y|A+1], B
   mov   [OP0], X
   mov   [OP0+1], B
   mov   Y, jg.h
   jmp   [Y|jg.l]

jin:
   mov   B, [OP0+1]
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [object]
@l1:
   mov   A, [X|B+4]
   mov   B, [OP1+1]
   mov   Y, dont_branch.h
   cmp   B, A
   jnz   [Y|dont_branch.l]
   jmp   [Y|branch.l]

test:
   mov   X, [OP0]
   mov   B, [OP0+1]
   mov   Y, [OP1]
   mov   A, [OP1+1]
   and   X, Y
   and   B, A
   cmp   X, Y
   mov   Y, dont_branch.h
   jnz   [Y|dont_branch.l]
   cmp   B, A
   jnz   [Y|dont_branch.l]
   jmp   [Y|branch.l]

or:
   mov   X, [OP0]
   mov   B, [OP0+1]
   mov   Y, [OP1]
   mov   A, [OP1+1]
   or    X, Y
   or    B, A
   mov   [OP0], X
   mov   [OP0+1], B
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   X, storei.h
   jmp   [X|storei.l]

and:
   mov   X, [OP0]
   mov   B, [OP0+1]
   mov   Y, [OP1]
   mov   A, [OP1+1]
   and   X, Y
   and   B, A
   mov   [OP0], X
   mov   [OP0+1], B
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   X, storei.h
   jmp   [X|storei.l]

; Get address of attribute in TMP and mask in X. Return to Y|A through TMP+2
get_attr:
   mov   [TMP+2], Y
   mov   [TMP+3], A
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   B, [OP0+1]
   jmp   [object]
@l1:
   mov   A, [OP1+1]
   mov   Y, shr3.hr
   mov   A, [Y|A]
   add   B, A
   addc  X, 0
   mov   [TMP], X
   mov   [TMP+1], B
   mov   A, [OP1+1]
   and   A, 0x07
   mov   X, 7
   sub   A, X, A
   mov   X, 1
   test  A
   mov   Y, @end.h
   jz    [Y|@end.l]
   mov   Y, @loop.h
@loop:
   add   X, X
   sub   A, 1
   jnz   [Y|@loop.l]
@end:
   mov   Y, [TMP+2]
   mov   A, [TMP+3]
   jmp   [Y|A]

test_attr:
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   X, get_attr.h
   jmp   [X|get_attr.l]
@l1:
   mov   Y, [TMP]
   mov   A, [TMP+1]
   mov   B, [Y|A]
   and   B, X
   mov   Y, do_branch.h
   jmp   [Y|do_branch.l]

set_attr:
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   X, get_attr.h
   jmp   [X|get_attr.l]
@l1:
   mov   Y, [TMP]
   mov   A, [TMP+1]
   mov   B, [Y|A]
   or    B, X
   mov   [Y|A], B
   mov   Y, main_loop.h
   jmp   [Y|main_loop.l]

clear_attr:
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   X, get_attr.h
   jmp   [X|get_attr.l]
@l1:
   mov   Y, [TMP]
   mov   A, [TMP+1]
   mov   B, [Y|A]
   nand  X, 0xFF
   and   B, X
   mov   [Y|A], B
   mov   Y, main_loop.h
   jmp   [Y|main_loop.l]

zstore:
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   B, [OP0+1]
   test  B
   jnz   [var]
   mov   Y, [SP]
   mov   A, [SP+1]
@l1:
   mov   X, [OP1]
   mov   B, [OP1+1]
   mov   [Y|A], X
   mov   [Y|A+1], B
   mov   Y, main_loop.h
   jmp   [Y|main_loop.l]

insert_obj:
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   X, _remove_obj.h
   jmp   [X|_remove_obj.l]
@l1:
   mov   B, [OP1+1]
   mov   Y, @l2.hr
   mov   A, @l2.l
   jmp   [object]
@l2:
   mov   A, [X|B+6]
   mov   [TMP], A
   mov   A, [OP0+1]
   mov   [X|B+6], A
   mov   B, [OP0+1]
   mov   Y, @l3.hr
   mov   A, @l3.l
   jmp   [object]
@l3:
   mov   A, [TMP]
   mov   [X|B+5], A
   mov   A, [OP1+1]
   mov   [X|B+4], A
   mov   Y, main_loop.h
   jmp   [Y|main_loop.l]

loadw:
   mov   X, [OP0]
   mov   B, [OP0+1]
   mov   Y, [OP1]
   mov   A, [OP1+1]
   add   A, A
   addc  Y, Y
   add   B, A
   addc  X, Y
   add   X, 0x40
   mov   Y, [X|B]
   mov   A, [X|B+1]
   mov   [OP0], Y
   mov   [OP0+1], A
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   X, storei.h
   jmp   [X|storei.l]

loadb:
   mov   X, [OP0]
   mov   B, [OP0+1]
   mov   Y, [OP1]
   mov   A, [OP1+1]
   add   B, A
   addc  X, Y
   add   X, 0x40
   mov   A, [X|B]
   mov   Y, 0
   mov   [OP0], Y
   mov   [OP0+1], A
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   X, storei.h
   jmp   [X|storei.l]

get_prop:
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   X, _get_prop_addr.h
   jmp   [X|_get_prop_addr.l]
@l1:
   mov   X, @normal.h
   cmp   Y, 0
   jnz   [X|@normal.l]
   mov   X, [OP1]
   mov   B, [OP1+1]
   add   B, B
   addc  X, X
   mov   Y, [OBJECT]
   mov   A, [OBJECT+1]
   add   A, B
   addc  Y, X
   mov   X, [Y|A-64]
   mov   B, [Y|A-63]
   mov   [OP0], X
   mov   [OP0+1], B
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   X, storei.h
   jmp   [X|storei.l]
@normal:
   mov   B, [Y|A-1]
   and   B, 0xE0
   jnz   [X|@word.l]
@byte:
   mov   B, 0
   mov   A, [Y|A]
   jmp   [X|@end.l]
@word:
   mov   B, [Y|A]
   mov   A, [Y|A+1]
@end:
   mov   [OP0], B
   mov   [OP0+1], A
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   X, storei.h
   jmp   [X|storei.l]

get_prop_addr:
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   X, _get_prop_addr.h
   jmp   [X|_get_prop_addr.l]
@l1:
   mov   X, @l2.h
   test  Y
   jnz   [X|@l2.l]
   mov   [OP0], Y
   mov   [OP0+1], Y
   jmp   [X|@end.l]
@l2:
   sub   Y, 0x40
   mov   [OP0], Y
   mov   [OP0+1], A
@end:
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   X, storei.h
   jmp   [X|storei.l]

; Get address of property OP1 of object OP0, in Y|A, return to Y|A through TMP
_get_prop_addr:
   mov   [TMP], Y
   mov   [TMP+1], A
   mov   B, [OP0+1]
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [object]
@l1:
   mov   Y, [X|B+7]
   add   Y, 0x40
   mov   A, [X|B+8]
   mov   B, [Y|A]
   add   B, B
   addc  Y, 0
   add   A, B
   addc  Y, 0
   add   A, 1
   addc  Y, 0

   mov   X, @loop_start.h
   jmp   [X|@loop_start.l]
@loop:
   mov   X, shr5.hr
   mov   B, [Y|A]
   mov   B, [X|B]
   add   B, 2
   add   A, B
   addc  Y, 0
@loop_start:
   mov   B, [Y|A]
   and   B, 0x1F
   mov   X, [OP1+1]
   cmp   B, X
   mov   X, @again.h
   jc    [X|@again.l]
   mov   Y, 0
   jmp   [X|@end.l]
@again:
   jnz   [X|@loop.l]
   add   A, 1
   addc  Y, 0
@end:
   mov   X, [TMP]
   mov   B, [TMP+1]
   jmp   [X|B]

get_next_prop:
   mov   B, [OP1+1]
   test  B
   mov   Y, @l1.h
   jnz   [Y|@l1.l]
   mov   B, [OP0+1]
   mov   Y, @l3.hr
   mov   A, @l3.l
   jmp   [object]
@l3:
   mov   Y, [X|B+7]
   add   Y, 0x40
   mov   A, [X|B+8]
   mov   B, [Y|A]
   add   B, B
   addc  Y, 0
   add   A, B
   addc  Y, 0
   add   A, 1
   addc  Y, 0
   mov   X, @end.h
   jmp   [X|@end.l]
@l1:
   mov   Y, @l2.hr
   mov   A, @l2.l
   mov   X, _get_prop_addr.h
   jmp   [X|_get_prop_addr.l]
@l2:
   mov   X, shr5.hr
   mov   B, [Y|A-1]
   mov   B, [X|B]
   add   B, 1
   add   A, B
   addc  Y, 0
@end:
   mov   B, [Y|A]
   and   B, 0x1F
   mov   [OP0+1], B
   mov   B, 0
   mov   [OP0], B
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   X, storei.h
   jmp   [X|storei.l]

mul:
   mov   A, 0
   mov   [TMP+4], A
   mov   [TMP+5], A
   mov   Y, @op0p.h
   mov   X, [OP0]
   mov   B, [OP0+1]
   andt  X, 0x80
   jp    [Y|@op0p.l]
   mov   A, 0
   sub   B, A, B
   subc  X, A, X
@op0p:
   mov   [TMP], X
   mov   [TMP+1], B
   mov   X, [OP1]
   mov   B, [OP1+1]
   andt  X, 0x80
   jp    [Y|@op1p.l]
   mov   A, 0
   sub   B, A, B
   subc  X, A, X
@op1p:
   mov   [TMP+2], X
   mov   [TMP+3], B
   mov   Y, @loop_start.h
   jmp   [Y|@loop_start.l]
@loop:
   mov   X, shr.hr
   mov   B, [TMP+3]
   mov   B, [X|B]
   mov   Y, @skip.h
   mov   A, [TMP+2]
   andt  A, 1
   jz    [Y|@skip.l]
   or    B, 0x80
@skip:
   mov   Y, shr.hr
   mov   X, [Y|A]
   mov   [TMP+2], X
   mov   [TMP+3], B
   mov   Y, @loop_start.h
   test  X
   jnz   [Y|@loop_start.l]
   test  B
   mov   Y, @loop_end.h
   jz    [Y|@loop_end.l]
@loop_start:
   mov   A, [TMP+1]
   andt  B, 1
   mov   Y, @zero.h
   jz    [Y|@zero.l]
   mov   X, [TMP+4]
   mov   B, [TMP+5]
   mov   Y, [TMP]
   add   B, A
   addc  X, Y
   mov   [TMP+4], X
   mov   [TMP+5], B
@zero:
   mov   Y, [TMP]
   add   A, A
   addc  Y, Y
   mov   [TMP], Y
   mov   [TMP+1], A
   mov   X, [TMP+2]
   mov   B, [TMP+3]
   mov   Y, @loop.h
   jmp   [Y|@loop.l]
@loop_end:
   mov   X, [TMP+4]
   mov   B, [TMP+5]
   mov   A, [OP0]
   and   A, 0x80
   mov   Y, [OP1]
   and   Y, 0x80
   cmp   Y, A
   mov   Y, @pos.h
   jz    [Y|@pos.l]
   mov   A, 0
   sub   B, A, B
   subc  X, A, X
@pos:
   mov   [OP0], X
   mov   [OP0+1], B
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   X, storei.h
   jmp   [X|storei.l]

add:
   mov   X, [OP0]
   mov   B, [OP0+1]
   mov   Y, [OP1]
   mov   A, [OP1+1]
   add   B, A
   addc  X, Y
   mov   [OP0], X
   mov   [OP0+1], B
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   X, storei.h
   jmp   [X|storei.l]

sub:
   mov   X, [OP0]
   mov   B, [OP0+1]
   mov   Y, [OP1]
   mov   A, [OP1+1]
   sub   B, A
   subc  X, Y
   mov   [OP0], X
   mov   [OP0+1], B
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   X, storei.h
   jmp   [X|storei.l]

div:
   mov   B, TMP+4
; TMP:  N
; TMP2: D
; TMP4: Q
; TMP6: R
; B is the location to get result from
_div:
   mov   [TMP8], B
   mov   A, 0
   mov   [TMP+4], A
   mov   [TMP+5], A
   mov   [TMP+6], A
   mov   [TMP+7], A
   mov   [TMP8+1], A
   mov   Y, @op0p.h
   mov   X, [OP0]
   mov   B, [OP0+1]
   andt  X, 0x80
   jp    [Y|@op0p.l]
   mov   A, 0
   sub   B, A, B
   subc  X, A, X
@op0p:
   mov   [TMP], X
   mov   [TMP+1], B
   mov   X, [OP1]
   mov   B, [OP1+1]
   andt  X, 0x80
   jp    [Y|@op1p.l]
   mov   A, 0
   sub   B, A, B
   subc  X, A, X
@op1p:
   mov   [TMP+2], X
   mov   [TMP+3], B
@loop:
   mov   A, [TMP8+1]
   add   A, 1
   cmp   A, 17
   mov   [TMP8+1], A
   mov   Y, @loop_end.h
   jz    [Y|@loop_end.l]
   mov   Y, [TMP+6]
   mov   A, [TMP+7]
   add   A, A
   addc  Y, Y
   mov   [TMP+6], Y
   mov   X, [TMP]
   mov   B, [TMP+1]
   mov   Y, @l1.h
   andt  X, 0x80
   jz    [Y|@l1.l]
   or    A, 1
@l1:
   mov   [TMP+7], A
   add   B, B
   addc  X, X
   mov   [TMP], X
   mov   [TMP+1], B
   mov   Y, [TMP+6]
   mov   X, [TMP+2]
   mov   B, [TMP+3]
   sub   B, A, B
   subc  X, Y, X
   mov   A, 0
   addc  A, 0
   mov   Y, [TMP+5]
   add   Y, Y
   mov   [TMP+5], Y
   mov   Y, [TMP+4]
   addc  Y, Y
   mov   [TMP+4], Y
   mov   Y, @loop.h
   test  A
   jz    [Y|@loop.l]
   mov   [TMP+6], X
   mov   [TMP+7], B
   mov   B, [TMP+5]
   or    B, 1
   mov   [TMP+5], B
   jmp   [Y|@loop.l]
@loop_end:
   mov   A, [TMP8]
   mov   X, [A]
   mov   B, [A+1]
   mov   Y, @skip.h  ; probbably less than 0x80...
   cmp   A, TMP+6
   jz    [Y|@skip.l]
   mov   Y, [OP1]
@skip:
   and   Y, 0x80
   mov   A, [OP0]
   and   A, 0x80
   cmp   Y, A
   mov   Y, @pos.h
   jz    [Y|@pos.l]
   mov   A, 0
   sub   B, A, B
   subc  X, A, X
@pos:
   mov   [OP0], X
   mov   [OP0+1], B
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   X, storei.h
   jmp   [X|storei.l]

mod:
   mov   B, TMP+6
   mov   Y, _div.h
   jmp   [Y|_div.l]

.section data
.org 0xC700
run_2op:
   main_loop
   je
   jl
   jg
   dec_chk
   inc_chk
   jin
   test
   or
   and
   test_attr
   set_attr
   clear_attr
   zstore
   insert_obj
   loadw
   loadb
   get_prop
   get_prop_addr
   get_next_prop
   add
   sub
   mul
   div
   mod
