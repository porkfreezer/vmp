.section instruction
.org 0x0000
reset:
   nop
   nop
   mov   A, 0x0B
   mov   [ACIA_COMMAND], A
   mov   A, 0x1F
   mov   [ACIA_CONTROL], A
   mov   Y, start.h
   mov   A, [RAND]
   test  A
   jnz   [Y|start.l]
   mov   A, 1
   mov   [RAND], A
   jmp   [Y|start.l]

print_zscii:
   mov   X, _print_zscii.h
   jmp   [X|_print_zscii.l]

; Increment STR then retrieve byte in B. Return to Y|A through RP
get_s:
   mov   [RP], Y
   mov   [RP+1], A

   mov   A, [STR]
   mov   X, [STR+1]
   mov   B, [STR+2]
   add   B, 1
   mov   [STR+2], B
   jnc   [_get]
   addc  X, 0
   mov   [STR+1], X
   jnc   [_get]
   addc  A, 0
   mov   [STR], A
   jmp   [_get]

; Increment PC then retrieve byte in B. Return to Y|A through RP
get_i:
   mov   [RP], Y
   mov   [RP+1], A

   mov   A, [PC]
   mov   X, [PC+1]
   mov   B, [PC+2]
   add   B, 1
   mov   [PC+2], B
   jnc   [_get]
   addc  X, 0
   mov   [PC+1], X
   jnc   [_get]
   addc  A, 0
   mov   [PC], A

; Retrieve byte at A|X|B, return to RP
_get:
   addt  X, X
   addc  A, A
   add   A, 1
   mov   [RAM_PAGE], A
   and   X, 0x7F
   add   X, 0x40
   mov   B, [X|B]
;
   mov   Y, [RP]
   mov   A, [RP+1]
   jmp   [Y|A]

; Convert *nonzero* variable reference in B to address in Y|A, return to Y|A through RP
var:
   mov   [RP], Y
   mov   [RP+1], A
   cmp   B, 0x10
   jc    [@global]
   mov   Y, [BP]
   mov   A, [BP+1]
   add   B, B
   sub   A, B
   subc  Y, 0
   jmp   [@end]
@global:
   sub   B, 0x10
   mov   Y, [GLOBAL]
   mov   A, [GLOBAL+1]
   add   A, B
   addc  Y, 0
   add   A, B
   addc  Y, 0
@end:
   mov   X, [RP]
   mov   B, [RP+1]
   jmp   [X|B]

; Store next variable operand to X, return to Y|A
op_var:
   mov   [TMP], Y
   mov   [TMP+1], A
   mov   [TMP+2], X
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [get_i]
@l1:
   test  B
   jnz   [@l2]
   mov   A, @l3.l
   jmp   [pop]
@l3:
   mov   A, [TMP+2]
   mov   [A], X
   mov   [A+1], B
   jmp   [@l5]
@l2:
   mov   A, @l4.l
   jmp   [var]
@l4:
   mov   B, 1
   mov   [RAM_PAGE], B
   mov   X, [Y|A]
   mov   B, [Y|A+1]
   mov   A, [TMP+2]
   mov   [A], X
   mov   [A+1], B
@l5:
   mov   Y, [TMP]
   mov   A, [TMP+1]
   jmp   [Y|A]

; Get next operand of type B to X, return to Y|A
; Doesn't check 3/none
op_parse:
   cmp   B, 2
   jz    [op_var]
   mov   [TMP], Y
   mov   [TMP+1], A
   mov   [TMP+2], X
   cmp   B, 1
   jz    [op_short]

op_long:
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [get_i]
@l1:
   mov   A, [TMP+2]
   mov   [A], B
   mov   A, @l2.l
   jmp   [get_i]
@l2:
   mov   A, [TMP+2]
   mov   [A+1], B
   mov   Y, [TMP]
   mov   A, [TMP+1]
   jmp   [Y|A]

.org 0x0080

start:
   mov   X, start_clear.hr
   mov   B, start_clear.l
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [print]
@l1:

   mov   A, STACK_START.hr
   mov   [SP], A
   mov   A, STACK_START.l
   mov   [SP+1], A

; Load headers to 0page for easy access.
; Add 0x4000 offset for RAM page
; Subtract 1 from PC, since get_i preincrements.
   mov   A, 1
   mov   [RAM_PAGE], A
   mov   Y, 0x40

   mov   B, [Y|HFLAGS.l]
   mov   [FLAGS], B

   mov   X, [Y|HPC_START.l]
   mov   B, [Y|HPC_START.l+1]
   sub   B, 1
   subc  X, 0
   mov   A, 0
   mov   [PC], A
   mov   [PC+1], X
   mov   [PC+2], B

   mov   X, [Y|HDICT.l]
   mov   B, [Y|HDICT.l+1]
   add   X, 0x40
   mov   A, [X|B]
   add   A, 4
   add   B, A
   addc  X, 0
   mov   [DICT], X
   mov   [DICT+1], B

   mov   X, [Y|HOBJECT.l]
   mov   B, [Y|HOBJECT.l+1]
   add   B, 62                ; This skips the property defaults
   addc  X, 0x40              ; get_prop must subtract this
   mov   [OBJECT], X
   mov   [OBJECT+1], B

   mov   X, [Y|HGLOBAL.l]
   mov   B, [Y|HGLOBAL.l+1]
   add   X, 0x40
   mov   [GLOBAL], X
   mov   [GLOBAL+1], B

   mov   X, [Y|HABBR.l]
   mov   B, [Y|HABBR.l+1]
   add   X, 0x40
   mov   [ABBR], X
   mov   [ABBR+1], B

   mov   X, 0x80
   mov   [WORDBUF], X
   mov   X, 0
   mov   [CURPOS], X
   mov   [LINES], X

main_loop:
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [get_i]
@l1:

   mov   [INSTR], B
   mov   Y, long.h
   andt  B, 0x80
   jz    [Y|long.l]
   andt  B, 0x40
   jz    [Y|short.l]

; Variable opcode format
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [get_i]
@l1:
   mov   [INSTR+1], B

   mov   X, -2
   mov   [N_ARGS], X

   mov   B, [INSTR+1]
   mov   X, shr2.hr
   mov   B, [X|B]
   mov   B, [X|B]
   mov   B, [X|B]
   and   B, 3
   mov   Y, op_var_done.h
   cmp   B, 3
   jz    [Y|op_var_done.l]
   mov   X, OP0
   mov   [N_ARGS], X
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [op_parse]
@l1:

   mov   B, [INSTR+1]
   mov   X, shr2.hr
   mov   B, [X|B]
   mov   B, [X|B]
   and   B, 3
   mov   Y, op_var_done.h
   cmp   B, 3
   jz    [Y|op_var_done.l]
   mov   X, OP1
   mov   [N_ARGS], X
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [op_parse]
@l1:

   mov   B, [INSTR+1]
   mov   X, shr2.hr
   mov   B, [X|B]
   and   B, 3
   mov   Y, op_var_done.h
   cmp   B, 3
   jz    [Y|op_var_done.l]
   mov   X, OP2
   mov   [N_ARGS], X
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [op_parse]
@l1:

   mov   B, [INSTR+1]
   and   B, 3
   mov   Y, op_var_done.h
   cmp   B, 3
   jz    [Y|op_var_done.l]
   mov   X, OP3
   mov   [N_ARGS], X
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [op_parse]
@l1:

op_var_done:
   mov   B, [N_ARGS]
   add   B, 2
   mov   X, shr.hr
   mov   B, [X|B]
   mov   [N_ARGS], B
   mov   X, run_2op.hr
   mov   B, [INSTR]
   andt  B, 0x20
   mov   Y, @l1.h
   jz    [Y|@l1.l]
   mov   X, run_var.hr
@l1:
   and   B, 0x1F
   add   B, B
   mov   Y, [X|B]
   mov   A, [X|B+1]
   mov   B, 1
   mov   [RAM_PAGE], B
   jmp   [Y|A]

; Long opcode format
long:
   mov   X, 2
   mov   [N_ARGS], X
   mov   X, OP0
   mov   Y, @op1.hr
   mov   A, @op1.l
   andt  B, 0x40
   jnz   [op_var]
;
@short0:
   mov   B, 0
   mov   [OP0], B
   mov   A, @l1.l
   jmp   [get_i]
@l1:
   mov   [OP0+1], B
;
@op1:
   mov   A, @run.l
   mov   X, OP1
   mov   B, [INSTR]
   andt  B, 0x20
   jnz   [op_var]
;
@short1:
   mov   B, 0
   mov   [OP1], B
   mov   A, @l2.l
   jmp   [get_i]
@l2:
   mov   [OP1+1], B
@run:
   mov   B, [INSTR]
   and   B, 0x1F
   add   B, B
   mov   X, run_2op.hr
   mov   Y, [X|B]
   mov   A, [X|B+1]
   mov   B, 1
   mov   [RAM_PAGE], B
   jmp   [Y|A]


short:
   mov   X, shr2.hr
   mov   B, [X|B]
   mov   B, [X|B]
   and   B, 3
   cmp   B, 3
   jz    [Y|@0op.l]
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   X, OP0
   jmp   [op_parse]
@l1:
   mov   B, [INSTR]
   and   B, 0x0F
   add   B, B
   mov   X, run_1op.hr
   mov   Y, [X|B]
   mov   A, [X|B+1]
   mov   B, 1
   mov   [RAM_PAGE], B
   jmp   [Y|A]
@0op:
   mov   B, [INSTR]
   and   B, 0x0F
   add   B, B
   mov   X, run_0op.hr
   mov   Y, [X|B]
   mov   A, [X|B+1]
   mov   B, 1
   mov   [RAM_PAGE], B
   jmp   [Y|A]

; Get reference and store OP0 return to Y|A through TMP10
storei:
   mov   [TMP10], Y
   mov   [TMP10+1], A
   mov   Y, store.hr
   mov   A, store.l
   jmp   [get_i]

; Store OP0 to B, return to TMP10
store:
   mov   A, 1
   mov   [RAM_PAGE], A
   test  B
   jnz   [Y|@normal.l]
   mov   X, [OP0]
   mov   B, [OP0+1]
   mov   Y, [TMP10]
   mov   A, [TMP10+1]
   jmp   [push]
@normal:
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [var]
@l1:
   mov   X, [OP0]
   mov   B, [OP0+1]
   mov   [Y|A], X
   mov   [Y|A+1], B
   mov   Y, [TMP10]
   mov   A, [TMP10+1]
   jmp   [Y|A]

; Branch
branch:
   mov   B, 1
   jmp   [Y|do_branch.l]
; Get offset, don't branch
dont_branch
   mov   B, 0

; Get offset and branch with condition B. Returns to main_loop
do_branch:
   mov   [TMP], B
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [get_i]
@l1:
   mov   [TMP+1], B
   andt  B, 0x40
   jnz   [Y|@onebyte.l]
   mov   A, @l2.l
   jmp   [get_i]
@l2:
   mov   X, [TMP+1]
   and   X, 0x3F
   andt  X, 0x20
   jz    [Y|@finish.l]
   or    X, 0xC0
   jmp   [Y|@finish.l]
@onebyte:
   mov   X, 0
   and   B, 0x3F
@finish:
   mov   A, [TMP+1]
   andt  A, 0x80
   mov   Y, @not.h
   jz    [Y|@not.l]
   mov   A, [TMP]
   mov   Y, main_loop.h
   test  A
   jz    [Y|main_loop.l]
   mov   Y, @branch.h
   jmp   [Y|@branch.l]
@not:
   mov   A, [TMP]
   mov   Y, main_loop.h
   test  A
   jnz   [Y|main_loop.l]
@branch:
   mov   Y, @normal.h
   cmp   X, 0
   jnz   [Y|@normal.l]
   cmp   B, 2
   jc    [Y|@normal.l]
   mov   [OP0], X
   mov   [OP0+1], B
   mov   Y, return.h
   jmp   [Y|return.l]
@normal:
   mov   A, 0
   sub   B, 2
   subc  X, 0
   mov   Y, @s.h
   jp    [Y|@s.l]
   mov   A, 0xFF
@s:
   mov   Y, [PC+2]
   add   Y, B
   mov   [PC+2], Y
   mov   Y, [PC+1]
   addc  Y, X
   mov   [PC+1], Y
   mov   Y, [PC]
   addc  Y, A
   mov   [PC], Y
   mov   Y, main_loop.h
   jmp   [Y|main_loop.l]

; Return value stored in OP0. Returns to main_loop
return:
   mov   X, [BP]
   mov   B, [BP+1]
   mov   [SP], X
   mov   [SP+1], B
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [pop]
@l1:
   mov   [BP], X
   mov   [BP+1], B
   mov   A, @l2.l
   jmp   [pop]
@l2:
   mov   [PC], X
   mov   [TMP], B
   mov   A, @l3.l
   jmp   [pop]
@l3:
   mov   [PC+1], X
   mov   [PC+2], B

   mov   B, [TMP]
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   [TMP10], Y
   mov   [TMP10+1], A
   mov   Y, store.h
   jmp   [Y|store.l]

ill:
   mov   A, [INSTR]
   mov   [ACIA_DATA], A
   mov   A, [PC]
   mov   [ACIA_DATA], A
   mov   A, [PC+1]
   mov   [ACIA_DATA], A
   mov   A, [PC+2]
   mov   [ACIA_DATA], A
   mov   A, '~'
   mov   [ACIA_DATA], A
   mov   Y, @end.h
@end:
   jmp   [Y|@end.l]

save:

.org 0xFF80

; Get address of object number B to X|B, return to Y|A through RP
object:
   mov   [RP], Y
   mov   [RP+1], A
   sub   B, 1
   mov   Y, 0
   add   A, B, B  ; Y|A = B*2
   addc  Y, 0
   add   A, A
   addc  Y, Y     ; Y|A = B*4
   add   A, A
   addc  Y, Y     ; Y|A = B*8
   add   A, B
   addc  Y, 0     ; Y|A = B*9
   mov   X, [OBJECT]
   mov   B, [OBJECT+1]
   add   B, A
   addc  X, Y
   mov   Y, [RP]
   mov   A, [RP+1]
   jmp   [Y|A]

; Push X|B, return to Y|A
push:
   mov   [RP], Y
   mov   [RP+1], A
   mov   Y, [SP]
   mov   A, [SP+1]
   sub   A, 2
   subc  Y, 0
   mov   [SP], Y
   mov   [SP+1], A
   mov   [Y|A], X
   mov   [Y|A+1], B
   mov   Y, [RP]
   mov   A, [RP+1]
   jmp   [Y|A]

; Pop into X|B, return to Y|A
pop:
   mov   [RP], Y
   mov   [RP+1], A
   mov   Y, [SP]
   mov   A, [SP+1]
   mov   X, [Y|A]
   mov   B, [Y|A+1]
   add   A, 2
   addc  Y, 0
   mov   [SP], Y
   mov   [SP+1], A
   mov   Y, [RP]
   mov   A, [RP+1]
   jmp   [Y|A]

; Print X|B, return to Y|A
print:
   mov   [RP], A
@loop:
   mov   A, [ACIA_STATUS]
   and   A, 0x10
   jz    [@loop]
   mov   A, [X|B]
   test  A
   jz    [@end]
   mov   [ACIA_DATA], A
   add   B, 1
   addc  X, 0
   jmp   [@loop]
@end:
   mov   A, [RP]
   jmp   [Y|A]

op_short:
   mov   B, 0
   mov   A, [TMP+2]
   mov   [A], B
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [get_i]
@l1:
   mov   A, [TMP+2]
   mov   [A+1], B
   mov   Y, [TMP]
   mov   A, [TMP+1]
   jmp   [Y|A]

.org 0xFFFA
restart:
   mov   A, 0
   mov   [APP_PAGE], A

.org 0xFFFE
quit:
   mov   A, 0
   mov   [APP_PAGE], A

.org save
