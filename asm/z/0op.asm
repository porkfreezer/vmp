.section instruction

rtrue:
   mov   A, 0
   mov   [OP0], A
   mov   A, 1
   mov   [OP0+1], A
   mov   Y, return.h
   jmp   [Y|return.l]

rfalse:
   mov   A, 0
   mov   [OP0], A
   mov   [OP0+1], A
   mov   Y, return.h
   jmp   [Y|return.l]

zprint:
   mov   A, [PC]
   mov   X, [PC+1]
   mov   B, [PC+2]
   mov   [STR], A
   mov   [STR+1], X
   mov   [STR+2], B
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [print_zscii]
@l1:
   mov   A, [STR]
   mov   X, [STR+1]
   mov   B, [STR+2]
   mov   [PC], A
   mov   [PC+1], X
   mov   [PC+2], B
   mov   Y, main_loop.h
   jmp   [Y|main_loop.l]

print_ret:
   mov   A, [PC]
   mov   X, [PC+1]
   mov   B, [PC+2]
   mov   [STR], A
   mov   [STR+1], X
   mov   [STR+2], B
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [print_zscii]
@l1:
   mov   A, [STR]
   mov   X, [STR+1]
   mov   B, [STR+2]
   mov   [PC], A
   mov   [PC+1], X
   mov   [PC+2], B
   mov   Y, @wait.h
@wait:
   mov   A, [ACIA_STATUS]
   and   A, ACIA_TDRE
   jz    [Y|@wait.l]
   mov   A, '\n'
   mov   [ACIA_DATA], A
   mov   A, [LINES]
   add   A, 1
   mov   [LINES], A
   mov   X, 0
   mov   [CURPOS], X
   mov   B, 1
   mov   [OP0], X
   mov   [OP0+1], B
   mov   Y, return.h
   jmp   [Y|return.l]

ret_popped:
   mov   A, @l1.l
   jmp   [pop]
@l1:
   mov   [OP0], X
   mov   [OP0+1], B
   mov   Y, return.h
   jmp   [Y|return.l]

zpop:
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   jmp   [pop]

new_line:
   mov   A, '\n'
   mov   [ACIA_DATA], A
   mov   A, 0
   mov   [CURPOS], A
   mov   A, [LINES]
   add   A, 1
   mov   [LINES], A
   mov   Y, main_loop.h
   jmp   [Y|main_loop.l]

.section data
.org 0xC500
run_0op:
   rtrue
   rfalse
   zprint
   print_ret
   main_loop   ; nop
   main_loop   ; save
   main_loop   ; restore
   restart
   ret_popped
   zpop
   quit
   new_line
   show_status
   branch
