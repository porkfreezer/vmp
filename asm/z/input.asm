.section instruction

show_status:
   mov   [TMP20+8], B   ; save to check where to return
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   X, status_pre.hr
   mov   B, status_pre.l
   jmp   [print]           ; Go to top of screen, change background
@l1:

   mov   X, [GLOBAL]
   mov   B, [GLOBAL+1]
   mov   B, [X|B+1]
   mov   A, @l1.l
   jmp   [object]    ; Get address of first global
@l1:
   mov   Y, [X|B+7]  ; Z address of first global name
   mov   A, [X|B+8]
   mov   B, 0
   mov   [STR], B
   mov   [STR+1], Y
   mov   [STR+2], A
   mov   [CURPOS], B ; reset cursor so no line breaks
   mov   Y, @l2.hr
   mov   A, @l2.l
   jmp   [print_zscii]  ; Print room name
@l2:

   mov   Y, _score_game.h
   mov   A, [FLAGS]
   andt  A, 0x02
   jz    [Y|_score_game.l]
   mov   X, status_time.hr
   mov   B, status_time.l
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [print]
@l1:

   mov   Y, @tens.h
   mov   X, [GLOBAL]
   mov   B, [GLOBAL+1]
   mov   A, [X|B+3]     ; second global holds hours
   mov   B, 0x2F
@tens:
   add   B, 1
   sub   A, 10
   jc    [Y|@tens.l]
   add   A, 10+'0'
   mov   [ACIA_DATA], B
@wait:
   mov   B, [ACIA_STATUS]
   and   B, ACIA_TDRE
   jz    [Y|@wait.l]
   mov   [ACIA_DATA], A

   mov   A, ':'
@wait:
   mov   B, [ACIA_STATUS]
   and   B, ACIA_TDRE
   jz    [Y|@wait.l]
   mov   [ACIA_DATA], A

   mov   X, [GLOBAL]
   mov   B, [GLOBAL+1]
   mov   A, [X|B+5]     ; third global holds minutes
   mov   B, 0x2F
@tens:
   add   B, 1
   sub   A, 10
   jc    [Y|@tens.l]
   add   A, 10+'0'
   mov   [ACIA_DATA], B
@wait:
   mov   B, [ACIA_STATUS]
   and   B, ACIA_TDRE
   jz    [Y|@wait.l]
   mov   [ACIA_DATA], A
   jmp   [Y|_status_complete.l]


_score_game:
   mov   X, status_score.hr
   mov   B, status_score.l
   mov   A, @l1.l
   jmp   [print]
@l1:
   mov   X, [GLOBAL]
   mov   B, [GLOBAL+1]
   mov   A, [X|B+2]     ; second global holds score
   mov   [OP0], A
   mov   A, [X|B+3]
   mov   [OP0+1], A
   mov   Y, @l2.hr
   mov   A, @l2.l
   mov   B, 0
   mov   X, print_num.h
   jmp   [X|print_num.l]
@l2:

   mov   X, status_moves.hr
   mov   B, status_moves.l
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [print]
@l1:
   mov   X, [GLOBAL]
   mov   B, [GLOBAL+1]
   mov   A, [X|B+4]     ; third global holds turns
   mov   [OP0], A
   mov   A, [X|B+5]
   mov   [OP0+1], A
   mov   Y, @l2.hr
   mov   A, @l2.l
   mov   X, print_num.h
   jmp   [X|print_num.l]
@l2:

_status_complete:
   mov   X, @l2.h
   mov   B, [TMP20+8]   ; if B=0 then printing status before input
   test  B
   jnz   [X|@l3.l]
   mov   Y, _input.hr
   mov   A, _input.l
   jmp   [X|@l2.l]
@l3:
   mov   Y, main_loop.hr
   mov   A, main_loop.l
@l2:
   mov   X, status_post.hr
   mov   B, status_post.l
   jmp   [print]

input:
   mov   X, [OP0]
   mov   B, [OP0+1]
   mov   [TMP20+6], X
   mov   [TMP20+7], B
   mov   B, 0
   mov   Y, show_status.h
   jmp   [Y|show_status.l]

_input:

; TMP:     buf
; TMP+2:   wstart
; TMP+4:   parse
; TMP+6:   wend
; TMP8:    len
; TMP8+1:  n_words

   mov   X, [TMP20+6]
   mov   B, [TMP20+7]
   add   B, 1
   addc  X, 0x40
   mov   [TMP], X
   mov   [TMP+1], B
   mov   A, 0
   mov   [TMP+2], A
   mov   [LINES], A
   mov   Y, @loop.h
@loop:
   mov   A, [ACIA_STATUS]
   and   A, 0x08
   jz    [Y|@loop.l]
   mov   A, [ACIA_DATA]
   cmp   A, 127
   jnz   [Y|@notback.l]
   mov   A, [TMP20+7]
   add   A, 1
   cmp   A, B
   jz    [Y|@loop.l]
   mov   A, 0x08
   mov   [ACIA_DATA], A
@wait:
   mov   A, [ACIA_STATUS]
   and   A, ACIA_TDRE
   jz    [Y|@wait.l]
   mov   A, 0x20
   mov   [ACIA_DATA], A
@wait2:
   mov   A, [ACIA_STATUS]
   and   A, ACIA_TDRE
   jz    [Y|@wait2.l]
   mov   A, 0x08
   mov   [ACIA_DATA], A
   sub   B, 1
   subc  X, 0
   jmp   [Y|@loop.l]
@notback:
   cmp   A, 10
   jz    [Y|@end.l]
   cmp   A, 0x20
   jnc   [Y|@loop.l]
   mov   [ACIA_DATA], A
   cmp   A, 'A'
   jnc   [Y|@lower.l]
   cmp   A, 'Z'+1
   jc    [Y|@lower.l]
   or    A, 0x20
@lower:
   mov   [X|B], A
   add   B, 1
   addc  X, 0
   jmp   [Y|@loop.l]
@end:
   mov   [ACIA_DATA], A
   mov   A, 0
   mov   [X|B], A
   mov   [TMP8+1], A

   mov   X, [OP1]
   mov   B, [OP1+1]
   add   B, 2
   addc  X, 0x40
   mov   [TMP+4], X
   mov   [TMP+5],  B

   mov   X, [TMP]
   mov   B, [TMP+1]
input_loop_outer:
   mov   A, [X|B]
   test  A
   mov   Y, input_end.h
   jz    [Y|input_end.l]
   cmp   A, 0x20
   mov   Y, @l2.h
   jnz   [Y|@l2.l]
   add   B, 1
   addc  X, 0
   jmp   [Y|input_loop_outer.l]
@l2:
   mov   [TMP+2], X
   mov   [TMP+3], B
@l3:
   add   B, 1
   addc  X, 0
   mov   A, [X|B]
   test  A
   mov   Y, @l4.h
   jz    [Y|@l4.l]
   cmp   A, 0x20
   jz    [Y|@l4.l]
   cmp   A, '.'
   jz    [Y|@l4.l]
   cmp   A, '\c'
   jz    [Y|@l4.l]
   cmp   A, '"'
   mov   Y, @l3.h
   jnz   [Y|@l3.l]
@l4:
   mov   [TMP+6], X
   mov   [TMP+7], B
   mov   A, [TMP+3]
   sub   B, A
   mov   Y, [TMP+4]
   mov   A, [TMP+5]
   mov   [Y|A+2], B
   mov   [TMP8], B

   mov   B, [TMP+3]
   mov   X, [TMP+1]
   sub   B, X
   add   B, 1
   mov   [Y|A+3], B
   mov   B, 0
   mov   [Y|A+0], B
   mov   [Y|A+1], B

   mov   A, -1
   mov   Y, @loop.h
@loop:
   add   A, 1
   cmp   A, 6
   jz    [Y|@end.l]
   mov   B, [TMP8]
   cmp   A, B
   jnc   [Y|@normal.l]
   mov   B, 5
   mov   [A+TMP10], B
   jmp   [Y|@loop.l]
@normal:
   mov   X, [TMP+2]
   mov   B, [TMP+3]
   add   B, A
   mov   B, [X|B]
   cmp   B, 'A'
   jc    [Y|@alpha.l]
   mov   X, rztable.h
   mov   B, [X|B+rztable.l-0x21]
   mov   [A+TMP10+1], B
   mov   B, 5
   mov   [A+TMP10], B
   add   A, 1
   jmp   [Y|@loop.l]
@alpha:
   sub   B, 'a'-6
   mov   [A+TMP10], B
   jmp   [Y|@loop.l]
@end:

   mov   A, [TMP10]
   add   A, A
   add   A, A
   mov   B, [TMP10+1]
   mov   X, shr3.hr
   mov   B, [X|B]
   or    B, A
   mov   [TMP18], B

   mov   A, [TMP10+1]
   add   A, A
   add   A, A
   add   A, A
   add   A, A
   add   A, A
   mov   B, [TMP10+2]
   or    B, A
   mov   [TMP18+1], B

   mov   A, [TMP10+3]
   add   A, A
   add   A, A
   or    A, 0x80
   mov   B, [TMP10+4]
   mov   B, [X|B]
   or    B, A
   mov   [TMP18+2], B

   mov   A, [TMP10+4]
   add   A, A
   add   A, A
   add   A, A
   add   A, A
   add   A, A
   mov   B, [TMP10+5]
   or    B, A
   mov   [TMP18+3], B

; TMP10:   start
; TMP10+2: middle
; TMP10+4: end

   mov   A, 0
   mov   [TMP10], A
   mov   [TMP10+1], A
   mov   Y, [DICT]
   mov   A, [DICT+1]
   mov   X, [Y|A-2]
   mov   B, [Y|A-1]
   mov   [TMP10+4], X
   mov   [TMP10+5], B
@loop:
   mov   Y, [TMP10]
   mov   A, [TMP10+1]
   mov   X, [TMP10+4]
   mov   B, [TMP10+5]
   cmp   B, A
   cmpc  X, Y
   mov   Y, @loop_end.h
   jnc   [Y|@loop_end.l]
   mov   Y, [TMP10]
   add   A, B
   addc  B, X, Y
   mov   Y, shr.hr
   mov   A, [Y|A]
   mov   X, @skip.h
   andt  B, 1
   jz    [X|@skip.l]
   or    A, 0x80
@skip:
   mov   X, shr.hr
   mov   Y, [X|B]
   mov   [TMP10+2], Y
   mov   [TMP10+3], A
;
   add   B, A, A
   addc  X, Y, Y  ; Y|A=middle, X|B=middle*2
   add   B, B
   addc  X, X     ; X|B=middle*4
   add   B, B
   addc  X, X     ; X|B=middle*8
   sub   B, A
   subc  X, Y     ; X|B=middle*7
   mov   Y, [DICT]
   mov   A, [DICT+1]
   add   B, A
   addc  X, Y
   mov   A, 0
@loop2:
   mov   [TMP10+6], A
   mov   A, [A+TMP18]
   mov   Y, [X|B]     ; X=a[i], A=word[i]
   cmp   A, Y
   mov   Y, @equal.h
   jz    [Y|@equal.l]
   mov   Y, @notless.h
   jc    [Y|@notless.l]
   mov   X, [TMP10+2]
   mov   B, [TMP10+3]
   sub   B, 1
   subc  X, 0
   mov   [TMP10+4], X
   mov   [TMP10+5], B
   mov   Y, @loop.h
   jmp   [Y|@loop.l]
@notless:
   mov   X, [TMP10+2]
   mov   B, [TMP10+3]
   add   B, 1
   addc  X, 0
   mov   [TMP10], X
   mov   [TMP10+1], B
   mov   Y, @loop.h
   jmp   [Y|@loop.l]
@equal:
   mov   A, [TMP10+6]
   add   A, 1
   add   B, 1
   addc  X, 0
   cmp   A, 4
   mov   Y, @loop2.h
   jnz   [Y|@loop2.l]
   mov   Y, [TMP+4]
   mov   A, [TMP+5]
   sub   B, 4
   subc  X, 0x40
   mov   [Y|A], X
   mov   [Y|A+1], B
@loop_end:
   mov   X, [TMP+4]
   mov   B, [TMP+5]
   add   B, 4
   addc  X, 0
   mov   [TMP+4], X
   mov   [TMP+5], B
   mov   B, [TMP8+1]
   add   B, 1
   mov   [TMP8+1], B
   mov   X, [TMP+6]
   mov   B, [TMP+7]
   mov   [TMP+2], X
   mov   [TMP+3], B
   mov   Y, input_loop_outer.h
   jmp   [Y|input_loop_outer.l]

input_end:
   mov   X, [OP1]
   mov   B, [OP1+1]
   add   X, 0x40
   mov   A, [TMP8+1]
   mov   [X|B+1], A
   mov   A, 0
   mov   [CURPOS], A
   mov   Y, main_loop.h
   jmp   [Y|main_loop.l]
