.section instruction

call:
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [get_i]
@l1:
   mov   Y, @normal.h
   mov   X, [OP0]
   test  X
   jnz   [Y|@normal.l]
   mov   X, [OP0+1]
   test  X
   jnz   [Y|@normal.l]
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   [TMP10], Y
   mov   [TMP10+1], A
   mov   Y, store.h
   jmp   [Y|store.l]
@normal:
   mov   [TMP], B
   mov   X, [PC+1]
   mov   B, [PC+2]
   mov   Y, @l2.hr
   mov   A, @l2.l
   jmp   [push]
@l2:
   mov   B, [TMP]
   mov   X, [PC]
   mov   Y, @l3.hr
   mov   A, @l3.l
   jmp   [push]
@l3:
   mov   X, [BP]
   mov   B, [BP+1]
   mov   A, @l4.l
   jmp   [push]
@l4:
   mov   X, [SP]
   mov   B, [SP+1]
   mov   [BP], X
   mov   [BP+1], B
   mov   A, 0
   mov   X, [OP0]
   mov   B, [OP0+1]
   add   B, B
   addc  X, X
   addc  A, 0
   sub   B, 1
   subc  X, 0
   subc  A, 0
   mov   [PC], A
   mov   [PC+1], X
   mov   [PC+2], B

   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [get_i]
@l1:
   test  B
   mov   Y, main_loop.h
   jz    [Y|main_loop.l]
   add   B, B
   add   B, 2
   mov   [TMP], B
;
   mov   B, [N_ARGS]
   add   B, B
   mov   [N_ARGS], B
   mov   B, 2
@loop:
   mov   [TMP+1], B
   mov   X, [N_ARGS]
   cmp   B, X
   mov   Y, @default.h
   jp    [Y|@default.l]
   mov   A, [PC]
   mov   X, [PC+1]
   mov   Y, [PC+2]
   add   Y, 2
   addc  X, 0
   addc  A, 0
   mov   [PC], A
   mov   [PC+1], X
   mov   [PC+2], Y
   mov   X, [B]
   mov   B, [B+1]
   mov   Y, @loop_end.hr
   mov   A, @loop_end.l
   jmp   [push]
@default:
   mov   Y, @l3.hr
   mov   A, @l3.l
   jmp   [get_i]
@l3:
   mov   [TMP8], B
   mov   A, @l2.l
   jmp   [get_i]
@l2:
   mov   X, [TMP8]
   mov   A, @loop_end.l
   jmp   [push]
@loop_end:
   mov   B, [TMP+1]
   add   B, 2
   mov   X, [TMP]
   cmp   B, X
   mov   Y, @loop.h
   jnz   [Y|@loop.l]
   mov   Y, main_loop.h
   jmp   [Y|main_loop]

storew:
   mov   X, [OP0]
   mov   B, [OP0+1]
   add   X, 0x40
   mov   Y, [OP1]
   mov   A, [OP1+1]
   add   A, A
   addc  Y, Y
   add   B, A
   addc  X, Y
   mov   Y, [OP2]
   mov   A, [OP2+1]
   mov   [X|B], Y
   mov   [X|B+1], A
   mov   Y, main_loop.h
   jmp   [Y|main_loop.l]

storeb:
   mov   X, [OP0]
   mov   B, [OP0+1]
   add   X, 0x40
   mov   Y, [OP1]
   mov   A, [OP1+1]
   add   B, A
   addc  X, Y
   mov   A, [OP2+1]
   mov   [X|B], A
   mov   Y, main_loop.h
   jmp   [Y|main_loop.l]

put_prop:
   mov   Y, @l1.hr
   mov   A, @l1.l
   mov   X, _get_prop_addr.h
   jmp   [X|_get_prop_addr.l]
@l1:
   mov   B, [Y|A-1]
   mov   X, @byte.h
   and   B, 0xE0
   jz    [X|@byte.l]
   mov   X, [OP2]
   mov   B, [OP2+1]
   mov   [Y|A], X
   mov   [Y|A+1], B
   mov   Y, main_loop.h
   jmp   [Y|main_loop.l]
@byte:
   mov   B, [OP2+1]
   mov   [Y|A], B
   mov   Y, main_loop.h
   jmp   [Y|main_loop.l]

print_char:
   mov   A, [OP0+1]
   mov   [ACIA_DATA], A
   mov   A, [CURPOS]
   add   A, 1
   mov   [CURPOS], A
   mov   Y, main_loop.h
   jmp   [Y|main_loop.l]

print_num:
   cmp   A, print_num.l
   mov   X, @return_ya.h
   jnz   [X|@return_ya.l]
   mov   Y, main_loop.hr
   mov   A, main_loop.l
@return_ya:
   mov   [TMP+2], Y
   mov   [TMP+3], A
   mov   A, 0
   mov   [TMP], A
   mov   X, [OP0]
   mov   B, [OP0+1]
   test  X
   mov   Y, @pos.h
   jp    [Y|@pos.l]
   sub   B, A, B
   subc  X, A, X
   mov   A, '-'
   mov   [ACIA_DATA], A
   mov   A, [CURPOS]
   add   A, 1
   mov   [CURPOS], A
@pos:
   mov   A, 0x2F

@ten4:
   add   A, 1
   sub   B, 0x10
   subc  X, 0x27 ; 10000
   jc     [Y|@ten4.l]
   cmp   A, 0x30
   jnz   [Y|@l1.l]
   mov   Y, [TMP]
   test  Y
   mov   Y, @l2.h
   jz    [Y|@l2.l]
@l1:
   mov   [ACIA_DATA], A
   mov   [TMP], A
   mov   A, [CURPOS]
   add   A, 1
   mov   [CURPOS], A
@l2:
   mov   A, 0x2F
   add   B, 0x10
   addc  X, 0x27 ; 10000

@ten3:
   add   A, 1
   sub   B, 0xE8
   subc  X, 0x03 ; 1000
   jc     [Y|@ten3.l]
   cmp   A, 0x30
   jnz   [Y|@l1.l]
   mov   Y, [TMP]
   test  Y
   mov   Y, @l2.h
   jz    [Y|@l2.l]
@l1:
   mov   [ACIA_DATA], A
   mov   [TMP], A
   mov   A, [CURPOS]
   add   A, 1
   mov   [CURPOS], A
@l2:
   mov   A, 0x2F
   add   B, 0xE8
   addc  X, 0x03 ; 1000

@ten2:
   add   A, 1
   sub   B, 0x64
   subc  X, 0x00 ; 100
   jc     [Y|@ten2.l]
   mov   Y, @l1.h
   cmp   A, 0x30
   jnz   [Y|@l1.l]
   mov   Y, [TMP]
   test  Y
   mov   Y, @l2.h
   jz    [Y|@l2.l]
@l1:
   mov   [ACIA_DATA], A
   mov   [TMP], A
   mov   A, [CURPOS]
   add   A, 1
   mov   [CURPOS], A
@l2:
   mov   A, 0x2F
   add   B, 0x64
   addc  X, 0x00 ; 100

@ten1:
   add   A, 1
   sub   B, 0x0A
   jc    [Y|@ten1.l]
   cmp   A, 0x30
   jnz   [Y|@l1.l]
   mov   Y, [TMP]
   test  Y
   mov   Y, @l2.h
   jz    [Y|@l2.l]
@l1:
   mov   [ACIA_DATA], A
   mov   A, [CURPOS]
   add   A, 1
   mov   [CURPOS], A
@l2:
   mov   X, [ACIA_STATUS]
   and   X, ACIA_TDRE
   jz    [Y|@l2.l]
   add   B, 0x3A
   mov   [ACIA_DATA], B
   mov   Y, [TMP+2]
   mov   A, [TMP+3]
   jmp   [Y|A]

random:
   mov   A, [OP0]
   mov   Y, @gen_random.h
   andt  A, 0x80           ; if range is negative, seed generator
   jz    [Y|@gen_random.l]
   mov   B, [OP0+1]
   add   A, B
   mov   [RAND], A
   mov   A, 0
   mov   [OP0], A
   mov   [OP0+1], A
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   X, storei.h
   jmp   [X|storei.l]
@gen_random:
   mov   Y, shr.hr
   mov   X, @l1.h
   mov   A, [RAND]
   andt  A, 1
   mov   A, [Y|A]
   jz    [X|@l1.l]
   mov   B, A
   nand  A, 0xB8
   or    B, 0xB8
   and   A, B
@l1:
   mov   [TMP], A

   andt  A, 1
   mov   A, [Y|A]
   jz    [X|@l1.l]
   mov   B, A
   nand  A, 0xB8
   or    B, 0xB8
   and   A, B
@l1:
   mov   [RAND], A
   mov   [TMP+1], A

   mov   A, 0
   mov   [TMP+4], A
   mov   [TMP+5], A
   mov   [TMP+6], A
   mov   [TMP+7], A
   mov   [TMP8+1], A
@loop:
   mov   A, [TMP8+1]
   add   A, 1
   cmp   A, 17
   mov   [TMP8+1], A
   mov   Y, @loop_end.h
   jz    [Y|@loop_end.l]
   mov   Y, [TMP+6]
   mov   A, [TMP+7]
   add   A, A
   addc  Y, Y
   mov   [TMP+6], Y
   mov   X, [TMP]
   mov   B, [TMP+1]
   mov   Y, @l1.h
   andt  X, 0x80
   jz    [Y|@l1.l]
   or    A, 1
@l1:
   mov   [TMP+7], A
   add   B, B
   addc  X, X
   mov   [TMP], X
   mov   [TMP+1], B
   mov   Y, [TMP+6]
   mov   X, [OP0]
   mov   B, [OP0+1]
   sub   B, A, B
   subc  X, Y, X
   mov   A, 0
   addc  A, 0
   mov   Y, [TMP+5]
   add   Y, Y
   mov   [TMP+5], Y
   mov   Y, [TMP+4]
   addc  Y, Y
   mov   [TMP+4], Y
   mov   Y, @loop.h
   test  A
   jz    [Y|@loop.l]
   mov   [TMP+6], X
   mov   [TMP+7], B
   mov   B, [TMP+5]
   or    B, 1
   mov   [TMP+5], B
   jmp   [Y|@loop.l]
@loop_end:
   mov   X, [TMP+6]
   mov   B, [TMP+7]
   add   B, 1
   addc  X, 0
   mov   [OP0], X
   mov   [OP0+1], B
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   mov   X, storei.h
   jmp   [X|storei.l]

zpush:
   mov   X, [OP0]
   mov   B, [OP0+1]
   mov   Y, main_loop.hr
   mov   A, main_loop.l
   jmp   [push]

zpull:
   mov   Y, @l1.hr
   mov   A, @l1.l
   jmp   [pop]
@l1:
   mov   [TMP10], X
   mov   [TMP10+1], B
   mov   A, @l2.l
   mov   B, [OP0+1]
   test  B
   jnz   [var]
   mov   Y, [SP]
   mov   A, [SP+1]
@l2:
   mov   X, [TMP10]
   mov   B, [TMP10+1]
   mov   [Y|A], X
   mov   [Y|A+1], B
   mov   Y, main_loop.h
   jmp   [Y|main_loop.l]

.section data
.org 0xC800
run_var:
   call
   storew
   storeb
   put_prop
   input
   print_char
   print_num
   random
   zpush
   zpull
