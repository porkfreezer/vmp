
.section data
num:
   0x3f800000q ; 1
   0xc1200000q ; 10
   0x4996b438q ; 1234567
   0x4ceb79a3q ; ~123456789 = 123456792
   0x33d6bf95q ; 1e-7
num_end:

.section instruction

.org 0x0000
reset:
   mov   Y, start.h
   jmp   [Y|start.l]

; Unpack IEEE-754 to FPDST
unpack:
   mov   [TMP], Y
   mov   [TMP+1], A
   mov   A, [FPDST]
   mov   Y, [X|B]
   and   Y, 0x80
   mov   [A+4], Y
   mov   Y, [X|B+1]
   add   Y, Y
   mov   Y, [X|B]
   addc  Y, Y
   mov   [A], Y
   mov   Y, [X|B+1]
   or    Y, 0x80
   mov   [A+1], Y
   mov   Y, [X|B+2]
   mov   [A+2], Y
   mov   Y, [X|B+3]
   mov   [A+3], Y
   mov   Y, [TMP]
   mov   A, [TMP+1]
   jmp   [Y|A]

copy_0to1:
   mov   B, [FP0]
   mov   [FP1], B
   mov   B, [FP0+1]
   mov   [FP1+1], B
   mov   B, [FP0+2]
   mov   [FP1+2], B
   mov   B, [FP0+3]
   mov   [FP1+3], B
   mov   B, [FP0+4]
   mov   [FP1+4], B
   jmp   [Y|A]

copy_0to2:
   mov   B, [FP0]
   mov   [FP2], B
   mov   B, [FP0+1]
   mov   [FP2+1], B
   mov   B, [FP0+2]
   mov   [FP2+2], B
   mov   B, [FP0+3]
   mov   [FP2+3], B
   mov   B, [FP0+4]
   mov   [FP2+4], B
   jmp   [Y|A]

start:
   mov   A, FP1
   mov

   mov   Y, @l1.hr
   mov   B, num.l-4
   mov   [TMP+16], B
@l1:
   mov   B, '\n'
   mov   [OUT], B
   mov   B, [TMP+16]
   add   B, 4
   mov   [TMP+16], B
   cmp   B, num_end.l
   jz    [Y|@end.l]
   mov   X, num.hr
   mov   A, @l2.l
   jmp   [unpack]
@l2:
   mov   A, @l1.l
   mov   X, print_float.hr
   jmp   [X|print_float.l]
@end:
   jmp   [Y|@end.l]

; FP1 + FP2 => FP0
add_float:
   mov   [TMP], Y
   mov   [TMP+1], A
   mov   Y, @pos.h
   mov   A, FP1
   mov   X, [FP1]
   mov   B, [FP2]
   sub   B, X
   jnc   [Y|@pos.l]
   nand  B, 0xff
   add   B, 1
   mov   A, FP2
@pos:

; Print FP0
print_float:
   mov   [TMP+2], Y
   mov   [TMP+3], A
   mov   A, 23
   mov   [TMP], A
   mov   Y, @l1.hr
   mov   B, [FP0+4]
   mov   A, '+'
   test  B
   jp    [Y|@l1.l]
   mov   A, '-'
@l1:
   mov   [OUT], A
   mov   A, '1'
   mov   [OUT], A
   mov   A, '.'
   mov   [OUT], A
   mov   X, [FP0+1]
   mov   B, [FP0+2]
   mov   A, [FP0+3]
   mov   [TMP+4], A
@loop:
   mov   A, [TMP+4]
   add   A, A
   mov   [TMP+4], A
   addc  B, B
   addc  X, X
   andt  X, 0x80
   mov   A, '0'
   jz    [Y|@l2.l]
   mov   A, '1'
@l2:
   mov   [OUT], A
   mov   A, [TMP]
   sub   A, 1
   mov   [TMP], A
   jnz   [Y|@loop.l]

   mov   A, 'e'
   mov   [OUT], A
   mov   A, [FP0]
   sub   A, 127
   mov   B, '+'
   jp    [Y|@l1.l]
   nand  A, 0xFF
   add   A, 1
   mov   B, '-'
@l1:
   mov   [OUT], B
   mov   B, 7
@loop:
   andt  A, 0x40
   mov   X, '0'
   jz    [Y|@l2.l]
   mov   X, '1'
@l2:
   mov   [OUT], X
   add   A, A
   sub   B, 1
   jnz   [Y|@loop]

   mov   Y, [TMP+2]
   mov   A, [TMP+3]
   jmp   [Y|A]
